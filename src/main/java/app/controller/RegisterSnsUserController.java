package app.controller;

import app.domain.model.Company;
import app.domain.model.SnsUser;
import app.domain.store.SnsUserStore;
import app.mappers.dto.SnsUserDto;

public class RegisterSnsUserController {
    private final SnsUserStore snsUserStore;
    private SnsUser snsUser;

    /**
     * Get company
     */
    public RegisterSnsUserController() {
        this(App.getInstance().getCompany());
    }

    /**
     * Company constructor
     * @param company DGS/SNS
     */
    public RegisterSnsUserController(Company company) {
        this.snsUserStore = company.getSnsUserStore();
        this.snsUser = null;
    }

    /**
     * Create new instance of SNS User
     * @param snsUserDto DTO with SNS User data
     * @return SNS User
     */
    public SnsUser createSnsUser(SnsUserDto snsUserDto) {
        snsUser = snsUserStore.createSnsUser(snsUserDto);
        snsUserStore.validateSnsUser(snsUser);
        return snsUser;
    }

    /**
     * Saves SNS User to auth and list
     */
    public boolean saveSnsUser() {
       return this.snsUserStore.saveSnsUser(snsUser);
    }

    /**
     * Return SNS User list as formatted String
     * @return SNS User list String
     */
    public String getSnsUsers() {
        return this.snsUserStore.toString();
    }
}