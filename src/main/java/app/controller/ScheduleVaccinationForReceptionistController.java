package app.controller;

import app.domain.model.*;
import app.domain.store.*;
import app.mappers.dto.VaccinationCenterDto;
import app.mappers.dto.VaccineScheduleDTO;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class ScheduleVaccinationForReceptionistController {

    private final SnsUserStore userStore;
    private final VaccineStore vaccineStore;
    private final VaccineScheduleStore vaccineScheduleStore;
    private final VaccineTypeStore vaccineTypeStore;
    private final VaccinationCenterStore vaccinationCenterStore;
    private ArrayList<SnsUser> userList = new ArrayList<>();


    public ScheduleVaccinationForReceptionistController(Company company){
        userStore = company.getSnsUserStore();
        vaccineStore = company.getVaccineStore();
        vaccineScheduleStore = company.getVaccineScheduleStore();
        vaccineTypeStore = company.getVaccineTypeStore();
        vaccinationCenterStore = company.getVaccinationCenterStore();
    }

    public SnsUser findSNSUser(String SNSUserNumber){
        SnsUser snsUser = this.userStore.findUserByNumber(SNSUserNumber, userList );
        if(this.vaccineScheduleStore.checkUserSchedule(snsUser)){
            return snsUser;
        }
        return null;
    }

    public List<VaccineType> getVaccineTypeList(){
        return this.vaccineTypeStore.getVaccineTypeList();
    }

    public List<VaccinationCenter> getVaccinationCenterList(){
        return this.vaccinationCenterStore.getVaccinationCenterList();
    }

    public List<LocalDateTime> getNextAvailableDates(VaccinationCenterDto vcDTO){

        return vcDTO.getNextAvailableDates();
    }

    public VaccineScheduleDTO createVaccineScheduleDTO(SnsUser snsUser,VaccineType vaccineType,VaccinationCenter vaccinationCenter,LocalDateTime date){
        return null;
    }

    public VaccineSchedule createVaccineSchedule(VaccineScheduleDTO vaccineScheduleDTO){
        return null;
    }

    public boolean validateVaccineSchedule(VaccineScheduleDTO vsDTO){
        /*
        Method is meant to validate the data in the vaccineScheduleDTO instance
        To be Implemented
         */
        return true;
    }

    public boolean saveVaccineSchedule(VaccineSchedule vaccineSchedule){
        return this.vaccineScheduleStore.addVaccineSchedule(vaccineSchedule);
    }

}
