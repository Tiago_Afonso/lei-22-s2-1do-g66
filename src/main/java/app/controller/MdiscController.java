package app.controller;

import app.ui.console.MdiscUI;

import java.awt.*;
import java.io.*;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.*;
import java.util.List;

public class MdiscController implements Serializable {

    private List<LocalDateTime> arrivalTimeList;
    private List<LocalDateTime> departureTimeList;

    private static final String SPLIT_REGEX1 = ";";
    private static final String SPLIT_REGEX2 = "[/ :]";
    private static final int ARRIVAL_TIME_INDEX = 5;
    private static final int DEPARTURE_TIME_INDEX = 7;
    private static final int MONTH_INDEX = 0;
    private static final int DAY_INDEX = 1;
    private static final int YEAR_INDEX = 2;
    private static final int HOUR_INDEX = 3;
    private static final int MINUTE_INDEX = 4;

    public MdiscController() {
        arrivalTimeList = new ArrayList<>();
        departureTimeList = new ArrayList<>();

        try {
            importDataFromExcelFile();
        } catch (IOException exception) {
            exception.printStackTrace();
        }
        Collections.sort(arrivalTimeList);
        Collections.sort(departureTimeList);
    }

    public List<LocalDateTime> getArrivalTimeList() {
        return arrivalTimeList;
    }

    public List<LocalDateTime> getDepartureTimeList() {
        return departureTimeList;
    }

    public int determineMaxLoop(int timeInterval){
        return 720/timeInterval;
    }

    public ArrayList<LocalTime> determineLimitTimeList(LocalTime openHour, int timeInterval, int maxLoop){
        ArrayList<LocalTime> listToReturn = new ArrayList<>();
        for(int i = 0; i < maxLoop; i++) {
            openHour = openHour.plusMinutes(timeInterval);
            listToReturn.add(openHour);
        }
        return listToReturn;
    }

    public ArrayList<Integer> determineFinalList(ArrayList<LocalTime> limitTimeList, int timeInterval){
        ArrayList<Integer> listToReturn = new ArrayList<>();
        for (LocalTime limitTime : limitTimeList){
            LocalTime limiteDeBaixo = limitTime.minusMinutes(timeInterval);
            LocalTime limiteDeCima = limitTime;
            int somaIntervalo = 0;
            for (LocalDateTime departureTime : departureTimeList){
                LocalDateTime minDateToCompare = LocalDateTime.of(departureTime.toLocalDate(),limiteDeBaixo);
                LocalDateTime maxDateToCompare = LocalDateTime.of(departureTime.toLocalDate(),limiteDeCima);
                if (departureTime.isAfter(minDateToCompare.minusSeconds(1)) && departureTime.isBefore(maxDateToCompare)){
                    somaIntervalo--;
                }
            }
            for (LocalDateTime arrivalTime : arrivalTimeList){
                LocalDateTime minDateToCompare = LocalDateTime.of(arrivalTime.toLocalDate(),limiteDeBaixo);
                LocalDateTime maxDateToCompare = LocalDateTime.of(arrivalTime.toLocalDate(),limiteDeCima);
                if (arrivalTime.isAfter(minDateToCompare.minusSeconds(1)) && arrivalTime.isBefore(maxDateToCompare)){
                    somaIntervalo++;
                }
            }
            listToReturn.add(somaIntervalo);
        }
        return listToReturn;
    }

    public ArrayList<LocalDateTime> determineIntervaloDeTempos(ArrayList<Integer> finalList, ArrayList<Integer> listOfEntries, LocalTime openHour, int timeInterval){
        int maxSoma = 1;
        int minSoma = -1;
        for (int i = 0; i < finalList.size(); i++){
            minSoma++;
            if (listOfEntries.get(0).equals(finalList.get(i))){
                for (int j = 1; j < listOfEntries.size(); j++){
                    if (listOfEntries.get(j).equals(finalList.get(i+j))){
                        maxSoma++;
                        if (maxSoma == listOfEntries.size() ) {
                            break;
                        }
                    }else{
                        maxSoma = 1;
                        break;
                    }
                }
                if (maxSoma > 1){
                    break;
                }
            }

        }
        LocalDateTime minDate = LocalDateTime.of(arrivalTimeList.get(0).toLocalDate(),openHour.plusMinutes(timeInterval * minSoma));
        LocalDateTime maxDate = LocalDateTime.of(arrivalTimeList.get(0).toLocalDate(),openHour.plusMinutes(timeInterval * maxSoma));
        ArrayList<LocalDateTime> toPrint2 = new ArrayList<>();
        toPrint2.add(minDate);
        toPrint2.add(maxDate);
        return toPrint2;
    }

    public static int[] intListToArray(ArrayList<Integer> intList){
        int[] arrayToReturn = new int[intList.size()];
        for (int i = 0; i < intList.size(); i++){
            arrayToReturn[i] = intList.get(i);
        }
        return arrayToReturn;
    }

    private String selectFile() {
        FileDialog dialog = new FileDialog((Frame)null, "Select File to Open");
        StringBuilder stringBuilder = new StringBuilder();
        dialog.setMode(FileDialog.LOAD);
        dialog.setVisible(true);
        stringBuilder.append(dialog.getDirectory()).append(dialog.getFile());
        return stringBuilder.toString();
    }

    private void importDataFromExcelFile() throws IOException {
        Scanner scanner = new Scanner(new File(selectFile()));
        boolean headerIteration = true;
        while (scanner.hasNext()) {
            if (headerIteration) {
                scanner.nextLine();
                headerIteration = false;
            } else {
                String[] auxilaryArray1 = scanner.nextLine().split(SPLIT_REGEX1);
                String[] auxilaryArray2 = auxilaryArray1[ARRIVAL_TIME_INDEX].split(SPLIT_REGEX2);
                arrivalTimeList.add(LocalDateTime.of(Integer.valueOf(auxilaryArray2[YEAR_INDEX]),
                        Integer.valueOf(auxilaryArray2[MONTH_INDEX]),
                        Integer.valueOf(auxilaryArray2[DAY_INDEX]),
                        Integer.valueOf(auxilaryArray2[HOUR_INDEX]),
                        Integer.valueOf(auxilaryArray2[MINUTE_INDEX])));

                auxilaryArray2 = auxilaryArray1[DEPARTURE_TIME_INDEX].split(SPLIT_REGEX2);

                departureTimeList.add(LocalDateTime.of(Integer.valueOf(auxilaryArray2[YEAR_INDEX]),
                        Integer.valueOf(auxilaryArray2[MONTH_INDEX]),
                        Integer.valueOf(auxilaryArray2[DAY_INDEX]),
                        Integer.valueOf(auxilaryArray2[HOUR_INDEX]),
                        Integer.valueOf(auxilaryArray2[MINUTE_INDEX])));
            }
        }
        scanner.close();
    }

    public static int[] Max(ArrayList<Integer> seqList) {

        int[] seq = intListToArray(seqList);

        int maxSoFar = 0;
        int maxEndingHere = 0;
        int startMaxSoFar = 0;
        int endMaxSoFar = 0;
        int startMaxEndingHere = 0;

        for(int i = 0; i < seq.length; ++i) {
            int elem = seq[i];
            int endMaxEndingHere = i + 1;
            if (maxEndingHere + elem < 0) {
                maxEndingHere = 0;
                startMaxEndingHere = i + 1;
            } else {
                maxEndingHere += elem;
            }

            if (maxSoFar < maxEndingHere) {
                maxSoFar = maxEndingHere;
                startMaxSoFar = startMaxEndingHere;
                endMaxSoFar = endMaxEndingHere;
            }
        }
        return Arrays.copyOfRange(seq, startMaxSoFar, endMaxSoFar);
    }
}
