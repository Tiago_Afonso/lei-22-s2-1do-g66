package app.controller;

import app.domain.model.Company;
import app.domain.model.SNSUsersWaitingRoom;
import app.domain.model.VaccinationCenter;
import app.domain.store.ListSNSUsersWaitingRoomStore;

import java.util.List;

public class ListSNSUsersWaitingRoomController {
    private ListSNSUsersWaitingRoomStore listSNSUsersWaitingRoomStore;
    private VaccinationCenter vaccinationCenter;
    private ListSNSUsersWaitingRoomStore list;

    public ListSNSUsersWaitingRoomController() {
        this.listSNSUsersWaitingRoomStore = listSNSUsersWaitingRoomStore;
    }

    public List<SNSUsersWaitingRoom> getListSNSUsersWaitingRoom() {
        return this.listSNSUsersWaitingRoomStore.getListSNSUsersWaitingRoom();
    }
}