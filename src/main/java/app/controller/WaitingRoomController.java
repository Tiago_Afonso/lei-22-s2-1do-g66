package app.controller;

import app.domain.model.Company;
import app.domain.model.VaccineAdministration;
import app.domain.model.WaitingRoom;
import app.domain.store.WaitingRoomStore;
import app.mappers.dto.WaitingRoomDto;

import java.util.List;

public class WaitingRoomController {
    private final Company company;
    private final WaitingRoomStore waitingRoomStore;
    private WaitingRoom waitingRoom;

    public WaitingRoomController() {
        this.company = App.getInstance().getCompany();
        this.waitingRoomStore = company.getWaitingRoomStore();
        this.waitingRoom = null;
    }

    public WaitingRoom createNewEntry(WaitingRoomDto waitingRoomDto){
        this.waitingRoom = waitingRoomStore.createEntry(waitingRoomDto);
        return this.waitingRoom;
    }

    public boolean saveNewEntry(){return this.waitingRoomStore.saveEntry(this.waitingRoom);}

    public List<WaitingRoom> getWaitingRoomList(){return this.waitingRoomStore.getWaitingRoomEntryList();}
}
