package app.controller;

import app.domain.model.Company;
import app.domain.model.VaccinationCenter;
import app.domain.store.VaccinationCenterStore;
import app.mappers.dto.VaccinationCenterDto;

import java.util.List;

public class RegisterVaccinationCenterController {
    private final VaccinationCenterStore vaccinationCenterStore;
    private VaccinationCenter vaccinationCenter;


    public RegisterVaccinationCenterController() {
        this(App.getInstance().getCompany());
    }
    
    public RegisterVaccinationCenterController(VaccinationCenterStore vaccinationCenterStore, VaccinationCenter vaccinationCenter) {
        this.vaccinationCenterStore = vaccinationCenterStore;
        this.vaccinationCenter = vaccinationCenter;
    }

    /**
     * Company constructor
     */

    public RegisterVaccinationCenterController(Company company) {
        this.vaccinationCenterStore = company.getVaccinationCenterStore();
        this.vaccinationCenter = null;
    }

    /**
     * Create new instance of Vaccination Center
     * @param vaccinationCenterDto DTO with Vaccination Center data
     * @return Vaccination Center
     */
    public VaccinationCenter createVaccinationCenter(VaccinationCenterDto vaccinationCenterDto) {
        vaccinationCenter = vaccinationCenterStore.createVaccinationCenter(vaccinationCenterDto);
        return vaccinationCenter;
    }

    /**
     * Saves Vaccination Center to list
     * @return
     * @param vaccinationCenterDto
     */
    public boolean saveVaccinationCenter(VaccinationCenterDto vaccinationCenterDto) {
        return this.vaccinationCenterStore.saveVaccinationCenter();
    }

    /**
     * Return Vaccination Center list
     * @return Vaccination Center list
     */
    public List<VaccinationCenter> getVaccinationcenterList() {
        return this.vaccinationCenterStore.getVaccinationCenterList();
    }

    /**
     * Return Vaccination Center list as formatted String
     * @return Vaccination Center list String
     */
    public String getVaccinationCenterList() {
        return this.vaccinationCenterStore.toString();
    }
}
