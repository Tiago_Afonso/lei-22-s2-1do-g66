package app.controller;

import app.domain.model.*;
import app.domain.shared.Constants;
import app.domain.store.*;
import app.mappers.dto.SnsUserDto;
import pt.isep.lei.esoft.auth.AuthFacade;
import pt.isep.lei.esoft.auth.UserSession;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

/**
 * @author Paulo Maio <pam@isep.ipp.pt>
 */
public class App {

    private Company company;
    private AuthFacade authFacade;
    private VaccineStore vaccineStore;
    private DosageStore dosageStore;
    private VaccinationCenterStore vcStore;
    private VaccineAdministrationStore vaStore;
    private final SnsUserStore snsUserStore;
    private final AdverseReactionStore adverseReactionStore;

    private App() {
        Properties props = getProperties();
        this.company = new Company(props.getProperty(Constants.PARAMS_COMPANY_DESIGNATION));
        this.authFacade = this.company.getAuthFacade();
        this.vaccineStore = this.company.getVaccineStore();
        this.dosageStore = this.company.getDosageStore();
        this.snsUserStore = this.company.getSnsUserStore();
        this.adverseReactionStore = this.company.getAdverseReactionStore();
        this.vaStore = this.company.getVaccineAdministrationStore();
        this.vcStore = this.company.getVaccinationCenterStore();
        bootstrap();
    }

    public Company getCompany() {
        return this.company;
    }

    public UserSession getCurrentUserSession() {
        return this.authFacade.getCurrentUserSession();
    }

    public boolean doLogin(String email, String pwd) {
        return this.authFacade.doLogin(email, pwd).isLoggedIn();
    }

    public void doLogout() {
        this.authFacade.doLogout();
    }

    private Properties getProperties() {
        Properties props = new Properties();

        // Add default properties and values
        props.setProperty(Constants.PARAMS_COMPANY_DESIGNATION, "DGS/SNS");


        // Read configured values
        try {
            InputStream in = new FileInputStream(Constants.PARAMS_FILENAME);
            props.load(in);
            in.close();
        } catch (IOException ex) {

        }
        return props;
    }


    private void bootstrap() {
        this.authFacade.addUserRole(Constants.ROLE_ADMIN, Constants.ROLE_ADMIN);
        this.authFacade.addUserRole(Constants.ROLE_RECEPTIONIST, Constants.ROLE_RECEPTIONIST);
        this.authFacade.addUserRole(Constants.ROLE_SNSUSER, Constants.ROLE_SNSUSER);



        this.authFacade.addUserWithRole("Main Administrator", "admin@lei.sem2.pt", "123456", Constants.ROLE_ADMIN);
        this.authFacade.addUserWithRole("Main Receptionist", "recep@lei.sem2.pt", "123456", Constants.ROLE_RECEPTIONIST);
        this.authFacade.addUserWithRole("Main Receptionist", "recep@lei.sem2.pt", "123456", Constants.ROLE_RECEPTIONIST);

        //bootstrap for US11
        this.authFacade.addUserRole(Constants.ROLE_NURSE,Constants.ROLE_NURSE);
        this.authFacade.addUserRole(Constants.ROLE_CENTER_COORDINATOR,Constants.ROLE_CENTER_COORDINATOR);
        this.authFacade.addUserWithRole("Main SNSUser", "user@lei.sem2.pt", "123456", Constants.ROLE_SNSUSER);
        this.authFacade.addUserWithRole("Main Nurse", "nurse@lei.sem2.pt", "123456", Constants.ROLE_NURSE);
        this.authFacade.addUserWithRole("US11 Center Coordinator", "us11centercoordinator@lei.sem2.pt", "123456", Constants.ROLE_CENTER_COORDINATOR);

        //-----------------------------------------------------------------

        this.authFacade.addUserWithRole("Center Coordinator", "centercoordinator@lei.sem2.pt", "123456", Constants.ROLE_CENTER_COORDINATOR);
        //-----------------------------------------------------------------

        VaccineType vt = new VaccineType (1234, "Covid19");
        List<Integer> list = new ArrayList<>();
        list.add(17);
        list.add(18);
        Vaccine v = vaccineStore.createVaccine("Spikevax", "123456", "Moderna", vt, list);
        vaccineStore.saveVaccine(v);
        //Vaccine v = new Vaccine ("Spikevax", "123456", "Moderna", vt, list);

        List<AgeGroup> ag = new ArrayList<>();

        SnsUser sns = new SnsUser("André Ferreira", "Rua Bentos Do Caraça 301", "Male", "964055743", "andrefer435@gmail.com","Grulis",  LocalDate.of(100, 1, 31 ),"45456573", "15563708");


        VaccinationCenter vc1 = new VaccinationCenter("name1", "adress","email", "website", LocalTime.now(), LocalTime.now(), 1, 1, 1,1);
        VaccinationCenter vc2 = new VaccinationCenter("name2", "adress","email", "website", LocalTime.now(), LocalTime.now(), 1, 1, 1,1);
        VaccinationCenter vc3 = new VaccinationCenter("name3", "adress","email", "website", LocalTime.now(), LocalTime.now(), 1, 1, 1,1);

        VaccineAdministration va1 = new VaccineAdministration(sns, new Vaccine("Covid-19","001","name", new VaccineType(12," "),ag),vc1);
        VaccineAdministration va2 = new VaccineAdministration(sns, new Vaccine("Covid-19","001","name", new VaccineType(12," "),ag),vc1);
        VaccineAdministration va3 = new VaccineAdministration(sns, new Vaccine("Covid-19","001","name", new VaccineType(12," "),ag),vc1);


        this.vcStore.saveVaccinationCenter(vc1);
        this.vcStore.saveVaccinationCenter(vc2);
        this.vcStore.saveVaccinationCenter(vc3);

        this.vaStore.saveVaccineAdministration(va1,true);
        this.vaStore.saveVaccineAdministration(va2,true);
        this.vaStore.saveVaccineAdministration(va3,true);

        //-----------------------------------------------------------------------------
        List<Integer> dosage1 = new ArrayList<>();
        dosage1.add(30);
        List<Integer> timeInterval1 = new ArrayList<>();
        timeInterval1.add(30);
        Dosage dosage = new Dosage(2,dosage1,timeInterval1);
        dosageStore.saveDosage(dosage);

        adverseReactionStore.deserialize();
    }

    // Extracted from https://www.javaworld.com/article/2073352/core-java/core-java-simply-singleton.html?page=2
    private static App singleton = null;

    public static App getInstance() {
        if (singleton == null) {
            synchronized (App.class) {
                singleton = new App();
            }
        }
        return singleton;
    }
}
