package app.controller;

import app.domain.model.*;
import app.domain.store.AdverseReactionStore;
import app.domain.store.SnsUserStore;
import app.mappers.dto.AdverseReactionDto;
import app.mappers.dto.SNSUserMapper;
import app.mappers.dto.SnsUserDtoV2;

import java.util.List;

public class AdverseReactionController {
    private final SnsUserStore snsUserStore;
    private final SNSUserMapper snsUserMapper;
    private final AdverseReactionStore adverseReactionStore;

    /**
     * Get company
     */
    public AdverseReactionController() {
        this(App.getInstance().getCompany());
    }

    /**
     * Constructor
     * @param company SNS/DGHS
     */
    public AdverseReactionController(Company company) {
        this.adverseReactionStore = company.getAdverseReactionStore();
        this.snsUserStore = company.getSnsUserStore();
        this.snsUserMapper = new SNSUserMapper();
    }

    /**
     *
     * @return List of SNS user name and SNS number
     */
    public List<SnsUserDtoV2> getSnsUsers(){
        List<SnsUser> snsUserList = snsUserStore.getSnsUserList();
        return snsUserMapper.toDTO(snsUserList);
    }

    /**
     * @param adverseReactionDto DTO of SdverseReaction
     * @throws Exception if description has more than
     */
    public void createAdverseReaction(AdverseReactionDto adverseReactionDto) throws Exception {
        AdverseReaction adverseReaction = adverseReactionStore.createAdverseReaction(adverseReactionDto);
        adverseReactionStore.saveAdverseReaction(adverseReaction);
    }

    /**
     * get
     * @param snsNumber SNS number of SNS user
     * @return SNS User with SNS number
     */
    public SnsUser getSnsUser(String snsNumber) {
        return snsUserStore.findUserBySNSNumber(snsNumber);
    }

    public String getAdverseReactions() {
        return adverseReactionStore.getAdverseReactionList().toString();
    }

}