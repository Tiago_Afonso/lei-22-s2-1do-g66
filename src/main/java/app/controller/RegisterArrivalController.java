package app.controller;

import app.domain.model.*;
import app.domain.store.RegisterArrivalStore;
import app.domain.store.SnsUserStore;
import app.domain.store.VaccineScheduleStore;
import app.mappers.dto.RegisterArrivalDto;
import app.mappers.dto.VaccineScheduleDTO;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class RegisterArrivalController {

    private RegisterArrivalStore registerArrivalStore;
    private RegisterArrival registerArrival;
    private final Company company;
    private final SnsUserStore snsUserStore;
    private final VaccineScheduleStore vaccineScheduleStore;

    /**
     * Main Constructor
     */
    public RegisterArrivalController() {
        this.company = App.getInstance().getCompany();
        this.registerArrivalStore = company.getArrivalStore();
        this.snsUserStore = company.getSnsUserStore();
        this.vaccineScheduleStore = company.getVaccineScheduleStore();
        registerArrival = null;

    }

    /**
     * Creating a new instance of arrival
     * @param registerArrivalDto registerArrivalDto DTO with User arrival data
     * @return
     */
    public RegisterArrival createArrival(RegisterArrivalDto registerArrivalDto){
        registerArrival = registerArrivalStore.createArrivalTime(registerArrivalDto);
        registerArrivalStore.validateArrival(registerArrival);
        return registerArrival;
    }

    /**
     * Return a Boolean for success of the operation
     * @return Boolean if data was saved (in)correctly
     */
    public boolean saveArrival() {return this.registerArrivalStore.saveArrival(registerArrival);}


    /**
     * Returns a string of registered arrivals
     * @return toString of a list of registered arrivals
     */
    public String getArrivals() {return this.registerArrivalStore.toString();}

    /**
     * Returns an ArrayList of the registered arrivals
     * @return ArrayList of registered arrivals
     */
    public List<RegisterArrival> getArrivalsData() {return this.registerArrivalStore.getListUserArrivalTimes();}

    /**
     * Returns a SNS user that has the same number as the input one
     * @param snsNumber input SNS number to compare with the list of existing SNS users
     * @return snsUser user that has the same number as the input one
     */
    public SnsUser getUserFromSNSNumber(String snsNumber) {
        for (SnsUser snsUsers : snsUserStore.getSnsUserList()) {
            if (snsUsers.getSnsNumber().equals(snsNumber)) {
                return snsUsers;
            }
        }
        throw new IllegalArgumentException("There's no SNS user registered with respective SNS number");
    }

    /**public boolean checkCheduledVaccination(Date date, SnsUser user){
        for(VaccineSchedule schedule : vaccineScheduleStore.getScheduledVaccinations()){
            if (schedule.getDate().equeals(date) && schedule.getUser().equals(user)){
            return true;

            }
        }
        throw new IllegalArgumentException("There's no schedule for selected SNS User for the respective date.")
    }*/
}
