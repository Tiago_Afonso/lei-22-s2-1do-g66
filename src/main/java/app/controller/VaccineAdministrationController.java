package app.controller;

import app.domain.model.*;
import app.domain.store.*;
import app.mappers.dto.SnsUserDto;
import app.mappers.dto.VaccinationCenterDto;
import app.mappers.dto.VaccineAdministrationDto;
import app.mappers.dto.WaitingRoomDto;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.List;

public class VaccineAdministrationController{
    private final Company company;
    private final WaitingRoomStore waitingRoomStore;
    private final VaccineAdministrationStore vaccineAdministrationStore;
    private final VaccineStore vaccineStore;
    private final DosageStore dosageStore;
    private final SnsUserStore snsUserStore;
    private VaccineAdministration vaccineAdministration;
    private boolean bootstrapDebounce = false;
    public final static int NOTIFICATION_TIME = 30;


    public VaccineAdministrationController() {
        this.company = App.getInstance().getCompany();
        this.waitingRoomStore = company.getWaitingRoomStore();
        this.vaccineStore = company.getVaccineStore();
        this.dosageStore = company.getDosageStore();
        this.vaccineAdministrationStore = company.getVaccineAdministrationStore();
        this.snsUserStore = company.getSnsUserStore();
        this.vaccineAdministration = null;
        this.vaccineAdministrationStore.deSerialize();
        bootstrap();
        checkForDuplicates();
    }

    /**
     * Creating a new vaccine administration.
     * @param vaccineAdministrationDto
     * @return VaccineAdministration object to be used to store the administration in the respective store
     */
    public VaccineAdministration createAdministration(VaccineAdministrationDto vaccineAdministrationDto){
        vaccineAdministration = vaccineAdministrationStore.createVaccineAdministration(vaccineAdministrationDto);
        return vaccineAdministration;
    }

    /**
     * Saves the current vaccine administration into the store.
     * @return boolean true if save was succesful otherwise it'll return false
     */
    public boolean saveAdministration(boolean ignoreSerialize){
        return vaccineAdministrationStore.saveVaccineAdministration(this.vaccineAdministration,ignoreSerialize);
    }

    /**
     * Simple getter to get the list of already administered vaccines.
     * @return List<VaccineAdministration> </VaccineAdministration> List that contains the administrations
     */
    public List<VaccineAdministration> getVaccineAdministrations(){return vaccineAdministrationStore.getVaccineAdministrationList();}

    /**
     * Removes a user from the waiting list. Used for when an administration is registered so user doesn't
     * get called twice to be vaccinated.
     * @param vaccineAdministration
     * @return boolean true if the removal was successful otherwise will return false
     */
    public boolean removeWaitingRoomListEntry(VaccineAdministration vaccineAdministration){return waitingRoomStore.removeEntry(vaccineAdministration.getSnsUser());}

    /**
     * Simple getter to get the waiting room store.
     * @return WaitingRoomStore object
     */
    public WaitingRoomStore getWaitingRoomStore() {return waitingRoomStore;}

    /**
     * Get's the available vaccines that are registered and left in the system to be administered.
     * @return List<Vaccine></Vaccine> list of available vaccines
     */
    public List<Vaccine> getAvailableVaccines(){
        return vaccineStore.getVaccineTypeList();
    }

    /**
     * Get's the registered dosages in the system so the actor can select what was the dosage of the vaccine administered
     * to the user.
     * @return List<Dosage></Dosage> list of existing dosages
     */
    public List<Dosage> getDosages(){return dosageStore.getDosageList();}

    /**
     * Responsible for notifying the user x amount of time after he's vaccinated so they are knowledgeable that they can
     * leave the vaccination center after the recovery period is up.
     * @param time parameter responsible for how long it takes for the use to be notified.
     * @param vaccineAdministration parameter that hold all info about the vaccine administtration including the user
     *                              SNS number and phone number.
     */
    public void notifyUser(int time, VaccineAdministration vaccineAdministration){
        Thread notificationThread = new Thread(() -> {
            LocalTime endTime = LocalTime.now().plusMinutes(time);

            do{}while (LocalTime.now().isBefore(endTime));

            try {
                FileWriter file = new FileWriter("src\\main\\java\\app\\domain\\model\\SMSNotification"
                        + vaccineAdministration.getSnsUser().getSnsNumber() + ".txt");
                file.write("The recovery period has ended. You're free to leave the vaccination center.");
                file.close();

                vaccineAdministration.setTimeDeparted(LocalDateTime.now());

            } catch (IOException exception) {
                System.out.println("Message" + exception.getMessage());
            }

        });
        notificationThread.start();
    }

    /**
     * Method responsible for running a loop that detects duplicate users in the waiting room store and the vaccine
     * administration store. This is important for serialization, since threads may be uncoordinated and this clears up
     * any duplications between different user stories.
     */
    public void checkForDuplicates(){
        for (int i = 0; i < vaccineAdministrationStore.getVaccineAdministrationList().size(); i++){
            VaccineAdministration currentAdministration = vaccineAdministrationStore.getVaccineAdministrationList().get(i);
            for (int j = 0; j < waitingRoomStore.getWaitingRoomEntryList().size(); j++ ){
                if (currentAdministration.getSnsUser().getSnsNumber().equals
                        (waitingRoomStore.getWaitingRoomEntryList().get(j).getUser().getSnsNumber())){
                    waitingRoomStore.getWaitingRoomEntryList().remove(j);
                    break;
                }
            }
        }
    }

    //BOOTSTRAP
    private void bootstrap() {
        if (bootstrapDebounce == false) {

            RegisterVaccinationCenterController registerVaccinationCenterController = new RegisterVaccinationCenterController();
            RegisterSnsUserController registerSnsUserController = new RegisterSnsUserController();

            VaccinationCenterDto testVaccinationCenterDto = new VaccinationCenterDto("Centro 1", "Rua do Centro 45", "centro1@mail.com", "centro1.com", LocalTime.of(8, 0), LocalTime.of(20, 0), 939393939, 20202, 50, 10);

            VaccinationCenter vaccinationCenter = registerVaccinationCenterController.createVaccinationCenter(testVaccinationCenterDto);
            registerVaccinationCenterController.saveVaccinationCenter(testVaccinationCenterDto);

            SnsUserDto snsUserDto = new SnsUserDto("FranciscoA", "Rua A", "M", "000000000",
                    "francisco@isep.pt", "AAAA1111", LocalDate.now(), "000000000",
                    "00000000");
            registerSnsUserController.createSnsUser(snsUserDto);
            registerSnsUserController.saveSnsUser();
            SnsUserDto snsUserDto1 = new SnsUserDto("FranciscoB", "Rua A1", "M", "111111111",
                    "francisco1@isep.pt", "AAAA1111", LocalDate.now(), "111111111",
                    "11111111");
            registerSnsUserController.createSnsUser(snsUserDto1);
            registerSnsUserController.saveSnsUser();
            SnsUserDto snsUserDto2 = new SnsUserDto("FranciscoC", "Rua A2", "M", "222222222",
                    "francisco2@isep.pt", "AAAA1111", LocalDate.now(), "222222222",
                    "22222222");
            registerSnsUserController.createSnsUser(snsUserDto2);
            registerSnsUserController.saveSnsUser();
            SnsUserDto snsUserDto3 = new SnsUserDto("FranciscoD", "Rua A3", "M", "333333333",
                    "francisco3@isep.pt", "AAAA1111", LocalDate.now(), "333333333",
                    "33333333");
            registerSnsUserController.createSnsUser(snsUserDto3);
            registerSnsUserController.saveSnsUser();
            SnsUserDto snsUserDto4 = new SnsUserDto("FranciscoE", "Rua A4", "M", "444444444",
                    "francisco4@isep.pt", "AAAA1111", LocalDate.now(), "444444444",
                    "44444444");
            registerSnsUserController.createSnsUser(snsUserDto4);
            registerSnsUserController.saveSnsUser();

            for (int i = 0; i < snsUserStore.getSnsUserList().size(); i++) {
                WaitingRoomDto waitingRoomDto = new WaitingRoomDto(snsUserStore.getSnsUserList().get(i), LocalDateTime.now().plusMinutes(i), vaccinationCenter);
                WaitingRoom waitingRoom = waitingRoomStore.createEntry(waitingRoomDto);
                waitingRoomStore.saveEntry(waitingRoom);
            }

            bootstrapDebounce = true;
        }
    }
    //END OF BOOTSTRAP
}
