package app.controller;

import app.domain.model.ImportSnsUserSetFromCsv;

public class ImportSnsUserSetFromCsvController {
    ImportSnsUserSetFromCsv importSnsUserSetFromCsv = new ImportSnsUserSetFromCsv();

    public void readCsv(String fileLocation) {
        importSnsUserSetFromCsv.readCsv(fileLocation);
    }
}
