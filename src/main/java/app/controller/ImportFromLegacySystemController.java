package app.controller;

import app.domain.model.*;
import app.domain.store.InformationLegacySystemStore;
import app.domain.store.SnsUserStore;
import app.domain.store.VaccineStore;
import app.domain.store.VaccineTypeStore;
import pt.isep.lei.esoft.auth.domain.store.UserStore;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class ImportFromLegacySystemController {
    private final ImportFromLegacySystem importFromLegacySystem = new ImportFromLegacySystem();
    private final InformationLegacySystemStore store;
    private final SnsUserStore userStore;
    private final VaccineStore vaccineStore;
    private final VaccineTypeStore vaccineTypeStore;
    private final SelectionSortAlgorithm selectionSortAlgorithm = new SelectionSortAlgorithm();
    private final BubbleSortAlgorithm bubbleSortAlgorithm = new BubbleSortAlgorithm();

    public ImportFromLegacySystemController(){
        store = App.getInstance().getCompany().getLsStore();
        userStore = App.getInstance().getCompany().getSnsUserStore();
        vaccineStore = App.getInstance().getCompany().getVaccineStore();
        vaccineTypeStore = App.getInstance().getCompany().getVaccineTypeStore();
    }

    public List<InformationLegacySystem> readFromLegacySystem(String path) {
        List<InformationLegacySystem> list = new ArrayList<>();
        list = importFromLegacySystem.readFromLegacySystem(path);
        return list;
    }

//    public boolean saveInformation(List<InformationLegacySystem> list){
//        return store.saveInformation(list);
//    }

    public List<InformationLegacySystem> informationSorted(String path, int attribute, int alg, int order){
        List<InformationLegacySystem> list = new ArrayList<>();
        InformationLegacySystem[] a = new InformationLegacySystem[readFromLegacySystem(path).size()];
        for (int i = 0; i < readFromLegacySystem(path).size(); i++) {
            a[i] = readFromLegacySystem(path).get(i);
        }
        if(alg == 1){ //Bubble Sort
            if(attribute == 1){
                list = bubbleSortAlgorithm.bubbleSortByArrivalDate(readFromLegacySystem(path));
            } else {
                list = bubbleSortAlgorithm.bubbleSortByLeavingDate(readFromLegacySystem(path));
            }
        } else {
            if(attribute == 1){
                selectionSortAlgorithm.selectionSortArrivalTime(a);
                list.addAll(Arrays.asList(a));
            } else {
                selectionSortAlgorithm.selectionSortLeavingTime(a);
                list.addAll(Arrays.asList(a));
            }
        }

        if(order == 2){ //Descending
            Collections.reverse(list);
        }
        //saveInformation(list);
        return list;
    }

    public List<InformationLegacySystem> getInformation(){
        return store.allInformation();
    }

    public String getUserName(String number){
        SnsUser u = userStore.findUserBySNSNumber(number);
        return u.getName();
    }

    public String getVaccineTypeDesc(String name){
        return vaccineStore.findVaccineType(name);
    }

    public void writeToFile(List<InformationLegacySystem> list, String path){
        BufferedWriter writer = null;
        try {
            writer = new BufferedWriter(new FileWriter(path));
            for (InformationLegacySystem i : list) {
                //System.out.println(i.toString());
                writer.write(i.toString() + "\n");
            }
        }catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                writer.close();
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
    }



}
