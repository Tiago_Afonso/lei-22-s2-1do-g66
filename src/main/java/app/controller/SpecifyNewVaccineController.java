package app.controller;

import app.domain.model.*;
import app.domain.store.AgeGroupStore;
import app.domain.store.DosageStore;
import app.domain.store.VaccineStore;
import app.domain.store.VaccineTypeStore;
import app.mappers.dto.VaccineTypeDTO;
import app.mappers.dto.VaccineTypeMapper;

import java.util.ArrayList;
import java.util.List;

public class SpecifyNewVaccineController {
    private Vaccine v;
    private final VaccineStore vStore;

    private Dosage dos;

    private final DosageStore dosStore;

    private AgeGroup ageGroup;

    private AgeGroupStore agStore;

    private String designation;

    private ArrayList<String> designations = new ArrayList<>();

    private VaccineType vt;

    private ArrayList<VaccineType> vtList;

    private VaccineTypeDTO vtdto;

    private VaccineTypeMapper vtMap;

    private VaccineTypeStore vtStore;

    public SpecifyNewVaccineController(){ this(App.getInstance().getCompany());}

    public SpecifyNewVaccineController(Company company) {
        this.vStore = company.getVaccineStore();
        this.v = null;
        this.dosStore = company.getDosageStore();
        this.dos = null;
        this.agStore = company.getAgeGroupStore();
        this.ageGroup=null;
        this.vtStore = company.getVaccineTypeStore();
        this.vt=null;
    }

    public ArrayList<VaccineType> bootstrapVaccineTypes(){
        ArrayList<VaccineType> vtList = new ArrayList<>();
        VaccineType vt1 = vtStore.createVaccineType(00001, "Tetano");
        vtList.add(vt1);
        VaccineType vt2 = vtStore.createVaccineType(00002, "Gripe Sasonal");
        vtList.add(vt2);
        VaccineType vt3 = vtStore.createVaccineType(00003, "Gripe A");
        vtList.add(vt3);
        VaccineType vt4 = vtStore.createVaccineType(00004, "Covid-19");
        vtList.add(vt4);
        return vtList;

    }
    public Vaccine createVaccine(String name, String iD, String brand, VaccineType vType, List ageGroups){

        v = vStore.createVaccine(name, iD, brand, vType, ageGroups);
        vStore.validateVaccine(v);
        return v;
    }
    public Dosage createDosage(int nrOfDoses, List<Integer> lDosageq, List<Integer> lTimeInterval){

        dos = dosStore.createDosage(nrOfDoses, lDosageq , lTimeInterval);
        dosStore.validateDosage(dos);
        return dos;
    }
    public AgeGroup createAgeGroup(int upperAge, int lowerAge,Dosage dosage){

        ageGroup = agStore.createAgeGroup(upperAge, lowerAge,dosage);
        agStore.validateAgeGroup(ageGroup);
        return ageGroup;
    }

    public List<Vaccine> getVaccines(){
        return vStore.getVaccineTypeList();
    }
 //   public ArrayList<String> getVaccineTypeList(){
 //       for(VaccineType vt : vtList){
    //          designation = vt.getDesignation();
  //          designations.add(designation);
   //     }
    //    return designations;
  //  }
   // public boolean saveVaccine(Vaccine v){
    //    return vStore.saveVaccine(v);
  //  }

}
