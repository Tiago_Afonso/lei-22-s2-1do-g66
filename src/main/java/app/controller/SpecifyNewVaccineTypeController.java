package app.controller;
import app.domain.model.Company;
import app.domain.model.VaccineType;
import app.domain.store.VaccineTypeStore;



public class SpecifyNewVaccineTypeController {
    private VaccineType vt;
    private VaccineTypeStore vtStore;

  public SpecifyNewVaccineTypeController(){ this(App.getInstance().getCompany());}

    public SpecifyNewVaccineTypeController(Company company) {
        this.vtStore = company.getVaccineTypeStore();

    }

    public VaccineType createVaccineType(int code, String designation){

      vt = vtStore.createVaccineType(code, designation);
      if (vtStore.validateVaccineType(vt)){
          return(vt);
      }
      else { return null;}

    }

}
