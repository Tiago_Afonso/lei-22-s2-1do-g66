package app.controller;

import app.domain.model.Company;
import app.domain.model.VaccinationCenter;
import app.domain.model.VaccineAdministration;
import app.domain.store.DailyRecordVaccinesStore;
import app.domain.store.VaccinationCenterStore;
import app.domain.store.VaccineAdministrationStore;


import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

public class RecordDailyVaccinesController implements Runnable {
    private final VaccinationCenterStore vcStore;
    private final VaccineAdministrationStore vaStore;
    private final DailyRecordVaccinesStore dvrStore;
    private List<VaccinationCenter> vcList;
    private List<VaccineAdministration> vaList;
    private List<VaccineAdministration> varList;
    private VaccinationCenter vc;
    private VaccineAdministration va;
    int a;
    private String[][] lists = null;
    private Company company;

    public RecordDailyVaccinesController() {
        company = App.getInstance().getCompany();
        this.vcStore = company.getVaccinationCenterStore();
        this.vaStore = company.getVaccineAdministrationStore();
        this.dvrStore = company.getDailyRecordVaccineStore();

        this.vaList = vaStore.getVaccineAdministrationList();
        this.vcList = vcStore.getVaccinationCenterList();
        this.a = vcList.size();
        this.lists = new String[a][3];
    }

    public void dailyRecording() {
        TimerTask task = new TimerTask() {
            public void run() {
                makeLists();
                dvrStore.dailySave(lists);

            }
        };
        Timer timer = new Timer("Timer");

        long delay =86400000;
        timer.schedule(task, delay);
    }


    public String[][] makeLists(){
        int i=0;
        for(VaccinationCenter vc: vcList){
            lists[i][0] = vc.getName();
            lists[i][1] = (LocalDate.now().getDayOfMonth()+ "/"+LocalDate.now().getMonth()+"/"+LocalDate.now().getYear());
            lists[i][2] ="0";
        }
        for(VaccineAdministration va: vaList){
            for(i=0; i<this.a; i++){
                if(va.getVaccinationCenter().getName() == lists[i][0]){
                    lists[i][2] = String.valueOf(Integer.parseInt(lists[i][2])+1);
                    vaList.remove(va);
                    varList.add(va);
                }
            }
        }
        return lists;
    }

    @Override
    public void run(){
        makeLists();
        dvrStore.dailySave(lists);
    }

}
