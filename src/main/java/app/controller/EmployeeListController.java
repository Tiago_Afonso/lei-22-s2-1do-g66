package app.controller;

import app.domain.model.Company;
import app.domain.shared.Constants;
import pt.isep.lei.esoft.auth.AuthFacade;
import pt.isep.lei.esoft.auth.mappers.dto.UserDTO;
import pt.isep.lei.esoft.auth.mappers.dto.UserRoleDTO;

import java.util.ArrayList;
import java.util.List;


public class EmployeeListController {

    private Company company;
    private AuthFacade authFacade;
    private App app;

    public EmployeeListController(){
        this.app = App.getInstance();
        this.company = app.getCompany();
        this.authFacade = this.company.getAuthFacade();
    }

    public List<String> getEmployees(){
        List<UserDTO> employeeList =  authFacade.getUsers();
        List<String> employeeFormattedList = new ArrayList<>();
        for (UserDTO user : employeeList){
            String userRole = "";
            for(UserRoleDTO roles : user.getRoles()){
                userRole = roles.getId();
            }

            if (userRole.equals(Constants.ROLE_SNSUSER)) {
                continue;
            }

            employeeFormattedList.add(String.format("Employee Name: %s %nEmployee ID: %s %nEmployee Role: %s %n------------------",user.getName(),user.getId(),userRole));
        }
        return employeeFormattedList;
    }
}
