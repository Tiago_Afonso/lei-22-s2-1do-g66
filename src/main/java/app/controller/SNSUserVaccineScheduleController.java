package app.controller;

import app.domain.model.*;
import app.domain.store.VaccineTypeStore;
import app.domain.store.SnsUserStore;
import app.domain.store.VaccinationCenterStore;
import app.domain.store.VaccineScheduleStore;


import java.sql.Array;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class SNSUserVaccineScheduleController {
    private final SnsUserStore userStore;
    private final VaccineScheduleStore vaccineScheduleStore;
    private final VaccineTypeStore vaccineTypeStore;
    private final VaccinationCenterStore vaccinationCenterStore;
    private ArrayList<VaccineSchedule> vsList = new ArrayList<>();
    private SnsUser snsUser;
    private VaccinationCenter vc;
    private VaccineType vt;
    private ArrayList<LocalDateTime> dtList = new ArrayList<>();
    private ArrayList<SnsUser> userList = new ArrayList<>();


    private VaccineType vt1;
    private VaccineType vt2;
    private VaccineType vt3;
    private VaccineType vt4;


    public SNSUserVaccineScheduleController (){ this(App.getInstance().getCompany());}

    public SNSUserVaccineScheduleController(Company company){
        userStore = company.getSnsUserStore();
        vaccineScheduleStore = company.getVaccineScheduleStore();
        vaccineTypeStore = company.getVaccineTypeStore();
        vaccinationCenterStore = company.getVaccinationCenterStore();
    }
    public List<VaccineType> getVaccineTypeList(){
        return this.vaccineTypeStore.getVaccineTypeList();
    }

    public List<VaccinationCenter> getVaccinationCenterList(){
        return this.vaccinationCenterStore.getVaccinationCenterList();
    }
    public ArrayList<LocalDateTime> getVaccineScheduleList(SnsUser snsUser, VaccinationCenter vc, VaccineType vt){
        //vsList = vaccineScheduleStore.getVaccineScheduleList();
        dtList = vaccineScheduleStore.calculateNextDates(snsUser, vc, vt);
        return dtList;
    }


    public SnsUser findSNSUser(String SNSUserNumber){
        userList = bootstrapUsers();
        snsUser = this.userStore.findUserByNumber(SNSUserNumber, userList);
            return snsUser;

    }
    public ArrayList<VaccinationCenter> bootstrapVaccinationCenters(){
        VaccinationCenter vc1 = new VaccinationCenter("Paranhos", "Rua Augusto Lessa 452", "centrovacParanhos@dgs.pt", "www.centrovacParanhos.pt",  LocalTime.of(8, 0), LocalTime.of(22, 0), 225033342, 223434545, 30, 1);
        ArrayList<VaccinationCenter> vcList = new ArrayList<>();
        vcList.add(vc1);
        VaccinationCenter vc2 = new VaccinationCenter("Bonfim", "Rua Da Constituição", "centrovacBonfim@dgs.pt", "www.centrovacBonfim.pt", LocalTime.of(8, 0),LocalTime.of(22, 0), 225033332, 223534545, 25, 1);
        vcList.add(vc2);
        return vcList;
    }
    public ArrayList<VaccineType> bootstrapVaccineTypes(){
        ArrayList<VaccineType> vtList = new ArrayList<>();
         this.vt1 = vaccineTypeStore.createVaccineType(00001, "Tetano");
        vtList.add(vt1);
         this.vt2 = vaccineTypeStore.createVaccineType(00002, "Gripe Sasonal");
        vtList.add(vt2);
        this.vt3 = vaccineTypeStore.createVaccineType(00003, "Gripe A");
        vtList.add(vt3);
        this.vt4 = vaccineTypeStore.createVaccineType(00004, "Covid-19");
        vtList.add(vt4);
        return vtList;

    }
    public ArrayList<VaccineSchedule> bootstrapSchedule(VaccinationCenter vc){
        ArrayList<VaccineSchedule> vsList = new ArrayList<>();
        VaccineSchedule vs1 = new VaccineSchedule(new SnsUser("André Ferreira", "Rua Bentos Do Caraça 301", "Male", "964055743", "andrefer435@gmail.com","Grulis",  LocalDate.of(100, 1, 31 ), "45456573", "15563708"),vt4, vc, LocalDateTime.of(122, 6, 3, 8, 0));
        vsList.add(vs1);
        VaccineSchedule vs2 = new VaccineSchedule(new SnsUser("Bruno Paiva", "Avenida Combatentes da Grande Guerra", "Male", "964035753", "Bpai_va@gmail.com","Baliambis",  LocalDate.of(100, 6, 14 ), "45476673", "15463008"),vt4, vc, LocalDateTime.of(122, 6, 3, 8, 0));
    vsList.add(vs2);
    return vsList;
    }
    public ArrayList<SnsUser> bootstrapUsers(){
        SnsUser su1 = new SnsUser("André Ferreira", "Rua Bentos Do Caraça 301", "Male", "964055743", "andrefer435@gmail.com","Grulis",  LocalDate.of(100, 1, 31 ), "45456573", "15563708");
        userList.add(su1);
        SnsUser su2 = new SnsUser("Bruno Paiva", "Avenida Combatentes da Grande Guerra", "Male", "964035753", "Bpai_va@gmail.com","Baliambis",  LocalDate.of(100, 6, 14 ), "45476673", "15463008");
        userList.add(su2);
        return userList;
    }

    }

