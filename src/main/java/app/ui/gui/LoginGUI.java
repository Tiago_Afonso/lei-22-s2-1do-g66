package app.ui.gui;

import app.controller.AuthController;
import app.domain.shared.Constants;
import app.ui.console.MenuItem;
import app.ui.console.utils.Utils;
import app.ui.gui.NurseGUI.NurseGUI;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import pt.isep.lei.esoft.auth.mappers.dto.UserRoleDTO;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;

public class LoginGUI implements Runnable {
    private final AuthController ctrl = new AuthController();
    private int maxTries = 3;
    @FXML
    private TextField TF_passwordTextField;

    @FXML
    private TextField TF_idTextField;

    @FXML
    void loginAction(ActionEvent event) {
        boolean success = doLogin();

        if (success) {
            List<UserRoleDTO> roles = this.ctrl.getUserRoles();
            if ((roles == null) || (roles.isEmpty())) {
                Alert alert = new Alert(Alert.AlertType.ERROR, "User has not any role assigned.");
                alert.showAndWait();
            } else {
                UserRoleDTO role = selectsRole(roles);
                if (!Objects.isNull(role)) {
                    List<MenuItem> rolesUI = getMenuItemForRoles();
                    this.redirectToRoleUI(rolesUI, role);
                } else {
                    Alert alert = new Alert(Alert.AlertType.ERROR, "User did not select a role.");
                    alert.showAndWait();
                }
            }
        }
        this.logout();
    }

    private boolean doLogin() {
        boolean success = ctrl.doLogin(TF_idTextField.getText().trim(), TF_passwordTextField.getText().trim());
        if (!success) {
            if (maxTries > 0) {
                maxTries--;
                Alert alert = new Alert(Alert.AlertType.ERROR, "Error: Invalid UserId and/or Password. \n You have " + maxTries + "more attempt(s).");
                alert.showAndWait();
            } else {
                MainApp.toMainMenu();
            }
        }
        return success;
    }

    private List<MenuItem> getMenuItemForRoles() {
        List<MenuItem> rolesUI = new ArrayList<>();
        rolesUI.add(new MenuItem(Constants.ROLE_NURSE, new NurseGUI()));
        return rolesUI;
    }

    private void logout() {
        ctrl.doLogout();
    }

    private void redirectToRoleUI(List<MenuItem> rolesUI, UserRoleDTO role) {
        boolean found = false;
        Iterator<MenuItem> it = rolesUI.iterator();
        while (it.hasNext() && !found) {
            MenuItem item = it.next();
            found = item.hasDescription(role.getDescription());
            if (found)
                item.run();
        }
        if (!found)
            System.out.println("There is no UI for users with role '" + role.getDescription() + "'");
    }

    private UserRoleDTO selectsRole(List<UserRoleDTO> roles) {
        if (roles.size() == 1)
            return roles.get(0);
        else
            return (UserRoleDTO) Utils.showAndSelectOne(roles, "Select the role you want to adopt in this session:");
    }

    @Override
    public void run() {
        Parent root;
        try {
            root = FXMLLoader.load(Objects.requireNonNull(getClass().getResource("/fxml/LoginGUI.fxml")));
            Scene scene = new Scene(root);
            Stage window = MainApp.STAGE;
            window.setScene(scene);
            window.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
