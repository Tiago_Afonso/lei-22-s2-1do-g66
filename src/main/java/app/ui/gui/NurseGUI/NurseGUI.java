package app.ui.gui.NurseGUI;

import app.ui.gui.MainApp;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

import java.io.IOException;
import java.util.Objects;


public class NurseGUI implements Runnable {

    @FXML public AnchorPane recordAdverseReactions;

    @FXML
    void logoutAction(ActionEvent event) {
        MainApp.toMainMenu();
    }

    @Override
    public void run() {
        Parent root;
        try {
            root = FXMLLoader.load(Objects.requireNonNull(getClass().getResource("/fxml/NurseGUI/NurseGUI.fxml")));
            Scene scene = new Scene(root);
            Stage window = MainApp.STAGE;
            window.setScene(scene);
            window.show();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
}
