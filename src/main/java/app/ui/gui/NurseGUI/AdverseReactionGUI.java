package app.ui.gui.NurseGUI;

import app.controller.AdverseReactionController;
import app.controller.RegisterSnsUserController;
import app.domain.model.SnsUser;
import app.domain.store.AdverseReactionStore;
import app.mappers.dto.AdverseReactionDto;
import app.mappers.dto.SnsUserDto;
import app.mappers.dto.SnsUserDtoV2;
import app.ui.gui.MainApp;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.time.LocalDate;
import java.util.Objects;
import java.util.ResourceBundle;

public class AdverseReactionGUI implements Runnable, Initializable {

    @FXML
    private TableView<SnsUserDtoV2> TV_SnsUsers;
    @FXML
    private TableColumn<SnsUserDtoV2, String> TC_Name;
    @FXML
    private TableColumn<SnsUserDtoV2, String> TC_SnsNumber;
    @FXML
    private TextField TF_Description;
    @FXML
    private TextField TF_SnsNumber;


    AdverseReactionController controller = new AdverseReactionController();
    RegisterSnsUserController registerSnsUserController = new RegisterSnsUserController();
    Alert a = new Alert(Alert.AlertType.NONE);
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        loadSnsUser();
        TC_Name.setCellValueFactory(new PropertyValueFactory<>("name"));
        TC_SnsNumber.setCellValueFactory(new PropertyValueFactory<>("snsNumber"));
        ObservableList<SnsUserDtoV2> data = FXCollections.observableArrayList(controller.getSnsUsers());
        TV_SnsUsers.setItems(data);
        TV_SnsUsers.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
    }

    @Override
    public void run() {
        Parent root;
        try {
            root = FXMLLoader.load(Objects.requireNonNull(getClass().getResource("/fxml/NurseGUI/RecordAdverseReactionsGUI.fxml")));
            Scene scene = new Scene(root);
            Stage window = MainApp.STAGE;
            window.setScene(scene);
            window.show();
        } catch (IOException exs) {
            exs.printStackTrace();
        }
    }

    @FXML
    public void recordAdverseReactionAction() {
        try {
            SnsUser snsUser = controller.getSnsUser(TF_SnsNumber.getText());
            if(snsUser == null) {
                throw new Exception("There is no user with selected SNS Number");
            }
            AdverseReactionDto adverseReactionDto = new AdverseReactionDto(snsUser, TF_Description.getText());
            controller.createAdverseReaction(adverseReactionDto);
            a.setAlertType(Alert.AlertType.INFORMATION);
            a.setContentText("Adverse reaction recorded");
            a.show();
        } catch (Exception e) {
            a.setAlertType(Alert.AlertType.ERROR);
            a.setContentText(e.getMessage());
            a.show();
        }
    }

    //trying to load SNS users in bootstrap causes and error, loading in UI as a workaround
    private void loadSnsUser() {
        SnsUserDto user1 = new SnsUserDto("Tiago Afonso", "Rua 1", "M", "987654321", "email1@email.com", "PASs23dasfs12", LocalDate.of(2002, 12, 24), "123456789", "12345678");
        registerSnsUserController.createSnsUser(user1);
        registerSnsUserController.saveSnsUser();
        SnsUserDto user2 = new SnsUserDto("Maria do Carmo", "Rua 2", "F", "987654322", "email2@email.com", "PASs23dasfs12", LocalDate.of(1999, 1, 4), "223456789", "22345678");
        registerSnsUserController.createSnsUser(user2);
        registerSnsUserController.saveSnsUser();
        SnsUserDto user3 = new SnsUserDto("Joana Mauricio", "Rua 3", "F", "987654323", "email3@email.com", "PASs23dasfs12", LocalDate.of(2010, 3, 5), "323456789", "32345678");
        registerSnsUserController.createSnsUser(user3);
        registerSnsUserController.saveSnsUser();
        SnsUserDto user4 = new SnsUserDto("Joao Tomas", "Rua 4", null, "987654324", "email4@email.com", "PASs23dasfs12", LocalDate.of(1970, 9, 11), "423456789", "42345678");
        registerSnsUserController.createSnsUser(user4);
        registerSnsUserController.saveSnsUser();
    }

    public void seeAdverseReactionsAction() {
        a.setAlertType(Alert.AlertType.INFORMATION);
        a.setAlertType(Alert.AlertType.ERROR);
        a.setContentText(controller.getAdverseReactions().replaceAll("]", "").replaceAll("\\[", ""));
        a.show();
    }
}
