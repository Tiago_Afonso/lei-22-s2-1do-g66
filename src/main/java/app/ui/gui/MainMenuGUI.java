package app.ui.gui;

import app.ui.console.DevTeamUI;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;


public class MainMenuGUI {

    @FXML
    void loginAction(ActionEvent event) {
        new LoginGUI().run();
    }

    @FXML
    void developmentTeamAction(ActionEvent event) {
        new DevTeamUI().run();
        Alert alert = new Alert(Alert.AlertType.INFORMATION, "Check the console output");
        alert.showAndWait();
    }
}
