package app.ui.gui;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;
import java.util.Objects;


public class MainApp extends Application implements Runnable {

    public static Stage STAGE;

    /**
     * Ignored
     * @param args
     */
    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage stage) throws Exception {

        STAGE = stage;
        stage.setOnCloseRequest(e -> stage.hide());
        stage.setTitle("DGS/SNS Application");
        stage.setMinWidth(stage.getMinWidth());
        stage.setMaxWidth(stage.getMaxWidth());
        toMainMenu();
    }

    @Override
    public void run() {
        if (STAGE == null) {
            Platform.setImplicitExit(false);
            new Thread(Application::launch).start();
        } else {
            Platform.runLater(MainApp::toMainMenu);
        }
    }

    public static void toMainMenu() {
        Parent root;
        try {
            root = FXMLLoader.load(Objects.requireNonNull(MainApp.class.getResource("/fxml/MainMenuGUI.fxml")));
            Scene scene = new Scene(root);
            STAGE.setScene(scene);
            STAGE.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
