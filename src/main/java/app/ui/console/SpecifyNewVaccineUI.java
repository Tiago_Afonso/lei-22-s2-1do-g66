package app.ui.console;
import app.controller.SpecifyNewVaccineController;
import app.domain.model.AgeGroup;
import app.domain.model.Dosage;
import app.domain.model.VaccineType;
import app.ui.console.utils.Utils;
import app.mappers.dto.VaccineTypeDTO;
import app.domain.model.Company;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class SpecifyNewVaccineUI implements Runnable{

    private String name;
    private String iD;
    private String brand;
    private VaccineType vType;
    private List<AgeGroup> ageGroups= new ArrayList<>();

    private int decision = 1;
    private int decision2 =1;

    private int upperAge;
    private int lowerAge;
    private int nrOfDoses;
    private int dosageq;

    private List<Integer> lDosageq= new ArrayList<>();

    private List<Integer> lTimeInterval= new ArrayList<>();
    private AgeGroup ageGroup;
    private Dosage dosage;
    private List<VaccineType> vaccineTypeList;
    private String designation;
    private Company company;

    private ArrayList<VaccineType> vtList = new ArrayList<>();


    private int loop;
     SpecifyNewVaccineController specifyNewVaccineController = new SpecifyNewVaccineController();


     public void run(){

         name = Utils.readLineFromConsole("Introduce the name");
         iD = Utils.readLineFromConsole("Introduce the iD");
         brand = Utils.readLineFromConsole("Introduce the brand");
         System.out.println("You will now be shown a list of Vaccine Types for the selection of one");

           vtList = specifyNewVaccineController.bootstrapVaccineTypes();
         System.out.println("You will now be shown the list of vaccine types for selection");
          vType = listForSelection(vtList);
      while(decision != 0){
          System.out.println("For all intended age groups you must introduce the data");
          lowerAge = Utils.readIntegerFromConsole("Introduce the lower age of the age group");
          upperAge = Utils.readIntegerFromConsole("Introduce the upper age of the age group");
          nrOfDoses = Utils.readIntegerFromConsole("Introduce the number of doses for the age group");
          for (loop =0; loop < nrOfDoses ; loop++){
              dosageq = Utils.readIntegerFromConsole("Introduce the dosage for the age group (mL)");

              lDosageq.add(dosageq);
          }

          for (loop =0; loop < nrOfDoses -1; loop++) {
              int timeInterval = Utils.readIntegerFromConsole("Introduce the time interval between doses(in days)");
              lTimeInterval.add(timeInterval);
          }
          Dosage dosage = specifyNewVaccineController.createDosage(nrOfDoses, lDosageq, lTimeInterval);
          AgeGroup ageGroup = specifyNewVaccineController.createAgeGroup(upperAge, lowerAge, dosage);
          ageGroups.add(ageGroup);
          decision = Utils.readIntegerFromConsole("Type 0 if you have finished introducing all of the age groups' information");

      }


         System.out.println(specifyNewVaccineController.createVaccine(name, iD, brand, vType, ageGroups));
         Utils.confirm("Y/N");
     }
    public VaccineType listForSelection(ArrayList<VaccineType> list){
        int y = 0;
        for( VaccineType object : list){
            System.out.println("#" + list.indexOf(object) + "Object" + object.toString() + "Is this your desired choice?");
        }
        y = Utils.readIntegerFromConsole("Introduce the number you want");
        VaccineType object = list.get(y);
        Utils.confirm(object.toString() + "Confirm this is the selection you wanted");
        return object;
    }
}


