package app.ui.console;

import app.ui.console.utils.Utils;

import java.util.ArrayList;
import java.util.List;

public class ReceptionistUI implements Runnable{

    public ReceptionistUI() {

    }
    public void run() {
        List<MenuItem> options = new ArrayList<MenuItem>();
        options.add(new MenuItem("Add New SNS User ", new RegisterSnsUserUI()));
        options.add(new MenuItem("SNS Users List ", new GetSnsUserListUI()));
        options.add(new MenuItem("Schedule a Vaccination ", new ScheduleVaccinationUI()));
        options.add(new MenuItem("Register SNS User arrival",new RegisterArrivalUI()));
        options.add(new MenuItem("Check Arrivals", new CheckArrivalsUI()));


        int option = 0;
        do {
            option = Utils.showAndSelectIndex(options, "\n\nReceptionist Menu:");

            if ((option >= 0) && (option < options.size())) {
                options.get(option).run();
            }
        }
        while (option != -1);
    }
}
