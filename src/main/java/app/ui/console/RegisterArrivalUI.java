package app.ui.console;

import app.controller.RegisterArrivalController;
import app.controller.RegisterVaccinationCenterController;
import app.domain.model.SnsUser;
import app.domain.model.VaccinationCenter;
import app.mappers.dto.RegisterArrivalDto;
import app.mappers.dto.VaccinationCenterDto;
import app.ui.console.utils.Utils;
import jdk.jshell.execution.Util;

import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class RegisterArrivalUI implements Runnable{
    RegisterVaccinationCenterController registerVaccinationCenterController = new RegisterVaccinationCenterController();
    RegisterArrivalController registerArrivalController = new RegisterArrivalController();
    private VaccinationCenter selectedVaccinationCenter;

    //FOR TESTING/BOOTSTRAP
    VaccinationCenterDto testVaccinationCenter = new VaccinationCenterDto("Centro 1", "Rua do Centro 45", "centro1@mail.com", "centro1.com", LocalTime.of(8, 0), LocalTime.of(20,0), 939393939, 20202, 50, 10);
    VaccinationCenterDto testVaccinationCenter2 = new VaccinationCenterDto("Centro 2", "Rua do Centro 45", "centro1@mail.com", "centro1.com", LocalTime.of(8,0), LocalTime.of(20,0), 939393939, 20202, 50, 10);
    {
        registerVaccinationCenterController.createVaccinationCenter(testVaccinationCenter);
        registerVaccinationCenterController.saveVaccinationCenter(testVaccinationCenter);
        registerVaccinationCenterController.createVaccinationCenter(testVaccinationCenter2);
        registerVaccinationCenterController.saveVaccinationCenter(testVaccinationCenter2);
    }
    //END OF BOOTSTRAP

    @Override
    public void run() {
        try{
            System.out.println("\nAll Vaccination Centers:\n");

            int index = 1;
            ArrayList<VaccinationCenter> centerList = new ArrayList<VaccinationCenter>();
            for (VaccinationCenter entry : registerVaccinationCenterController.getVaccinationcenterList()) {
                System.out.println(String.format("%d - %s",index, entry));
                centerList.add(entry);
                index++;
            }

            int option = 0;
            do{
                option = Utils.readIntegerFromConsole("Select a Vaccination Center: ");
                if ((option > 0) && (option <= centerList.size())){
                    option--;
                    selectedVaccinationCenter = centerList.get(option);
                    option = -1;
                }
            }while (option != -1);
            System.out.println(selectedVaccinationCenter);
            RegisterArrivalController registerArrivalController = new RegisterArrivalController();

            boolean toContinue = true;
            do{
                List<Integer> time = new ArrayList<>();
                String SNSNumber = Utils.readLineFromConsole("Enter SNS User number: ");

                SnsUser snsUser = registerArrivalController.getUserFromSNSNumber(SNSNumber);

                Date date = Utils.readDateFromConsole("Enter date of schedule (dd-MM-yyyy): ");

                time.add(Utils.readIntegerFromConsole("Enter hour of arrival (hh): "));
                time.add(Utils.readIntegerFromConsole("Enter minute of arrival (mm): "));

                RegisterArrivalDto registerArrivalDto = new RegisterArrivalDto(time,SNSNumber,selectedVaccinationCenter,snsUser,date);

                System.out.println(registerArrivalController.createArrival(registerArrivalDto));

                if (registerArrivalController.saveArrival()){
                    System.out.println("\nOperation successful");
                }

                toContinue = Utils.confirm("Register another User (S/N):");

            }while (toContinue);




        } catch (Exception exception) {
            System.out.println("Error: " + exception.getMessage());
        }


    }


}
