package app.ui.console;

import app.controller.ImportSnsUserSetFromCsvController;
import app.ui.console.utils.Utils;

public class ImportSnsUserSetFromCsvUI implements Runnable {

    ImportSnsUserSetFromCsvController importSnsUserSetFromCsvController = new ImportSnsUserSetFromCsvController();

    @Override
    public void run() {
        String fileLocation = Utils.readLineFromConsole("File Location: ");
        importSnsUserSetFromCsvController.readCsv(fileLocation);
    }
}
