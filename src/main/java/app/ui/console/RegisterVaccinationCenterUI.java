package app.ui.console;

import app.controller.RegisterVaccinationCenterController;
import app.domain.model.VaccinationCenter;
import app.mappers.dto.VaccinationCenterDto;
import app.ui.console.utils.Utils;

import java.time.LocalTime;


public class RegisterVaccinationCenterUI implements Runnable {
    private String name, adress, email, website;
    private LocalTime openingHour, closingHour;
    private int phoneNumber, faxNumber, slotDuration, vaccinePerSlot;

    RegisterVaccinationCenterController registerVaccinationCenterController = new RegisterVaccinationCenterController();


    @Override
    public void run() {
        try {
            name = Utils.readLineFromConsole("Name: ");
            adress = Utils.readLineFromConsole("Adress: ");
            email = Utils.readLineFromConsole("Email: ");
            website = Utils.readLineFromConsole("WebSite: ");
            openingHour = LocalTime.parse(Utils.readLineFromConsole("Opening Hour: "));
            closingHour = LocalTime.parse(Utils.readLineFromConsole("Closing Hour: "));
            phoneNumber = Utils.readIntegerFromConsole("Phone Number: ");
            faxNumber = Utils.readIntegerFromConsole("Fax Number: ");
            slotDuration = Utils.readIntegerFromConsole("Slot Duration: ");
            vaccinePerSlot = Utils.readIntegerFromConsole("Vaccine Per Slot: ");

            VaccinationCenterDto vaccinationCenterDto = new VaccinationCenterDto(name, adress, email, website, openingHour, closingHour, phoneNumber, faxNumber, slotDuration, vaccinePerSlot);
            System.out.println("\n");
            System.out.println(registerVaccinationCenterController.createVaccinationCenter(vaccinationCenterDto));

            Utils.confirm("Confirm (S/N)?\n");
            if (registerVaccinationCenterController.saveVaccinationCenter(vaccinationCenterDto)) {
                System.out.print("\nSucessfull\n");
            }
            else {
                System.out.println("Something went wrong! Vaccination Center not saved");
            }
        } catch (Exception exception) {
            System.out.println("Error: " + exception.getMessage());
        }
        System.out.println("\n-------------------------------------\n");
        System.out.println("\nAll Vaccination Centers:\n");
        System.out.println(registerVaccinationCenterController.getVaccinationCenterList());
    }
}
