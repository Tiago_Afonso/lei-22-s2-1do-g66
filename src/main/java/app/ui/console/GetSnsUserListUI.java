package app.ui.console;

import app.controller.RegisterSnsUserController;

public class GetSnsUserListUI implements Runnable{
    RegisterSnsUserController registerSnsUserController = new RegisterSnsUserController();

    @Override
    public void run() {
        System.out.println(registerSnsUserController.getSnsUsers());
    }
}
