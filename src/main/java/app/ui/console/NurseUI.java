package app.ui.console;

import app.ui.console.utils.Utils;

import java.util.ArrayList;
import java.util.List;

public class NurseUI implements Runnable{
    public NurseUI()
    {
    }

    public void run()
    {
        List<MenuItem> options = new ArrayList<MenuItem>();
        options.add(new MenuItem("US005 - Consult the users in the waiting room of a vacination center.", new ListSNSUsersWaitingRoomUI()));
        options.add(new MenuItem("US008 - Register the administration of a vaccine", new VaccineAdministrationUI()));
        options.add(new MenuItem("Other options will soon be added..... Loading.... ", new ShowTextUI("Unavailable option!!!")));

        int option = 0;
        do
        {
            option = Utils.showAndSelectIndex(options, "\n\nNurse Menu:");

            if ( (option >= 0) && (option < options.size()))
            {
                options.get(option).run();
            }
        }
        while (option != -1 );
    }
}