package app.ui.console;

import app.controller.SpecifyNewVaccineTypeController;
import app.domain.model.VaccineType;
import app.domain.store.VaccineTypeStore;
import app.ui.console.utils.Utils;

import java.util.ArrayList;
import java.util.List;

public class SpecifyNewVaccineTypeUI implements Runnable {
    int code;
    int iD;
    String designation;
    VaccineTypeStore vtStore = new VaccineTypeStore();


    public List<VaccineType> bootstrapVaccineTypes(){
        List<VaccineType> vtList = new ArrayList<>();
        VaccineType vt1 = vtStore.createVaccineType(00001, "Tetano");
        vtList.add(vt1);
        VaccineType vt2 = vtStore.createVaccineType(00002, "Gripe Sasonal");
        vtList.add(vt2);
        VaccineType vt3 = vtStore.createVaccineType(00003, "Gripe A");
        vtList.add(vt3);
        VaccineType vt4 = vtStore.createVaccineType(00004, "Covid-19");
        vtList.add(vt4);
        return vtList;

    }
    SpecifyNewVaccineTypeController specifyNewVaccineTypeController = new SpecifyNewVaccineTypeController();
    public void run(){
        List<VaccineType> vtList;
       vtList = bootstrapVaccineTypes();
       code = Utils.readIntegerFromConsole("Introduce the code");
       designation = Utils.readLineFromConsole("Introduce the designation");


        System.out.println(specifyNewVaccineTypeController.createVaccineType(code, designation));
    }

}
