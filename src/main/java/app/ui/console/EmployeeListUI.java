package app.ui.console;

import app.controller.EmployeeListController;
import pt.isep.lei.esoft.auth.mappers.dto.UserDTO;

public class EmployeeListUI implements Runnable{
    EmployeeListController employeeList = new EmployeeListController();

    @Override
    public void run() {
        for (String i : employeeList.getEmployees()){
            System.out.println(i);
        }

    }
}
