package app.ui.console;

import app.controller.ScheduleVaccinationController;
import app.domain.shared.Constants;
import app.mappers.dto.SnsUserDto;
import app.ui.console.utils.Utils;

import java.time.ZoneId;
import java.util.Arrays;
import java.util.Date;

public class ScheduleVaccinationUI implements Runnable {

    public ScheduleVaccinationUI() {

    }

    public void run() {
        try {
            Date birthDate = Utils.readDateFromConsole("Birth Date (dd-MM-yyyy): ");
            String snsNumber = Utils.readLineFromConsole("SNS Number: ");

            if(ScheduleVaccinationController.ageverify()){
                System.out.println("User does fulfill the requirements");
            }else{
                System.out.println("User does not fulfill the requirements");
            }

        } catch (Exception exception) {
            System.out.println("Error: " + exception.getMessage());
        }
    }
}
