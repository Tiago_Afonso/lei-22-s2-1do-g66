package app.ui.console;

import app.controller.AdverseReactionController;
import app.mappers.dto.SnsUserDtoV2;

import java.util.List;

public class TestUI implements Runnable{

    @Override
    public void run() {
        AdverseReactionController controller = new AdverseReactionController();
        List<SnsUserDtoV2> x = controller.getSnsUsers();
        System.out.println(x.get(0).getName() + " " + x.get(0).getSnsNumber());
    }
}
