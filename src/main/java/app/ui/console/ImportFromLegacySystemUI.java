package app.ui.console;

import app.controller.App;
import app.controller.ImportFromLegacySystemController;
import app.domain.model.*;
import app.domain.store.AgeGroupStore;
import app.domain.store.DosageStore;
import app.domain.store.VaccineStore;
import app.domain.store.VaccineTypeStore;
import app.ui.console.utils.Utils;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class ImportFromLegacySystemUI implements Runnable{

    private ImportFromLegacySystemController ctrl = new ImportFromLegacySystemController();
    private VaccineStore vstore = App.getInstance().getCompany().getVaccineStore();
    private VaccineTypeStore vtStore = App.getInstance().getCompany().getVaccineTypeStore();
    private AgeGroupStore agStore = App.getInstance().getCompany().getAgeGroupStore();
    private DosageStore dStore = App.getInstance().getCompany().getDosageStore();

    @Override
    public void run() {

        importInformation();

    }

    public void importInformation(){

        List<InformationLegacySystem> list = new ArrayList<>();
        String file = null;
        String path = Utils.readLineFromConsole("File Location: ");

        int alg = Utils.readIntegerFromConsole("1. Using the bubble sort algorithm" + "\n2. Using the selection sort algorithm");

        int att = Utils.readIntegerFromConsole("1. Information sorted by arrival time" + "\n2. Information sorted by leaving time");

        int order = Utils.readIntegerFromConsole("1. Ascending order" + "\n2. Descending order");

        list = ctrl.informationSorted(path, att, alg, order);
        if(alg == 1){ // bubble sort
            if(att == 1) { // arrival
                file = "src/main/resources/BubbleSortArrivalDate.txt";
                //ctrl.writeToFile(list, "src/main/resources/BubbleSortArrivalDate.txt");
            } else {
                file = "src/main/resources/BubbleSortLeavingDate.txt";
                //ctrl.writeToFile(list, "src/main/resources/BubbleSortLeavingDate.txt");
            }
        } else {
            if(att == 1) { // arrival
                file = "src/main/resources/SelectionSortArrivalDate.txt";
                //ctrl.writeToFile(list, "src/main/resources/SelectionSortArrivalDate.txt");
            } else{
                file = "src/main/resources/SelectionSortLeavingDate.txt";
                //ctrl.writeToFile(list, "src/main/resources/SelectionSortLeavingDate.txt");
            }
        }
        //list = ctrl.getInformation();
        for (InformationLegacySystem i : list) {
            System.out.print("\nSNS User Name: " + ctrl.getUserName(i.getSnsUserNumber()));
            System.out.println(i.toString());
            System.out.println("Vaccine type description: " + ctrl.getVaccineTypeDesc(i.getVaccineName()));
        }

        int option = Utils.readIntegerFromConsole("Is the data correct?" + "\nPress 1 to yes, 0 to no:");

        if(option == 1){
            ctrl.writeToFile(list, file);
            otherCsv();
        } else{
            otherCsv();
        }
    }

    public void otherCsv(){
        int selectedOption;

        do {
            selectedOption = Utils.readIntegerFromConsole("Do you want to import another csv file?" + "\nPress 1 to yes, 0 to no:");

        } while (selectedOption != 1 && selectedOption != 0);

        if (selectedOption == 1){
            run();
        }
    }
}
