package app.ui.console;

//algorithm that finds the contiguous subsequence with maximum sum

import app.controller.MdiscController;

import java.awt.*;
import java.io.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.*;
import java.util.List;

public class MdiscUI implements Runnable {

    ArrayList<Integer> toPrint =  new ArrayList<>();
    ArrayList<Integer> finalList = new ArrayList<>();
    ArrayList<Integer> savedList = new ArrayList<>();

    class SerializedLists implements Serializable {
        private static final long serialVersionUID = 20000;
        Serializable finalList;
        Serializable toPrint2;

        public Serializable getFinalList() {
            return finalList;
        }

        public Serializable getToPrint2() {
            return toPrint2;
        }

        public SerializedLists(List finalList, List toPrint2) {
            this.finalList = (Serializable) finalList;
            this.toPrint2 = (Serializable) toPrint2;
        }
    }

    public void run() {
        LocalTime openHour = LocalTime.of(8,00);
        //ArrayList<Integer> finalList = new ArrayList<>();


            //interface - pedido de dados
            System.out.println("Chose to: \n1- load file with data\n2-load serialized file\n3-save serialized file");
            Scanner optionMenuMdiscScanner = new Scanner(System.in);
            int optionMenuMdisc = optionMenuMdiscScanner.nextInt();
            switch (optionMenuMdisc) {
                case 1: {
                    int timeIntervalOption;
                    MdiscController mdiscController = new MdiscController();
                    do {
                        List<LocalDateTime> arrivalTimeList = mdiscController.getArrivalTimeList();
                        List<LocalDateTime> departureTimeList = mdiscController.getDepartureTimeList();
                        Scanner timeInterval = new Scanner(System.in);
                        System.out.println("\n\nChoose the time intervals (in minutes) to analyze the perfomance of a center. (m)\nm = 30 \nm = 20 \nm = 10 \nm = 5 \nm = 1 \n");
                        timeIntervalOption = timeInterval.nextInt();

                    } while (timeIntervalOption != 1 && timeIntervalOption != 5 && timeIntervalOption != 10 && timeIntervalOption != 20 && timeIntervalOption != 30);

                    int maxLoop = mdiscController.determineMaxLoop(timeIntervalOption);
                    ArrayList<LocalTime> limitTimeList = mdiscController.determineLimitTimeList(openHour, timeIntervalOption, maxLoop);
                    finalList = mdiscController.determineFinalList(limitTimeList, timeIntervalOption);

                    System.out.println("Lista de input ao algoritmo:");
                    System.out.println(finalList);
                    System.out.println("\nSublista contígua de soma máxima de output:");
                    int[] result = MdiscController.Max(finalList);
                    List<Integer> finalInterval = new ArrayList<>();
                    for (int i = 0; i < result.length; i++) {
                         finalInterval.add(result[i]);
                    }

                    toPrint = new ArrayList<>();
                    for (int i = 0; i < result.length; i++) {
                        toPrint.add(result[i]);
                    }
                    System.out.println(toPrint);

                    int somaTotal = 0;
                    for (int i : toPrint) {
                        somaTotal += i;
                    }
                    System.out.println(String.format("Soma: %d", somaTotal));
                    ArrayList<LocalDateTime> toPrint2 = mdiscController.determineIntervaloDeTempos(finalList, toPrint, openHour, timeIntervalOption);
                    System.out.println(String.format("Intervalo de tempos: %s", toPrint2.toString()));
                    System.out.println("\n");
                    run();
                }
                case 2 : {
                    try {
                        FileOutputStream fileOut =
                                new FileOutputStream("mdisc.ser");
                        ObjectOutputStream out = new ObjectOutputStream(fileOut);
                        // System.out.println(finalList);
                        // System.out.println(toPrint);
                        out.writeObject(toPrint);
                        out.close();
                        fileOut.close();
                        System.out.printf("Serialized data is saved in mdisc.ser");
                        for (int j = 0; j < toPrint.size(); j++) {
                            savedList.add(toPrint.get(j));
                        }
                        savedList.add(null);
                    } catch (IOException i) {
                        i.printStackTrace();
                    }
                    System.out.println("\n");
                    run();
                }
                case 3 : {
                    ArrayList<Integer> listObjects = new ArrayList<>();
                    try {
                        FileInputStream fileIn = new FileInputStream("mdisc.ser");
                        ObjectInputStream in = new ObjectInputStream(fileIn);
                        listObjects = (ArrayList<Integer>) in.readObject();
                        in.close();
                        fileIn.close();
                    } catch (IOException i) {
                        i.printStackTrace();
                        return;
                    } catch (ClassNotFoundException c) {
                        System.out.println("Mdisc class not found");
                        c.printStackTrace();
                        return;
                    }
                    System.out.println("Deserialized Lists Mdisc");
                    System.out.println("\nLast loaded data:");
                    for ( int i = 0; i < listObjects.size(); i++) {
                        if(i != 0) {
                            System.out.print(",");
                        }
                        System.out.print(listObjects.get(i));
                    }
                    System.out.println("\nAll saved data:");
                    System.out.println(savedList);
                    //System.out.println(MdiscController.intListToArray(listObjects).lengh);
                    System.out.println("\n");
                    run();
                }
            }
    }
}
