package app.ui.console;

import app.controller.RecordDailyVaccinesController;
import app.ui.console.utils.Utils;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Paulo Maio <pam@isep.ipp.pt>
 */

public class AdminUI implements Runnable{
    public AdminUI()
    {
    }

    public void run()
    {
        List<MenuItem> options = new ArrayList<MenuItem>();
        options.add(new MenuItem("US009 - Register a vaccination Center", new RegisterVaccinationCenterUI()));
        options.add(new MenuItem("US012 - Register a new vaccine type", new SpecifyNewVaccineTypeUI()));
        options.add(new MenuItem("US013 - Register a new vaccine ", new SpecifyNewVaccineUI()));
        options.add(new MenuItem("US011 - Get a list of employees", new EmployeeListUI()));
        options.add(new MenuItem("US006 - Serialize daily number of people vaccinated", new RecordDailyVaccinesController()));
        options.add(new MenuItem("Import SNS Users from CSV ", new ImportSnsUserSetFromCsvUI()));

        int option = 0;
        do
        {
            option = Utils.showAndSelectIndex(options, "\n\nAdmin Menu:");

            if ( (option >= 0) && (option < options.size()))
            {
                options.get(option).run();
            }
        }
        while (option != -1 );
    }
}
