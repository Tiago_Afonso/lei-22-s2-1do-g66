package app.ui.console;

/**
 *
 * @author Paulo Maio <pam@isep.ipp.pt>
 */
public class DevTeamUI implements Runnable{

    public DevTeamUI()
    {

    }
    public void run()
    {
        System.out.println("\n");
        System.out.printf("Development Team:\n");
        System.out.printf("\t Joao Tender Dias - 1201463@isep.ipp.pt \n");
        System.out.printf("\t Tiago Afonso - 1201305@isep.ipp.pt \n");
        System.out.printf("\t Francisco Marques - 1201850@isep.ipp.pt \n");
        System.out.printf("\t João Torres - 1201424@isep.ipp.pt \n");
        System.out.printf("\t Student Name 5 - 120XXXX@isep.ipp.pt \n");
        System.out.println("\n");
    }
}
