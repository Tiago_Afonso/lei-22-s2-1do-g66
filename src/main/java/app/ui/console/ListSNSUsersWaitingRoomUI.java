package app.ui.console;

import app.controller.ListSNSUsersWaitingRoomController;
import app.controller.RegisterArrivalController;
import app.domain.model.RegisterArrival;
import app.domain.model.VaccinationCenter;
import app.controller.RegisterVaccinationCenterController;
import app.mappers.dto.VaccinationCenterDto;
import app.ui.console.utils.Utils;

import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


public class ListSNSUsersWaitingRoomUI implements Runnable {
    private String name, adress, email, website;
    private int phoneNumber, faxNumber;
    private int searchOption;
    ListSNSUsersWaitingRoomController listSNSUsersWaitingRoomController = new ListSNSUsersWaitingRoomController();
    RegisterVaccinationCenterController registerVaccinationCenterController = new RegisterVaccinationCenterController();
    RegisterArrivalController registerArrivalController = new RegisterArrivalController();

    public void MenuType() {
        do {
            if (registerVaccinationCenterController.getVaccinationcenterList().size() == 0) {
                String name0= "joao";
                String adress0= "rua do";
                String email0= "email@mail.com";
                String website0= "website.com";
                LocalTime openingHour0= LocalTime.of(8,00,00);
                LocalTime closingHour0= LocalTime.of(20,00,00);
                int phoneNumber0= 939393939;
                int faxNumber0= 20202;
                int slotDuration0= 50;
                int vaccinePerSlot0= 10;
                VaccinationCenterDto vaccinationCenterDto0 = new VaccinationCenterDto(name0, adress0, email0, website0, openingHour0, closingHour0, phoneNumber0, faxNumber0, slotDuration0, vaccinePerSlot0);
                registerVaccinationCenterController.createVaccinationCenter(vaccinationCenterDto0);
                registerVaccinationCenterController.saveVaccinationCenter(vaccinationCenterDto0);
                String name1= "joao1";
                String adress1= "rua do1";
                String email1= "email@mail.com1";
                String website1= "website.com1";
                LocalTime openingHour1= LocalTime.of(10,00,00);
                LocalTime closingHour1= LocalTime.of(22,00,00);
                int phoneNumber1= 939393999;
                int faxNumber1= 20200;
                int slotDuration1= 60;
                int vaccinePerSlot1= 11;
                VaccinationCenterDto vaccinationCenterDto1 = new VaccinationCenterDto(name1, adress1, email1, website1, openingHour1, closingHour1, phoneNumber1, faxNumber1, slotDuration1, vaccinePerSlot1);
                registerVaccinationCenterController.createVaccinationCenter(vaccinationCenterDto1);
                registerVaccinationCenterController.saveVaccinationCenter(vaccinationCenterDto1);
            }
            System.out.println("|||||||||||||||||| VACCINATION CENTERS LIST ||||||||||||||||||");
            if (registerVaccinationCenterController.getVaccinationcenterList().equals(null)) {
                System.out.println("Attention!!!  No Vaccination Centers Available!   \n");
                new NurseUI().run();
            }
            else {
                System.out.println(registerVaccinationCenterController.getVaccinationCenterList());
            }
            System.out.println("|||||||||||||||||| SEARCH MENU ||||||||||||||||||");
            System.out.println("\nSEARCH BY ONE OF THESE OPTIONS:");
            System.out.println("1- Name");
            System.out.println("2- Address");
            System.out.println("3- Email");
            System.out.println("4- Website");
            System.out.println("5- PhoneNumber");
            System.out.println("6- FaxNumber");
            System.out.println("Exit / Cancel - 0");
            searchOption = Utils.readIntegerFromConsole("Choose your option:");
            switch(searchOption) {
                case 1:
                    name = Utils.readLineFromConsole("Search by Name: ");
                    if (name.equals("0")) {
                        new NurseUI().run();
                    }
                    break;
                case 2:
                    adress = Utils.readLineFromConsole("Search by Address:");
                    if (adress.equals("0")) {
                        new NurseUI().run();
                    }
                    break;
                case 3:
                    email = Utils.readLineFromConsole("Search by Email:");
                    if (email.equals("0")) {
                        new NurseUI().run();
                    }
                    break;
                case 4:
                    website = Utils.readLineFromConsole("Search by WebSite:");
                    if (website.equals("0")) {
                        new NurseUI().run();
                    }
                    break;
                case 5:
                    phoneNumber = Utils.readIntegerFromConsole("Search by Phone Number:");
                    if (phoneNumber == 0) {
                        new NurseUI().run();
                    }
                    break;
                case 6:
                    faxNumber = Utils.readIntegerFromConsole("Search by Fax Number:");
                    if (faxNumber == 0) {
                        new NurseUI().run();
                    }
                    break;
            }
        }while(false);
    }

    @Override
    public void run() {
        try {
            MenuType();
            for (VaccinationCenter vc : registerVaccinationCenterController.getVaccinationcenterList()) {
                if (vc.equals(new VaccinationCenter(new VaccinationCenterDto(name, adress, email, website, LocalTime.of(00,00,00), LocalTime.of(00,00,00), phoneNumber, faxNumber, 0, 0)))) {
                    System.out.println("\nProcura o seguinte Centro de Vacinação?");
                    System.out.println(vc);
                    String option0;
                    option0 = Utils.readLineFromConsole("sim / nao:");
                    if ((option0.equals("nao")) | (option0.equals("não"))) {
                        run();
                    } else if ((option0.equals("sim")) | (option0.equals("SIM"))){
                        List<RegisterArrival> data = registerArrivalController.getArrivalsData();
                        if (data.size() == 0) {
                            System.out.println("\n\nNão há clientes disponíveis!!!\nSerá apresentada lista de datas e horas provisória.\n\n");
                            int option;
                            System.out.println("1- Ver lista provisória\n2- Sair da página");
                            option = Utils.readIntegerFromConsole("Choose your option:");
                            switch (option) {
                                case 1:
                                    sortDateTimeObject();
                                    break;
                                case 2:
                                    break;
                            }
                        } else {
                            System.out.println(data);
                        }
                        System.out.println(registerArrivalController.getArrivals());
                    } else {

                    }
                }
            }

                System.out.println("\n");
                if (Utils.confirm("Exit this page (S/N)?\n") == false) {
                    System.out.println("Something went wrong!");
                } else {
                    System.out.print("\nSucessfull\n");
                }
                System.out.println("\n--------------------------------------------------------------------------\n");

            } catch (Exception exception) {
                System.out.println("Error: " + exception.getMessage());
            }
    }

    private static void sortDateTimeObject() {
        List<LocalDateTime>dateTimeList = new ArrayList<LocalDateTime>();
        dateTimeList.add(LocalDateTime.of(2022, 05, 29, 7, 16));
        dateTimeList.add(LocalDateTime.of(2022, 05, 29, 12, 16));
        dateTimeList.add(LocalDateTime.of(2022, 05, 29, 8, 25));
        dateTimeList.add(LocalDateTime.of(2022, 05, 29, 13, 16));
        dateTimeList.add(LocalDateTime.of(2022, 05, 29, 20, 25));
        dateTimeList.add(LocalDateTime.of(2022, 05, 23, 11, 26));
        dateTimeList.add(LocalDateTime.of(2022, 05, 13, 12, 36));
        dateTimeList.add(LocalDateTime.of(2022, 5, 17, 16, 16));
        dateTimeList.add(LocalDateTime.of(2022, 5, 19, 21, 26));
        dateTimeList.add(LocalDateTime.of(2012, 2, 10, 10, 10));

        DateTimeFormatter dateTimeFormatter = DateTimeFormatter
                .ofPattern("MM/dd/yyyy '@'hh:mm a");
        System.out.println("SEM ORDEM (MM/dd/yyyy '@'hh:mm a)");
        for(LocalDateTime dateTime:dateTimeList)
        {
            System.out.println(dateTime.format(dateTimeFormatter));
        }
        Collections.sort(dateTimeList);
        System.out.println("\n--------------------------------------------------------------------------\n");
        System.out.println("ORDEM DE CHEGADA (MM/dd/yyyy '@'hh:mm a)");
        for(LocalDateTime dateTime:dateTimeList)
        {
            System.out.println(dateTime.format(dateTimeFormatter));
        }
    }
}