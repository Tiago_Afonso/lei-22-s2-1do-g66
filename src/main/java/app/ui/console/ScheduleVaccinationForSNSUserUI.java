package app.ui.console;

import app.controller.SNSUserVaccineScheduleController;
import app.domain.model.*;
import app.ui.console.utils.Utils;


import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class ScheduleVaccinationForSNSUserUI<T> implements Runnable {
    private String snsNumber;
    private VaccinationCenter vc;
    private VaccineType vt;
    private LocalDateTime date;
    private SnsUser user;
    private VaccineSchedule vs;
    private ArrayList<SnsUser> userList;
    private ArrayList<VaccineSchedule> vsList;
    private ArrayList<LocalDateTime> dtList = new ArrayList<>();
    private List<VaccineType> vtList = new ArrayList<>();
    private List<VaccinationCenter> vcList = new ArrayList<>();
    SNSUserVaccineScheduleController cont = new SNSUserVaccineScheduleController();


    @Override
    public void run() {
        vtList = cont.bootstrapVaccineTypes();
        vcList = cont.bootstrapVaccinationCenters();
        vsList = cont.bootstrapSchedule(vc);
        userList = cont.bootstrapUsers();
        try {
             snsNumber = Utils.readLineFromConsole("Please Introduce your SNS Number: ");
             user=cont.findSNSUser(snsNumber);
        } catch (Exception exception) {
            System.out.println("Error: " + exception.getMessage());
        }
        while (cont.findSNSUser(snsNumber) == null) {
            System.out.println("SNS number not found, please try again");
            snsNumber = Utils.readLineFromConsole("Please Introduce your SNS Number: ");
        }
            user = cont.findSNSUser(snsNumber);


        vc = selectionOfVC();
        vt = selectionofVT();
        dtList = cont.getVaccineScheduleList(user,  vc,  vt);
        date = selectionofDT();
        vs = new VaccineSchedule(user, vt, vc, date);
        Utils.confirm(vs.toString() + "\n" + "Is this your desired vaccination schedule?");

    }

    public VaccinationCenter selectionOfVC() {
        System.out.println("You will now be shown the list of vaccination centers for selection");
        vc = (VaccinationCenter) listForSelection((ArrayList<T>) this.vcList);
        return vc;
    }

    public VaccineType selectionofVT() {
        System.out.println("You will now be shown the list of vaccine types for selection");
        vt = (VaccineType) listForSelection((ArrayList<T>) this.vtList);
        return vt;
    }

    public LocalDateTime selectionofDT() {
        System.out.println("You will now be shown the list of dates for selection");
        date = listForSelectionDT(this.dtList);


return date;
    }
   public T listForSelection(ArrayList<T> list){
        int y,count = 0;
       for( T object : list){

            System.out.println("#" + count + " Object "+ object.toString() + " ");
            count++;
       }
       y = Utils.readIntegerFromConsole("Introduce the number you want");
       T object = list.get(y);
       Utils.confirm(object.toString() + " \n Confirm this is the selection you wanted");
       return object;
   }
   public LocalDateTime listForSelectionDT(ArrayList<LocalDateTime> list){
        int y,count = 0;
        LocalDateTime dt;
        for (LocalDateTime object : list){

            System.out.println("#" + count + "Date" +object.toString()+ " ");
            count++;
        }
        y= Utils.readIntegerFromConsole("Introduce the number you want");
        dt = list.get(y);
        Utils.confirm(dt.toString() + "\n Confirm this is the selection you wanted");
        return dt;
   }

}