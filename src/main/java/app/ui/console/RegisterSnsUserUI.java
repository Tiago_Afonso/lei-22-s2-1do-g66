package app.ui.console;

import app.controller.RegisterSnsUserController;
import app.domain.model.PasswordGenerator;
import app.domain.shared.Constants;
import app.mappers.dto.SnsUserDto;
import app.mappers.dto.VaccinationCenterDto;
import app.ui.console.utils.Utils;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.*;

public class RegisterSnsUserUI implements Runnable {

    RegisterSnsUserController registerSnsUserController = new RegisterSnsUserController();
    PasswordGenerator passwordGenerator = new PasswordGenerator();



    @Override
    public void run() {
        try {
            String name = Utils.readLineFromConsole("Name: ");
            String address = Utils.readLineFromConsole("Address: ");
            String sex = (String) Utils.showAndSelectOne(Arrays.asList(Constants.SEX.all.toArray()), "\nSex: ");
            String phoneNumber = Utils.readLineFromConsole("Phone Number: ");
            String email = Utils.readLineFromConsole("Email: ");
            String password = passwordGenerator.generatePassword(7, 3, 2);
            Date birthDate = Utils.readDateFromConsole("Birth Date (dd-MM-yyyy): ");
            String snsNumber = Utils.readLineFromConsole("SNS Number: ");
            String ccNumber = Utils.readLineFromConsole("CC Number: ");

            SnsUserDto snsUserDto = new SnsUserDto(name, address, sex, phoneNumber, email, password, birthDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDate(), snsNumber, ccNumber);
            System.out.println(registerSnsUserController.createSnsUser(snsUserDto));

            Utils.confirm("Confirm (S/N)?");

            if (registerSnsUserController.saveSnsUser()) {
                System.out.println("Operation successful");
            }

        } catch (Exception exception) {
            System.out.println("Error: " + exception.getMessage());
        }
    }
}
