package app.ui.console;

import app.controller.*;
import app.domain.model.*;
import app.mappers.dto.VaccineAdministrationDto;
import app.ui.console.utils.Utils;
import java.time.LocalDateTime;
public class VaccineAdministrationUI implements Runnable{

    VaccineAdministrationController vaccineAdministrationController = new VaccineAdministrationController();
    WaitingRoomController waitingRoomController = new WaitingRoomController();

    public void run() {

        try {
            SnsUser user;
            Vaccine vaccine;
            LocalDateTime timeAdministered;
            Dosage dosage;

            do {
                if (waitingRoomController.getWaitingRoomList().size() == 0) {
                    System.out.println("There's no users in the waiting room!");
                    break;
                }
                System.out.println("Next user in the waiting room:");
                System.out.println(waitingRoomController.getWaitingRoomList().get(0)); //show user data and adverse reactions
                user = waitingRoomController.getWaitingRoomList().get(0).getUser();
                boolean toContinue = Utils.confirm("Confirm? (S/N)");
                if (!toContinue) {break;}

                do {
                    System.out.println("List of vaccines in the system:");

                    for (int i = 0; i < vaccineAdministrationController.getAvailableVaccines().size(); i++) {
                        System.out.printf("%d - %s%n", i, vaccineAdministrationController.getAvailableVaccines().get(i));
                    }

                    int input = Utils.readIntegerFromConsole("Select the vaccine administered to the user:");

                    if (input < vaccineAdministrationController.getAvailableVaccines().size()) {
                        vaccine = vaccineAdministrationController.getAvailableVaccines().get(input);
                        break;
                    }

                } while (true);

                timeAdministered = LocalDateTime.now();

                do {
                    System.out.println("List of dosages in the system:");
                    for (int i = 0; i < vaccineAdministrationController.getDosages().size(); i++) {
                        System.out.printf("%d - %s%n", i, vaccineAdministrationController.getDosages().get(i));
                    }

                    int input = Utils.readIntegerFromConsole("Select the respective dosage for the vaccine" +
                            " administered to the user:");

                    if (input < vaccineAdministrationController.getDosages().size()) {
                        dosage = vaccineAdministrationController.getDosages().get(input);
                        break;
                    }

                } while (true);


                if (user == null || vaccine == null || dosage == null) {
                    throw new IllegalArgumentException("One of the fields is empty.");
                }

                VaccineAdministrationDto vaccineAdministrationDto = new VaccineAdministrationDto(user, vaccine,
                        timeAdministered, dosage, waitingRoomController.getWaitingRoomList().get(0).getVaccinationCenter());

                VaccineAdministration vaccineAdministration = vaccineAdministrationController.createAdministration(vaccineAdministrationDto);

                System.out.println(vaccineAdministration);

                boolean toContinue2 = Utils.confirm("Confirm info? (S/N)");
                if (!toContinue2){break;}

                if (vaccineAdministrationController.saveAdministration(false)) {
                    System.out.println("Operation successful");
                }

                if (vaccineAdministrationController.removeWaitingRoomListEntry(vaccineAdministration)) {
                    System.out.println("Waiting room list updated!");
                } else {
                    System.out.println("There was no such user in the waiting room");
                }

                vaccineAdministrationController.notifyUser(VaccineAdministrationController.NOTIFICATION_TIME,vaccineAdministration);

                boolean toContinue3 =  Utils.confirm("Register next user's administration? (S/N)");
                if (!toContinue3){break;}

            } while (true);

        }catch (Exception exception){
            System.out.println("Error: " + exception.getMessage());
        }

    }
}
