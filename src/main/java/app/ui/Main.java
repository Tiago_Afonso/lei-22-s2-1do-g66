package app.ui;

import app.ui.console.MainMenuUI;
import app.ui.gui.MainApp;

/**
 *
 * @author Paulo Maio <pam@isep.ipp.pt>
 */

public class Main {

    public static void main(String[] args)
    {
        try
        {
            MainMenuUI menu = new MainMenuUI();
            menu.run();
            //MainApp.main(new String[]{});
        }
        catch( Exception e )
        {
            e.printStackTrace();
        }
    }
}
