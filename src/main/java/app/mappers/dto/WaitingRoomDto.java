package app.mappers.dto;

import app.domain.model.SnsUser;
import app.domain.model.VaccinationCenter;

import java.time.LocalDateTime;

public class WaitingRoomDto {

    private final SnsUser user;
    private final LocalDateTime dateTimeArrived;
    private final VaccinationCenter vaccinationCenter;

    public WaitingRoomDto(SnsUser user, LocalDateTime dateTimeArrived, VaccinationCenter vaccinationCenter) {
        this.user = user;
        this.dateTimeArrived = dateTimeArrived;
        this.vaccinationCenter = vaccinationCenter;
    }

    public SnsUser getUser() {return user;}

    public LocalDateTime getDateTimeArrived() {return dateTimeArrived;}

    public VaccinationCenter getVaccinationCenter() {return vaccinationCenter;}
}
