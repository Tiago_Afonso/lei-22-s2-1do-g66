package app.mappers.dto;

import app.domain.model.Dosage;
import app.domain.model.SnsUser;
import app.domain.model.VaccinationCenter;
import app.domain.model.Vaccine;

import java.time.LocalDateTime;

public class VaccineAdministrationDto {

    private SnsUser snsUser;
    private Vaccine vaccine;
    private LocalDateTime timeAdministred;
    private Dosage dosage;
    private VaccinationCenter vaccinationCenter;

    public VaccineAdministrationDto(SnsUser snsUser, Vaccine vaccine, LocalDateTime timeAdministred, Dosage dosage, VaccinationCenter vaccinationCenter) {
        this.snsUser = snsUser;
        this.vaccine = vaccine;
        this.timeAdministred = timeAdministred;
        this.dosage = dosage;
        this.vaccinationCenter = vaccinationCenter;
    }

    public SnsUser getSnsUser() {return snsUser;}

    public Vaccine getVaccine() {return vaccine;}

    public LocalDateTime getTimeAdministred() {return timeAdministred;}

    public Dosage getDosage() {return dosage;}

    public VaccinationCenter getVaccinationCenter() {return vaccinationCenter;}
}
