package app.mappers.dto;

import app.domain.model.SnsUser;

import java.util.ArrayList;
import java.util.List;

public class SNSUserMapper {

    List<SnsUserDtoV2> snsUserDtos = new ArrayList<>();

    public List<SnsUserDtoV2> toDTO(List<SnsUser> snsUsers){
        for(SnsUser vc: snsUsers){
            snsUserDtos.add(toDto(vc));
        }
        return snsUserDtos;
    }

    private SnsUserDtoV2 toDto(SnsUser snsUser) {
        String name = snsUser.getName();
        String snsNumber = snsUser.getSnsNumber();
        return new SnsUserDtoV2(name, snsNumber);
    }
}
