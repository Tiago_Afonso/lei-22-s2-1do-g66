package app.mappers.dto;

import app.domain.model.SnsUser;

public class AdverseReactionDto {
    private final SnsUser snsUser;
    private final String description;

    public AdverseReactionDto(SnsUser snsUser,String description){
        this.snsUser = snsUser;
        this.description = description;
    }

    public SnsUser getSnsUser() {
        return snsUser;
    }

    public String getDescription() {
        return description;
    }
}
