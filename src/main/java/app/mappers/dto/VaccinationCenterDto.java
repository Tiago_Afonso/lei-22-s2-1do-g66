package app.mappers.dto;

import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

public class VaccinationCenterDto extends Object {
    private final String name;
    private final String adress;
    private final String email;
    private final String website;
    private final LocalTime openingHour;
    private final LocalTime closingHour;
    private final int phoneNumber;
    private final int faxNumber;
    private final int slotDuration;
    private final int vaccinePerSlot;

    public VaccinationCenterDto(String name, String adress, String email, String website, LocalTime openingHour, LocalTime closingHour, int phoneNumber, int faxNumber, int slotDuration, int vaccinePerSlot) {
        this.name = name;
        this.adress = adress;
        this.email = email;
        this.website = website;
        this.openingHour = openingHour;
        this.closingHour = closingHour;
        this.phoneNumber = phoneNumber;
        this.faxNumber = faxNumber;
        this.slotDuration = slotDuration;
        this.vaccinePerSlot = vaccinePerSlot;
    }

    public String getName() { return name; }

    public String getAdress() {
        return adress;
    }

    public String getEmail() {
        return email;
    }

    public String getWebsite() { return website; }

    public LocalTime getOpeningHour() { return openingHour; }

    public LocalTime getClosingHour() { return closingHour; }

    public int getPhoneNumber() {
        return phoneNumber;
    }

    public int getFaxNumber() {
        return faxNumber;
    }

    public int getSlotDuration() { return slotDuration; }

    public int getVaccinePerSlot() { return vaccinePerSlot; }

    public List<LocalDateTime> getNextAvailableDates(){
        LocalDateTime today = LocalDateTime.now();
        /*
        Method is meant to calculate a number of LocalDateTime intances that increase in 1 hour starting in the current time where
        the VaccinationCenter will be open to vaccination
        This List<LocalDateTime> is meant to represent the next number of working times of the vaccination center

         */
        return new ArrayList<LocalDateTime>();
    }
}