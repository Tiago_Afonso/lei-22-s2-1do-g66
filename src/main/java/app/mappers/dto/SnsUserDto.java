package app.mappers.dto;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Date;

public class SnsUserDto {
    private final String name;
    private final String address;
    private final String sex;
    private final String phoneNumber;
    private final String email;
    private final String password;
    private final LocalDate birthDate;
    private final String snsNumber;
    private final String ccNumber;

    public SnsUserDto(String name, String address, String sex, String phoneNumber, String email, String password, LocalDate birthDate, String snsNumber, String ccNumber) {
        this.name = name;
        this.address = address;
        this.sex = sex;
        this.phoneNumber = phoneNumber;
        this.email = email;
        this.password = password;
        this.birthDate = birthDate;
        this.snsNumber = snsNumber;
        this.ccNumber = ccNumber;
    }

    public String getName() {
        return name;
    }

    public String getAddress() {
        return address;
    }

    public String getSex() {
        return sex;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }

    public LocalDate getBirthDate() {
        return birthDate;
    }

    public String getSnsNumber() {
        return snsNumber;
    }

    public String getCcNumber() {
        return ccNumber;
    }
}
