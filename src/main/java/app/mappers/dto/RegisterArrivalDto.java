package app.mappers.dto;

import app.domain.model.SnsUser;
import app.domain.model.VaccinationCenter;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

public class RegisterArrivalDto {

    private final List<Integer> timeArrived;
    private final String SNSNumber;
    private final VaccinationCenter vaccinationCenter;
    private final SnsUser snsUser;
    private final Date date;

    public RegisterArrivalDto(List<Integer> timeArrived, String SNSNumber, VaccinationCenter vaccinationCenter, SnsUser snsUser, Date date) {
        this.timeArrived = timeArrived;
        this.SNSNumber = SNSNumber;
        this.vaccinationCenter = vaccinationCenter;
        this.snsUser = snsUser;
        this.date = date;
    }

    public List<Integer> getTimeArrived() {return timeArrived;}

    public String getSNSNumber() {return SNSNumber;}

    public VaccinationCenter getVaccinationCenter() {return vaccinationCenter;}

    public SnsUser getSnsUser() {return snsUser;}

    public Date getDate() {return date;}
}
