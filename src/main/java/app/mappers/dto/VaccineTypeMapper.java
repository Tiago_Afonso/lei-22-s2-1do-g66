package app.mappers.dto;
import app.controller.App;
import app.domain.model.Company;
import app.domain.model.VaccineType;
import app.domain.store.VaccineTypeStore;

import java.util.ArrayList;
import java.util.List;

public class VaccineTypeMapper {
    private Company company;
    private int code;
    private String designation;

    private VaccineType vt;
    private ArrayList<VaccineType> vtList = new ArrayList<>();
    private boolean b;
    private List<VaccineTypeDTO> lVaccineTypeDTO = new ArrayList<>();

    public VaccineTypeMapper(){ App.getInstance().getCompany();
        }
    private VaccineTypeStore vtStore = company.getVaccineTypeStore();

    public void toDTO(List<VaccineType> lVaccineType){
        for(VaccineType vt:lVaccineType){
            lVaccineTypeDTO.add(this.toDTO(vt));
        }
    }

    public VaccineTypeDTO toDTO(VaccineType vt){
        int code = vt.getCode();
        String designation = vt.getDesignation();
        return new VaccineTypeDTO(code,designation);
    }

    public VaccineType getVaccineTypeWithDesignation(String designation){

        for(VaccineType vt : vtList){
            if(vt.getDesignation() == designation){
                b=true;
            }
            else{
                b=false;
            }
        }
        if ( b ){
            return vt;
        }
        if (!b){
            return null;
        }
        return vt;
    }

    public List<VaccineType> bootstrapVaccineTypes(){
        VaccineType vt1 = vtStore.createVaccineType(00001, "Tetano");
        vtList.add(vt1);
        VaccineType vt2 = vtStore.createVaccineType(00002, "Gripe Sasonal");
        vtList.add(vt2);
        VaccineType vt3 = vtStore.createVaccineType(00003, "Gripe A");
        vtList.add(vt3);
        VaccineType vt4 = vtStore.createVaccineType(00004, "Covid-19");
        vtList.add(vt4);
        return vtList;

    }
}
