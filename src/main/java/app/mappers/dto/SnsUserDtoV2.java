package app.mappers.dto;


public class SnsUserDtoV2 {
    private final String name;
    private final String snsNumber;

    public SnsUserDtoV2(String name, String snsNumber) {
        this.name = name;
        this.snsNumber = snsNumber;
    }

    public String getName() {
        return name;
    }

    public String getSnsNumber() {
        return snsNumber;
    }
}
