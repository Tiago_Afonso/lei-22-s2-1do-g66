package app.mappers.dto;

import app.domain.model.SnsUser;
import app.domain.model.VaccinationCenter;
import app.domain.model.VaccineType;

import java.time.LocalDateTime;

public class VaccineScheduleDTO {

    private final VaccinationCenter vaccinationCenter;
    private final LocalDateTime date;
    private final VaccineType vaccineType;
    private final SnsUser snsUser;

    public VaccineScheduleDTO(SnsUser snsUser,VaccineType vaccineType, VaccinationCenter vaccinationCenter, LocalDateTime date ) {
        this.vaccinationCenter = vaccinationCenter;
        this.date = date;
        this.vaccineType = vaccineType;
        this.snsUser = snsUser;
    }


    public LocalDateTime getDate() {
        return this.date;
    }

    public VaccineType getVaccineType() {
        return this.vaccineType;
    }

    public SnsUser getSNSUser() {
        return this.snsUser;
    }

    public VaccinationCenter getVaccinationCenter() {
        return this.vaccinationCenter;
    }
}