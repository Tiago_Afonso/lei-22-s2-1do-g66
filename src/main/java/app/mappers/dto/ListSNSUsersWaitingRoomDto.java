package app.mappers.dto;

public class ListSNSUsersWaitingRoomDto {
    private final String name;
    private final String adress;
    private final String email;
    private final String website;
    private final int phoneNumber;
    private final int faxNumber;

    public ListSNSUsersWaitingRoomDto(String name, String adress, String email, String website, int phoneNumber, int faxNumber) {
        this.name = name;
        this.adress = adress;
        this.email = email;
        this.website = website;
        this.phoneNumber = phoneNumber;
        this.faxNumber = faxNumber;
    }

    public String getName() {
        return name;
    }

    public String getAdress() {
        return adress;
    }

    public String getEmail() {
        return email;
    }

    public String getWebsite() { return website; }

    public int getPhoneNumber() {
        return phoneNumber;
    }

    public int getFaxNumber() {
        return faxNumber;
    }
}
