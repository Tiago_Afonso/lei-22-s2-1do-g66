package app.mappers.dto;

import java.util.ArrayList;
import java.util.List;

import app.controller.App;
import app.domain.model.VaccineType;
import app.domain.store.VaccineTypeStore;

public class VaccineTypeDTO {
    private int code;
    private String designation;

    public VaccineTypeDTO(int code,String designation){
        this.code=code;
        this.designation=designation;
    }
    public String getDesignation(){
        return designation;
    }

    public int getCode(){return code;}


}