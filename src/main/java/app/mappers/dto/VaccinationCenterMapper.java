package app.mappers.dto;

import app.domain.model.VaccinationCenter;

import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

public class VaccinationCenterMapper {

    List<VaccinationCenterDto> lVaccinationCenterDTO = new ArrayList<>();

    public void toDTO(List<VaccinationCenter> lVaccinationCenter){
      for(VaccinationCenter vc:lVaccinationCenter){
          lVaccinationCenterDTO.add(this.toDTO(vc));
      }
    }

    private VaccinationCenterDto toDTO(VaccinationCenter vaccinationCenter) {
          String name = vaccinationCenter.getName();
          String adress = vaccinationCenter.getAdress();
          String email = vaccinationCenter.getEmail();
          String website = vaccinationCenter.getWebsite();
          LocalTime openingHour = vaccinationCenter.getOpeningHour();
          LocalTime closingHour = vaccinationCenter.getClosingHour();
          int phoneNumber = vaccinationCenter.getPhoneNumber();
          int faxNumber = vaccinationCenter.getFaxNumber();
          int slotDuration = vaccinationCenter.getSlotDuration();
          int vaccinePerSlot = vaccinationCenter.getVaccinePerSlot();
          return new VaccinationCenterDto(name,adress,email,website,openingHour,closingHour,phoneNumber,faxNumber,slotDuration,vaccinePerSlot);
    }
}
