package app.domain.store;

import app.domain.model.RegisterArrival;
import app.domain.model.SnsUser;
import app.domain.model.VaccineAdministration;
import app.mappers.dto.VaccineAdministrationDto;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class VaccineAdministrationStore {
    List<VaccineAdministration> vaccineAdministrationList = new ArrayList<>();


    public VaccineAdministration createVaccineAdministration(VaccineAdministrationDto vaccineAdministrationDto){
        return new VaccineAdministration(vaccineAdministrationDto);
    }

    public List<VaccineAdministration> getVaccineAdministrationList(){return vaccineAdministrationList;}

    public boolean saveVaccineAdministration(VaccineAdministration vaccineAdministration,boolean ignoreSerialize){
        if (!validateAdministration(vaccineAdministration)){
            throw  new IllegalArgumentException("Validation Unsuccessful");
        }
        if (vaccineAdministrationList.contains(vaccineAdministration)){
            throw new IllegalArgumentException("User already had a vaccine administred");
        }else{
            if (ignoreSerialize){
                return vaccineAdministrationList.add(vaccineAdministration);
            }else{
                serialize(vaccineAdministration);
                return vaccineAdministrationList.add(vaccineAdministration);
            }

        }
    }

    public boolean validateAdministration(VaccineAdministration vaccineAdministration){
        if(vaccineAdministration == null) {
            return false;
        }
        for (VaccineAdministration administration : vaccineAdministrationList){
            //if (administration.getSnsUser().getSnsNumber().equals(vaccineAdministration.getSnsUser().getSnsNumber())){
                //throw new IllegalArgumentException("User administration already registered.");
            //}
        }
        return true;
    }

    public void serialize(VaccineAdministration vaccineAdministration) {
        try {
            FileOutputStream fileOutputStream = new FileOutputStream("src\\main\\java\\app\\domain\\store\\files\\VaccineAdministrations\\VaccineAdministration" + vaccineAdministration.getSnsUser().getSnsNumber() + ".txt");
            ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);
            objectOutputStream.writeObject(vaccineAdministration);
            objectOutputStream.close();
            fileOutputStream.close();
        } catch (IOException i) {
            i.printStackTrace();
        }
    }

    public void deSerialize(){
        try {

            File directory = new File("src\\main\\java\\app\\domain\\store\\files\\VaccineAdministrations");
            File[] listOfFiles = directory.listFiles();
            System.out.println(listOfFiles.length);

            for (int i = 0; i < listOfFiles.length; i++){
                FileInputStream fileInputStream = new FileInputStream(listOfFiles[i]);
                ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
                VaccineAdministration vaccineAdministration = (VaccineAdministration) objectInputStream.readObject();
                objectInputStream.close();
                fileInputStream.close();
                validateAdministration(vaccineAdministration);
                vaccineAdministrationList.add(vaccineAdministration);
            }

            System.out.println(vaccineAdministrationList);


        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }


    }

}

