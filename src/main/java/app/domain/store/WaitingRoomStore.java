package app.domain.store;

import app.domain.model.RegisterArrival;
import app.domain.model.SnsUser;
import app.domain.model.VaccineAdministration;
import app.domain.model.WaitingRoom;
import app.mappers.dto.WaitingRoomDto;

import java.util.ArrayList;
import java.util.List;

public class WaitingRoomStore {
    private final List<WaitingRoom> waitingRoomEntryList = new ArrayList<>();


    public WaitingRoom createEntry (WaitingRoomDto waitingRoomEntryDto){
        return new WaitingRoom(waitingRoomEntryDto);
    }

    public boolean saveEntry(WaitingRoom waitingRoom){
        if(waitingRoomEntryList.contains(waitingRoom)) {
            throw new IllegalArgumentException("User already registered in the waiting room");
        } else {
            return  waitingRoomEntryList.add(waitingRoom);
        }
    }

    public boolean removeEntry(SnsUser snsUser){
        for (int i = 0; i < waitingRoomEntryList.size(); i++){
            if (snsUser.equals(waitingRoomEntryList.get(i).getUser())){
                waitingRoomEntryList.remove(i);
                return true;
            }
        }
        return false;

    }

    public List<WaitingRoom> getWaitingRoomEntryList() {return waitingRoomEntryList;}
}
