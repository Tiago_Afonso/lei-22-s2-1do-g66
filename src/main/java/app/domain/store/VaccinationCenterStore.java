package app.domain.store;

import app.domain.model.VaccinationCenter;
import app.mappers.dto.VaccinationCenterDto;

import java.util.ArrayList;
import java.util.List;

public class VaccinationCenterStore extends Object {
    private List<VaccinationCenter> vaccinationCenterList = new ArrayList<>();
    private VaccinationCenter vaccinationCenter;

    public VaccinationCenter createVaccinationCenter(VaccinationCenterDto vaccinationCenterDto) {
        VaccinationCenter.validateVaccinationCenter(vaccinationCenterDto);
        vaccinationCenter = new VaccinationCenter(vaccinationCenterDto);
        return vaccinationCenter;
    }

    public boolean saveVaccinationCenter(VaccinationCenter vaccinationCenter) {
            vaccinationCenterList.add(vaccinationCenter);
            return true;
    }

    public boolean saveVaccinationCenter() {
        vaccinationCenterList.add(vaccinationCenter);
        return true;
    }

    public List<VaccinationCenter> getVaccinationCenterList() {
        return vaccinationCenterList;
    }

    @Override
    public String toString() {
        final StringBuilder stringBuilder = new StringBuilder();
        for(VaccinationCenter vaccinationCenter : vaccinationCenterList) {
            stringBuilder.append(vaccinationCenter).append("\n\n");
        }
        return stringBuilder.toString();
    }

}