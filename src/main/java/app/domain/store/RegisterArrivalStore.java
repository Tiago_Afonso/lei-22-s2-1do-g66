package app.domain.store;

import app.domain.model.RegisterArrival;
import app.mappers.dto.RegisterArrivalDto;

import java.util.ArrayList;
import java.util.List;

public class RegisterArrivalStore {
    private final List<RegisterArrival> userArrivalList = new ArrayList<>();

    public RegisterArrival createArrivalTime(RegisterArrivalDto registerArrivalDto){
        return new RegisterArrival(registerArrivalDto);
    }

    public List<RegisterArrival> getListUserArrivalTimes(){return userArrivalList;}

    public boolean saveArrival(RegisterArrival registerArrival){
        if(!validateArrival(registerArrival)) {
            return false;
        }
        if(userArrivalList.contains(registerArrival)) {
            throw new IllegalArgumentException("User arrival already registered");
        } else {
            return  userArrivalList.add(registerArrival);
        }
    }

    public boolean validateArrival(RegisterArrival registerArrival){
        if(registerArrival == null) {
            return false;
        }
        for (RegisterArrival arrivals : userArrivalList){
            if (arrivals.getSNSNumber().equals(registerArrival.getSNSNumber())){
                throw new IllegalArgumentException("User arrival already registered.");
            }
        }
        return true;
    }

    @Override
    public String toString() {
        final StringBuilder stringBuilder = new StringBuilder();
        for(RegisterArrival arrivals : userArrivalList) {
            stringBuilder.append(arrivals);
        }
        return stringBuilder.toString();
    }


}
