package app.domain.store;

import app.domain.model.SNSUsersWaitingRoom;
import app.domain.model.VaccinationCenter;
import app.mappers.dto.ListSNSUsersWaitingRoomDto;

import java.util.ArrayList;
import java.util.List;

public class ListSNSUsersWaitingRoomStore extends Object {
    private final List<SNSUsersWaitingRoom> SNSUsersWaitingRoom = new ArrayList<>();
    private SNSUsersWaitingRoom listsnsUsersWaitingRoom;

    public ListSNSUsersWaitingRoomStore(SNSUsersWaitingRoom listsnsUsersWaitingRoom) {
        SNSUsersWaitingRoom.add(listsnsUsersWaitingRoom);
    }

    public List<SNSUsersWaitingRoom> getListSNSUsersWaitingRoom() {
        return SNSUsersWaitingRoom;
    }

    @Override
    public String toString() {
        final StringBuilder stringBuilder = new StringBuilder();
        for(SNSUsersWaitingRoom snsUsersWaitingRoom : SNSUsersWaitingRoom) {
            stringBuilder.append(SNSUsersWaitingRoom).append("\n\n");
        }
        return stringBuilder.toString();
    }
}
