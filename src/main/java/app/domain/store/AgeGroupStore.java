package app.domain.store;



import app.domain.model.AgeGroup;
import app.domain.model.Dosage;
import app.domain.model.Vaccine;

import java.util.ArrayList;
import java.util.List;

public class AgeGroupStore {
    private final List<AgeGroup> agList = new ArrayList<>();
    private boolean a;
    public AgeGroup createAgeGroup(int upperAge, int lowerAge, Dosage dosage){
        AgeGroup ageGroup = new AgeGroup(upperAge, lowerAge, dosage);
        return ageGroup;
    }
    public boolean validateAgeGroup(AgeGroup ageGroup){
        if (ageGroup == null){
            return false;
        }

        return !this.agList.contains(ageGroup);
    }
    public boolean saveAgeGroup(AgeGroup ageGroup){
        agList.add(ageGroup);
        if (agList.contains(ageGroup)){
            boolean a = true;
        }
        else{
            boolean a = false;
        }
        return a;
    }
}
