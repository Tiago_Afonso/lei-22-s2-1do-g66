package app.domain.store;

import app.domain.model.InformationLegacySystem;
import app.domain.model.SelectionSortAlgorithm;
import app.domain.model.Vaccine;
import app.domain.model.VaccineType;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class InformationLegacySystemStore {

    private final List<InformationLegacySystem> list = new ArrayList<>();

    public InformationLegacySystemStore(){

    }

    public InformationLegacySystem create(String snsUserNumber, String vaccineName, String dose, String lotNumber, Date scheduleDateTime, Date  arrivalDateTime, Date nurseAdminDateTime, Date leavingDateTime){
        InformationLegacySystem inf = new InformationLegacySystem(snsUserNumber, vaccineName, dose, lotNumber, scheduleDateTime, arrivalDateTime, nurseAdminDateTime, leavingDateTime);
        return inf;
    }

    public boolean validateInformation(InformationLegacySystem i){
        return i != null;
    }

    public boolean saveInformation(InformationLegacySystem inf ){
        if (!validateInformation(inf)) {
            return false;
        }
        return list.add(inf);
    }

    public List<InformationLegacySystem> allInformation(){
        return list;
    }
}
