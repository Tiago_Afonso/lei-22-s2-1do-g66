package app.domain.store;

import app.domain.model.SnsUser;
import app.domain.model.Vaccine;
import app.domain.model.VaccineType;
import app.mappers.dto.VaccineTypeDTO;

import java.util.ArrayList;
import java.util.List;

public class VaccineTypeStore {
    private final List<VaccineType> vtList = new ArrayList<>();
  //  private boolean a = false;

    public VaccineTypeStore() {
    }

    public VaccineType createVaccineType(int code, String designation){
        VaccineType vt = new VaccineType(code, designation);

            validateVaccineType(vt);
        vtList.add(vt);
        return vt;
    }
    public boolean validateVaccineType(VaccineType vt){
        if (vt.getCode() == 0 || vt.getDesignation().equals(null)){
            return false;
        }
        if(vt.getCode() > 99999){
            throw new IllegalArgumentException("Code must be of 5 digits");
        }
        for (VaccineType vts : vtList){
            if(vts.getCode() == vt.getCode()){
                throw new IllegalArgumentException("Vaccine type already exists");
            }
        }
        return !this.vtList.contains(vt);
    }

    public boolean validateVaccineType(VaccineTypeDTO vt){
        if (vt.getCode() == 0 || vt.getDesignation().equals(null)){
            return false;
        }
        if(vt.getCode() > 99999){
            throw new IllegalArgumentException("Code must be of 5 digits");
        }
        for (VaccineType vts : vtList){
            if(vts.getCode() == vt.getCode()){
                throw new IllegalArgumentException("Vaccine type already exists");
            }
        }
        return !this.vtList.contains(vt);
    }

 //   public boolean saveVaccineType(VaccineType vt){
  //      vtList.add(vt);
  //      if (vtList.contains(vt)){
   //         boolean a = true;
   //     }
  //      else{
  //          boolean a = false;
   //     }
  //      return a;
  //  }

    public List<VaccineType> getVaccineTypeList(){

        return vtList;
    }


}
