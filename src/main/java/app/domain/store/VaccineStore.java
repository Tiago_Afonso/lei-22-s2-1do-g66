package app.domain.store;

import app.domain.model.Vaccine;
import app.domain.model.VaccineType;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class VaccineStore {
    private final List<Vaccine> vList = new ArrayList<>();
    private boolean a;
    public Vaccine createVaccine(String name, String iD, String brand, VaccineType vType, List ageGroups){
        Vaccine v = new Vaccine(name, iD, brand, vType, ageGroups);
        return v;
    }
    public boolean validateVaccine(Vaccine v){
        if (v == null){
            return false;
        }
        for (Vaccine vs : vList){
            if(vs.getName() == v.getName()){
                throw new IllegalArgumentException("Vaccine already exists");
            }
        }
        return !this.vList.contains(v);
    }

    public boolean saveVaccine(Vaccine v){
        if(validateVaccine(v)){
            vList.add(v);
            return true;
        }
        return false;
    }

    public boolean checkVaccineExistsForAgeGroup(VaccineType vaccineType, LocalDate bday) {
        /*
        Method checks if there are vaccines of the given type available to the user's age group; returns true if there is at least one, false otherwise
        To be Implemented
         */
        return true;
    }

    public List<Vaccine> getVaccineTypeList(){
        return this.vList;
    }

    public String findVaccineType(String name) {
        for (Vaccine v : vList) {
            if (v.getName().equals(name)) {
                return v.getVaccineTypeDesc();
            }
        }
        return null;
    }


  /*  public boolean saveVaccine(Vaccine v){
        vList.add(v);
        if (vList.contains(v)){
            boolean a = true;
        }
        else{
            boolean a = false;
        }
        return a;
    } */
}
