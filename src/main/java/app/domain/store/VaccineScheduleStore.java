package app.domain.store;

import app.domain.model.*;
import app.mappers.dto.VaccineScheduleDTO;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

public class VaccineScheduleStore {
    ArrayList<VaccineSchedule> vsList = new ArrayList<>();

    private final ArrayList<VaccineSchedule> vaccineScheduleList = new ArrayList<>();

    public VaccineSchedule createVaccineSchedule(VaccineScheduleDTO vaccineScheduleDTO) {
        return new VaccineSchedule(vaccineScheduleDTO);
    }

    public boolean checkUserSchedule(SnsUser snsUser) {
        /*
        Method checks if user's last scheduled vaccine was already taken
        In case it was taken before today's date it returns true, else if the
        scheduled vaccination wasn't taken yet it returns false
        To be Implemented
         */
        for(VaccineSchedule vs: this.vsList){
            if(vs.getSNSUser().equals(snsUser) && vs.getDate().isAfter(LocalDateTime.now()) ){
                return false;
            }
        }
        return true;
    }

    public boolean validateVaccineSchedule(VaccineScheduleDTO vsDTO) {
        /*
        Method checks if all vaccine schedule attributes in DTO are not null/valid, within parameters and returns true if they are valid.
        To be Implemented
         */
        return true;
    }

    public boolean addVaccineSchedule(VaccineSchedule vaccineSchedule) {
        return this.vaccineScheduleList.add(vaccineSchedule);
    }

    public ArrayList<VaccineSchedule> getVaccineScheduleList() {
        return vaccineScheduleList;
    }

    public List<LocalDateTime> getUserNextAvailableDates(SnsUser snsUser) {
        LocalDateTime oldDate = this.findUserLastSchedule(snsUser).getDate();

        List<LocalDateTime> lDate = new ArrayList<>();
        return lDate;
    }

    private VaccineSchedule findUserLastSchedule(SnsUser snsUser) {
        /*
        Method returns last known instance of the user's vaccine
        To be Implemented
         */
        return null;
    }

    public ArrayList<LocalDateTime> calculateNextDates(SnsUser snsUser, VaccinationCenter vc, VaccineType vt) {
        /*
        Method returns next available dates of vaccination schedule by taking into consideration the wait interval of the last dosage and the current day.
        Results should give a margin of at least 1 month of the next dates after today's date
        To be Implemented
         */
        ArrayList<LocalDateTime> dtList = new ArrayList<>();
        LocalTime oh = vc.getOpeningHour();
        LocalTime ch = vc.getClosingHour();
        String vtd = vt.getDesignation();
        String snsUserNumber = snsUser.getSnsNumber();
        ArrayList<LocalDateTime> temp = new ArrayList<>();
        long count;
        LocalDateTime dt;

        for (VaccineSchedule vs : vsList) {
            if (vs.getVaccinationCenter() == vc && vs.getVaccineType() == vt) {
                temp.add(vs.getDate());
            }
        }

        dt = LocalDateTime.now();

        for (count = 0; count < 50; count++) {

            LocalDate day = dt.toLocalDate();
            LocalTime min = dt.toLocalTime();
            if (dt.getMinute() % 5 != 0) {
                long a = dt.getMinute() % 5;
                dt = LocalDateTime.of(day, min.plusMinutes(5 - a));
            }
            dt = LocalDateTime.of(day, min.plusMinutes(5));

            if (oh.isBefore(dt.toLocalTime()) && ch.isAfter(dt.toLocalTime())) {
                if (!temp.contains(dt)) {
                    dtList.add(dt);
                }
            } else {
                dt = LocalDateTime.of(day.plusDays(1), oh);
                dtList.add(dt);
            }
        }
        return dtList;
    }
}
