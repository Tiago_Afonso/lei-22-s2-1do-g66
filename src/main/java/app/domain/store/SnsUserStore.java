package app.domain.store;

import app.domain.model.SnsUser;
import app.mappers.dto.SnsUserDto;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class SnsUserStore implements Serializable{
    private final List<SnsUser> snsUserList = new ArrayList<>();

    /**
     * creates instance of SnsUser using Dto
     *
     * @param snsUserDto DTO with SNS User data
     * @return SnsUser object
     */
    public SnsUser createSnsUser(SnsUserDto snsUserDto) {
        return new SnsUser(snsUserDto);
    }

    /**
     * validates if SNS User unique fields are unique, then adds SNS User to list
     *
     * @param snsUser SNS User object instance
     * @return true if SNS User was successfully added to list
     */
    public boolean saveSnsUser(SnsUser snsUser) {
        if (!validateSnsUser(snsUser)) {
            return false;
        }
        if (snsUser.saveSnsUser()) {
            return snsUserList.add(snsUser);
        } else {
            throw new IllegalArgumentException("Email already exists");
        }
    }

    /**
     * validates if SNS User unique fields are unique
     *
     * @param snsUser SNS User object instance
     * @return false if NULL, contains SNS User, or unique fields already exist, else true
     */
    public boolean validateSnsUser(SnsUser snsUser) {
        if (snsUser == null) {
            return false;
        }
        for (SnsUser snsUsers : snsUserList) {
            if (snsUsers.getPhoneNumber().equals(snsUser.getPhoneNumber())) {
                throw new IllegalArgumentException("Phone number already exists");
            }
            if (snsUsers.getSnsNumber().equals(snsUser.getSnsNumber())) {
                throw new IllegalArgumentException("SNS number already exists");
            }
            if (snsUsers.getCcNumber().equals(snsUser.getCcNumber())) {
                throw new IllegalArgumentException("CC number already exists");
            }
        }
        return !this.snsUserList.contains(snsUser);
    }

    /**
     * returns the list of registered sns users in the system
     *
     * @return snsUserList
     */
    public List<SnsUser> getSnsUserList() {
        return snsUserList;
    }

    @Override
    public String toString() {
        final StringBuilder stringBuilder = new StringBuilder();
        for (SnsUser snsUsers : snsUserList) {
            stringBuilder.append(snsUsers);
        }
        return stringBuilder.toString();
    }

    public SnsUser findUserByNumber(String snsUserNumber, ArrayList<SnsUser> snsUserList) {
        for (SnsUser snsUser : snsUserList) {
            if (snsUser.getSnsNumber().equals(snsUserNumber)) {
                return snsUser;
            }
            else{
                return null;
            }
        }
        return null;
    }

    public SnsUser findUserBySNSNumber(String snsUserNumber) {
        for (SnsUser snsUser : snsUserList) {
            if (snsUser.getSnsNumber().equals(snsUserNumber)) {
                return snsUser;
            }
        }
        return null;
    }

}
