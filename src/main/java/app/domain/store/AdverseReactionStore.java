package app.domain.store;

import app.domain.model.AdverseReaction;
import app.mappers.dto.AdverseReactionDto;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class AdverseReactionStore {

    private List<AdverseReaction> adverseReactionList = new ArrayList<>();

    public AdverseReaction createAdverseReaction(AdverseReactionDto adverseReactionDto) throws Exception {
            return new AdverseReaction(adverseReactionDto);
        }

    public void saveAdverseReaction(AdverseReaction adverseReaction) throws Exception {
        validateAdverseReaction(adverseReaction);
        adverseReactionList.add(adverseReaction);
        System.out.println(adverseReactionList.size());
        serialize();
    }

    public List<AdverseReaction> getAdverseReactionList(){
        return adverseReactionList;
    }

    public void validateAdverseReaction(AdverseReaction adverseReaction) throws Exception {
        if (adverseReaction == null) {
            throw new Exception("Could register adverse reaction");
        }
    }

    public void serialize() {
        try {
            FileOutputStream fileOutputStream = new FileOutputStream("src\\main\\java\\app\\domain\\store\\files\\AdverseReactionFile.bin");
            ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);
            objectOutputStream.writeObject(adverseReactionList);
            objectOutputStream.close();
            fileOutputStream.close();
        } catch (IOException i) {
            i.printStackTrace();
        }
    }

    public void deserialize() {
        try {
            FileInputStream fileInputStream = new FileInputStream("src\\main\\java\\app\\domain\\store\\files\\AdverseReactionFile.bin");
            ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
            adverseReactionList = (List<AdverseReaction>) objectInputStream.readObject();
            objectInputStream.close();
            fileInputStream.close();
        } catch (IOException i) {
            i.printStackTrace();
        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        }
    }

}

