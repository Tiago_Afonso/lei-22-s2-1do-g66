package app.domain.model;


import app.mappers.dto.VaccineScheduleDTO;

import java.time.LocalDateTime;
import java.util.Date;


public class VaccineSchedule {
    private SnsUser SNSUser;
    private VaccineType vaccineType;
    private VaccinationCenter vaccinationCenter;
    private LocalDateTime date;

    @Override
    public String toString() {
        return "VaccineSchedule{" +
                "SNSUser=" + SNSUser +
                "vaccineType=" + vaccineType.getDesignation() + "\n" +
                "vaccinationCenter=" + vaccinationCenter.getName() + "\n" +
                "date=" + date +
                '}';
    }

    public VaccineSchedule(){

    }


    public VaccineSchedule(SnsUser SNSUser, VaccineType vaccineType, VaccinationCenter vaccinationCenter, LocalDateTime date){
        this.SNSUser = SNSUser;
        this.vaccineType = vaccineType;
        this.vaccinationCenter = vaccinationCenter;
        this.date = date;
    }


    public VaccineSchedule(VaccineScheduleDTO vaccineScheduleDTO) {
        this.SNSUser = vaccineScheduleDTO.getSNSUser();
        this.vaccineType = vaccineScheduleDTO.getVaccineType();
        this.vaccinationCenter = vaccineScheduleDTO.getVaccinationCenter();
        this.date = vaccineScheduleDTO.getDate();
    }

    public LocalDateTime getDate(){
        return date;
    }

    public SnsUser getSNSUser() {return SNSUser;}
    public VaccinationCenter getVaccinationCenter(){return vaccinationCenter;}
    public VaccineType getVaccineType(){return vaccineType;}
}
