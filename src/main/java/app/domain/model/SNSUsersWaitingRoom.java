package app.domain.model;

import app.domain.store.VaccinationCenterStore;
import app.mappers.dto.ListSNSUsersWaitingRoomDto;

import java.util.ArrayList;
import java.util.List;

public class SNSUsersWaitingRoom {
    private final String name;
    private final String email;
    private final String adress;
    private final String website;
    private final int phoneNumber;
    private final int faxNumber;

    public static void validateVaccinationCenter (ListSNSUsersWaitingRoomDto listSNSUsersWaitingRoomDto) {
        if (listSNSUsersWaitingRoomDto.getEmail().split("@").length != 2 && listSNSUsersWaitingRoomDto.getEmail().split("\\.").length != 2) {
            throw new IllegalArgumentException("Email is in the wrong format! \n Correct format -> xxxxxxx@xxxx.xx");
        }
        else if (listSNSUsersWaitingRoomDto.getWebsite().split("\\.").length != 2) {
            throw new IllegalArgumentException("Website is in the wrong format! \n Correct format -> xxxxxxx.xx");
        }
        else if (listSNSUsersWaitingRoomDto.getPhoneNumber() < 99999999 | listSNSUsersWaitingRoomDto.getPhoneNumber() > 1000000000) {
            throw new IllegalArgumentException("PhoneNumber is in the wrong format! \n Correct format -> xxx xxx xxx (9 digits)");
        }
    }

    public SNSUsersWaitingRoom(ListSNSUsersWaitingRoomDto listSNSUsersWaitingRoomDto) {
        this.name = listSNSUsersWaitingRoomDto.getName();
        this.adress = listSNSUsersWaitingRoomDto.getAdress();
        this.email = listSNSUsersWaitingRoomDto.getEmail();
        this.website = listSNSUsersWaitingRoomDto.getWebsite();
        this.phoneNumber = listSNSUsersWaitingRoomDto.getPhoneNumber();
        this.faxNumber = listSNSUsersWaitingRoomDto.getFaxNumber();
    }

    @Override
    public String toString() {
        return "VaccinationCenter{" +
                "name='" + name + '\'' +
                ", email='" + email + '\'' +
                ", adress='" + adress + '\'' +
                ", website='" + website + '\'' +
                ", phoneNumber=" + phoneNumber +
                ", faxNumber=" + faxNumber +
                '}';
    }
}