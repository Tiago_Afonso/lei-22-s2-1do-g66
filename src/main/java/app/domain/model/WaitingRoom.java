package app.domain.model;

import app.mappers.dto.WaitingRoomDto;

import java.time.LocalDateTime;

public class WaitingRoom {
    private final SnsUser user;
    private final LocalDateTime dateTimeArrived;
    private final VaccinationCenter vaccinationCenter;

    public WaitingRoom(WaitingRoomDto waitingRoomDto) {
        this.user = waitingRoomDto.getUser();
        this.dateTimeArrived = waitingRoomDto.getDateTimeArrived();
        this.vaccinationCenter = waitingRoomDto.getVaccinationCenter();
    }

    public SnsUser getUser() {return user;}

    public LocalDateTime getDateTimeArrived() {return dateTimeArrived;}

    public VaccinationCenter getVaccinationCenter() {return vaccinationCenter;}

    @Override
    public String toString() {
        return "-------------\n" + user + "Arrived at: " + dateTimeArrived.getYear() + "-" + dateTimeArrived.getMonthValue()
        + "-" + dateTimeArrived.getDayOfMonth() + " " + dateTimeArrived.getHour() + ":" + dateTimeArrived.getMinute() +
        ":" + dateTimeArrived.getSecond() + "\n-------------";
    }
}
