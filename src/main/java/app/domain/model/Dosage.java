package app.domain.model;


import java.io.Serializable;
import java.util.List;

public class Dosage implements Serializable {
    private int nrOfDoses;
    private List<Integer> dosageq;
    private List<Integer> timeInterval;
    public Dosage(int nrOfDoses, List<Integer> dosageq, List<Integer> timeInterval ){
        this.nrOfDoses=nrOfDoses;
        this.dosageq=dosageq;
        this.timeInterval=timeInterval;

    }

    @Override
    public String toString() {
        return "Dosage{" +
                "nrOfDoses=" + nrOfDoses +
                ", dosageq=" + dosageq +
                ", timeInterval=" + timeInterval +
                '}';
    }
}
