package app.domain.model;

import app.controller.RecordDailyVaccinesController;
import app.domain.store.*;
import pt.isep.lei.esoft.auth.AuthFacade;
import org.apache.commons.lang3.StringUtils;

import java.io.File;
import java.io.FileNotFoundException;
import java.time.LocalTime;
import java.util.Scanner;

/**
 *
 * @author Paulo Maio <pam@isep.ipp.pt>
 */
public class Company {

    private final String designation;
    private final AuthFacade authFacade;
    private final SnsUserStore snsUserStore;
    private final VaccinationCenterStore vaccinationCenterStore;

    private final VaccineTypeStore vtStore;

    private final VaccineStore vStore;

    private final DosageStore dosStore;

    private final AgeGroupStore agStore;

    private final RegisterArrivalStore arrivalStore;

    private final VaccineScheduleStore vsStore;

    private final InformationLegacySystemStore lsStore;

    private final WaitingRoomStore waitingRoomStore;

    private final VaccineAdministrationStore vaccineAdministrationStore;


    private final DailyRecordVaccinesStore dvrStore;

    private final AdverseReactionStore adverseReactionStore;
    // private RecordDailyVaccinesController rdvCont;
    private static final String FILEPATH = "config.conf";


    public Company(String designation)  {
        if (StringUtils.isBlank(designation))
            throw new IllegalArgumentException("Designation cannot be blank.");

        this.designation = designation;
        this.authFacade = new AuthFacade();


        this.snsUserStore = new SnsUserStore();
        this.vaccinationCenterStore = new VaccinationCenterStore();
        this.vtStore = new VaccineTypeStore();
        this.vStore = new VaccineStore();
        this.agStore = new AgeGroupStore();
        this.dosStore = new DosageStore();
        this.arrivalStore = new RegisterArrivalStore();
        this.vsStore = new VaccineScheduleStore();
        this.lsStore = new InformationLegacySystemStore();
        this.waitingRoomStore = new WaitingRoomStore();
        this.vaccineAdministrationStore = new VaccineAdministrationStore();

        this.dvrStore = new DailyRecordVaccinesStore();
       // this.rdvCont = new RecordDailyVaccinesController();
        //LocalTime lt;
       // lt=getReportTime();
       // if(LocalTime.now() == lt){
      //      rdvCont.dailyRecording();
      //  }
        this.adverseReactionStore = new AdverseReactionStore();

    }

    public String getDesignation() {
        return designation;
    }

    public AuthFacade getAuthFacade() {
        return authFacade;
    }

    public SnsUserStore getSnsUserStore() {
        return snsUserStore;
    }

    public VaccinationCenterStore getVaccinationCenterStore() { return vaccinationCenterStore; }

    public VaccineTypeStore getVaccineTypeStore(){return vtStore;}

    public VaccineStore getVaccineStore(){return vStore;}

    public AgeGroupStore getAgeGroupStore(){return agStore;}

    public DosageStore getDosageStore(){return dosStore;}

    public RegisterArrivalStore getArrivalStore(){return arrivalStore;}

    public VaccineScheduleStore getVaccineScheduleStore() { return vsStore;
    }

    public InformationLegacySystemStore getLsStore(){
        return lsStore;
    }

    public WaitingRoomStore getWaitingRoomStore() {return waitingRoomStore;}

    public VaccineAdministrationStore getVaccineAdministrationStore() {return vaccineAdministrationStore;}

    public DailyRecordVaccinesStore getDailyRecordVaccineStore(){return dvrStore;}


    public AdverseReactionStore getAdverseReactionStore(){
        return adverseReactionStore;
    }
    public LocalTime getReportTime() {
        try{
            Scanner read = new Scanner(new File(FILEPATH));
            LocalTime config = LocalTime.parse(read.nextLine().trim());
            return config;
        }catch(FileNotFoundException e){
            return null;
        }

    }

}
