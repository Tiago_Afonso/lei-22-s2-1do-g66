package app.domain.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class BubbleSortAlgorithm{

    public List<InformationLegacySystem> bubbleSortByArrivalDate(List<InformationLegacySystem> list) {
        double start = System.nanoTime();
        int n = list.size();
        InformationLegacySystem temp;

        for (int j = 0; j < n ; j++) {
            for (int i = j + 1; i < n; i++) {
                if (list.get(j).getArrivalDateTime().compareTo(list.get(i).getArrivalDateTime()) > 0) {
                    temp = list.get(j);
                    list.set(j, list.get(i));
                    list.set(i, temp);
                }
            }
        }
        double end = System.nanoTime();
        double runtime = end - start;
        //System.out.println("The system took " + runtime + " nanoseconds to sort the clients using bubble sort");
        return list;
    }

    public List <InformationLegacySystem> bubbleSortByLeavingDate(List<InformationLegacySystem> list) {
        double start = System.nanoTime();
        int n = list.size();
        InformationLegacySystem temp;

        for (int j = 0; j < n ; j++) {
            for (int i = j + 1; i < n; i++) {
                if (list.get(j).getLeavingDateTime().compareTo(list.get(i).getLeavingDateTime()) > 0) {
                    temp = list.get(j);
                    list.set(j, list.get(i));
                    list.set(i, temp);
                }
            }
        }
        double end = System.nanoTime();
        double runtime = end - start;
        //System.out.println("The system took " + runtime + " nanoseconds to sort the clients using bubble sort");
        return list;
    }
}

