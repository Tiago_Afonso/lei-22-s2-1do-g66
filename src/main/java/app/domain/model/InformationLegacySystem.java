package app.domain.model;

import app.domain.store.SnsUserStore;

import java.util.Date;

public class InformationLegacySystem {
    private String snsUserNumber;
    private String vaccineName;
    private String dose;
    private String lotNumber;
    private Date scheduleDateTime;
    private Date arrivalDateTime;
    private Date nurseAdminDateTime;
    private Date leavingDateTime;

    public InformationLegacySystem(String snsUserNumber, String vaccineName, String dose, String lotNumber, Date scheduleDateTime, Date  arrivalDateTime, Date nurseAdminDateTime, Date leavingDateTime) {

        this.snsUserNumber = snsUserNumber;
        this.vaccineName = vaccineName;
        this.dose = dose;
        this.lotNumber = lotNumber;
        this.scheduleDateTime = scheduleDateTime;
        this.arrivalDateTime = arrivalDateTime;
        this.nurseAdminDateTime = nurseAdminDateTime;
        this.leavingDateTime = leavingDateTime;
    }

    public String getSnsUserNumber() {
        return snsUserNumber;
    }

    public void setSnsUserNumber(String snsUserNumber) {
        this.snsUserNumber = snsUserNumber;
    }

    public String getVaccineName() {
        return vaccineName;
    }

    public void setVaccineName(String vaccineName) {
        this.vaccineName = vaccineName;
    }

    public String getDose() {
        return dose;
    }

    public void setDose(String dose) {
        this.dose = dose;
    }

    public String getLotNumber() {
        return lotNumber;
    }

    public void setLotNumber(String lotNumber) {
        this.lotNumber = lotNumber;
    }

    public Date getScheduleDateTime() {
        return scheduleDateTime;
    }

    public void setScheduleDateTime(Date scheduleDateTime) {
        this.scheduleDateTime = scheduleDateTime;
    }

    public Date getArrivalDateTime() {
        return arrivalDateTime;
    }

    public void setArrivalDateTime(Date arrivalDateTime) {
        this.arrivalDateTime = arrivalDateTime;
    }

    public Date getNurseAdminDateTime() {
        return nurseAdminDateTime;
    }

    public void setNurseAdminDateTime(Date nurseAdminDateTime) {
        this.nurseAdminDateTime = nurseAdminDateTime;
    }

    public Date getLeavingDateTime() {
        return leavingDateTime;
    }

    public void setLeavingDateTime(Date leavingDateTime) {
        this.leavingDateTime = leavingDateTime;
    }

    public String toString() {
        return "\nSNS User Number: " + snsUserNumber +
                "\nVaccine Name: " + vaccineName +
                "\nDose: " + dose +
                "\nLot Number: " + lotNumber +
                "\nSchedule Date Time: " + scheduleDateTime +
                "\nArrival Date Time: " + arrivalDateTime +
                "\nNurse Administration Date Time: " + nurseAdminDateTime +
                "\nLeaving Date Time: " + leavingDateTime;
    }
}
