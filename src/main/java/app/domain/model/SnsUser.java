package app.domain.model;

import app.controller.App;
import app.mappers.dto.SnsUserDto;
import pt.isep.lei.esoft.auth.AuthFacade;
import app.domain.shared.Constants;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;

public class SnsUser implements Serializable {
    private final String name;
    private final String address;
    private final String sex;
    private final String phoneNumber;
    private final String email;
    private final String password;
    private final LocalDate birthDate;
    private final String snsNumber;
    private final String ccNumber;

    private static final String NAME_REGEX = "^([^0-9]*)$";
    private static final String SEX_REGEX = "^M|F|Masculino|Feminino$";
    private static final String PHONE_NUMBER_REGEX = "^[0-9]{9}$";
    private static final String EMAIL_REGEX = "^(.+)@(.+)$";
    private static final String PASSWORD_REGEX_LENGTH = "^.{7,}$";
    private static final String PASSWORD_REGEX_UPPERCASE = "^.*[A-Z].*[A-Z].*[A-Z].*$";
    private static final String PASSWORD_REGEX_DIGIT = "^.*[0-9].*[0-9].*$";
    private static final String SNS_NUMBER_REGEX = "^[0-9]{9}$";
    private static final String CC_NUMBER_REGEX = "^[0-9]{8}$";

    private final transient AuthFacade authFacade = App.getInstance().getCompany().getAuthFacade();

    public SnsUser(SnsUserDto snsUserDto) {
        validateName(snsUserDto);
        validateAddress(snsUserDto);
        validateSex(snsUserDto);
        validatePhoneNumber(snsUserDto);
        validateEmail(snsUserDto);
        validatePassword(snsUserDto);
        validateBirthdate(snsUserDto);
        validateSnsNumber(snsUserDto);
        validateCcNumber(snsUserDto);
        this.name = snsUserDto.getName();
        this.address = snsUserDto.getAddress();
        this.sex = snsUserDto.getSex();
        this.phoneNumber = snsUserDto.getPhoneNumber();
        this.email = snsUserDto.getEmail();
        this.password = snsUserDto.getPassword();
        this.birthDate = snsUserDto.getBirthDate();
        this.snsNumber = snsUserDto.getSnsNumber();
        this.ccNumber = snsUserDto.getCcNumber();
    }

    public SnsUser(String name, String address, String sex, String phoneNumber, String email, String password, LocalDate birthDate, String snsNumber, String ccNumber) {
        this.name = name;
        this.address = address;
        this.sex = sex;
        this.phoneNumber = phoneNumber;
        this.email = email;
        this.password = password;
        this.birthDate = birthDate;
        this.snsNumber = snsNumber;
        this.ccNumber = ccNumber;
    }

    public String getSnsNumber() {
        return snsNumber;
    }

    public String getName() {
        return name;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public String getCcNumber() {
        return ccNumber;
    }

    public LocalDate getBirthDate() {
        return this.birthDate;
    }

    public String getAddress() {
        return address;
    }

    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }

    public String getSex() {
        return sex;
    }


    /**
     * checks if name doesn't contain numbers or special characters
     *
     * @param snsUserDto snsUserDto DTO with SNS User data
     */
    private void validateName(SnsUserDto snsUserDto) {
        if (!snsUserDto.getName().matches(NAME_REGEX)) {
            throw new IllegalArgumentException("Name cannot contain numbers or special characters");
        }
    }

    /**
     * checks if sex is either M or F
     *
     * @param snsUserDto snsUserDto DTO with SNS User data
     */
    private void validateSex(SnsUserDto snsUserDto) {
        if (snsUserDto.getSex() == null) {
            return;
        }
        if (!snsUserDto.getSex().matches(SEX_REGEX)) {
            throw new IllegalArgumentException("Sex must be either M or F");
        }
    }

    /**
     * checks if address is not empty
     *
     * @param snsUserDto snsUserDto DTO with SNS User data
     */
    private void validateAddress(SnsUserDto snsUserDto) {
        if (snsUserDto.getAddress().matches("")) {
            throw new IllegalArgumentException("Address cannot be empty");
        }
    }

    /**
     * checks if phone number contains 9 digits
     *
     * @param snsUserDto DTO with SNS User data
     */
    private void validatePhoneNumber(SnsUserDto snsUserDto) {
        if (!snsUserDto.getPhoneNumber().matches(PHONE_NUMBER_REGEX)) {
            throw new IllegalArgumentException("Phone Number must contain 9 digits");
        }
    }

    /**
     * checks if email is of email format
     *
     * @param snsUserDto DTO with SNS User data
     */
    private void validateEmail(SnsUserDto snsUserDto) {
        if (!snsUserDto.getEmail().matches(EMAIL_REGEX)) {
            throw new IllegalArgumentException("Email format must be name@domain");
        }
    }

    /**
     * checks if password contains at least 2 numbers, 3 uppercase letters and length 7.
     *
     * @param snsUserDto DTO with SNS User data
     */
    private void validatePassword(SnsUserDto snsUserDto) {
        if (!snsUserDto.getPassword().matches(PASSWORD_REGEX_LENGTH)) {
            throw new IllegalArgumentException("Password must have at least 7 characters");
        }
        if (!snsUserDto.getPassword().matches(PASSWORD_REGEX_DIGIT)) {
            throw new IllegalArgumentException("Password must contain at least 2 digits");
        }
        if (!snsUserDto.getPassword().matches(PASSWORD_REGEX_UPPERCASE)) {
            throw new IllegalArgumentException("Password must at least contain 3 uppercase letters");
        }
    }

    /**
     * checks if birthdate is not greater than current date
     *
     * @param snsUserDto DTO with SNS User data
     */
    private void validateBirthdate(SnsUserDto snsUserDto) {
        LocalDate tomorrow = LocalDate.now();
        if (snsUserDto.getBirthDate().compareTo(tomorrow) > 0) {
            throw new IllegalArgumentException("Birthdate must not be higher than current date");
        }
    }

    /**
     * checks if SNS number contains 9 digits
     *
     * @param snsUserDto snsUserDto DTO with SNS User data
     */
    private void validateSnsNumber(SnsUserDto snsUserDto) {
        if (!snsUserDto.getSnsNumber().matches(SNS_NUMBER_REGEX)) {
            throw new IllegalArgumentException("SNS number must contain 9 digits");
        }
    }

    /**
     * checks if CC number contains 8 digits
     *
     * @param snsUserDto snsUserDto DTO with SNS User data
     */
    private void validateCcNumber(SnsUserDto snsUserDto) {
        if (!snsUserDto.getCcNumber().matches(CC_NUMBER_REGEX)) {
            throw new IllegalArgumentException("CC number must contain 8 digits");
        }
    }

    /**
     * Saves SNS user to auth
     *
     * @return true if user was successfully added, false if not
     */
    public boolean saveSnsUser() {
        return authFacade.addUserWithRole(name, email, password, Constants.ROLE_SNSUSER);
    }

    @Override
    public String toString() {
        return "Name: " + name + '\n' +
                "Address: " + address + '\n' +
                "Sex: " +
                Objects.requireNonNullElse(sex, "Unspecified") + '\n' +
                "Phone Number: " + phoneNumber + '\n' +
                "Email: " + email + '\n' +
                "Birth Date: " + birthDate + '\n' +
                "SNS Number: " + snsNumber + '\n' +
                "CC Number: " + ccNumber + "\n";
    }

    public String getNameSnsNumber() {
        return "Name: " + name + '\n' +
                "SNS Number: " + snsNumber + '\n';
    }
}
