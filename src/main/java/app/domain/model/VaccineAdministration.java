package app.domain.model;

import app.mappers.dto.VaccineAdministrationDto;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class VaccineAdministration implements Serializable {
    private SnsUser snsUser;
    private Vaccine vaccine;
    private LocalDateTime timeAdministred;
    private Dosage dosage;
    private VaccinationCenter vaccinationCenter;
    private LocalDateTime timeDeparted;


    public VaccineAdministration(VaccineAdministrationDto vaccineAdministrationDto) {
        this.snsUser = vaccineAdministrationDto.getSnsUser();
        this.vaccine = vaccineAdministrationDto.getVaccine();
        this.timeAdministred = vaccineAdministrationDto.getTimeAdministred();
        this.dosage = vaccineAdministrationDto.getDosage();
        this.vaccinationCenter = vaccineAdministrationDto.getVaccinationCenter();
    }

    public VaccineAdministration(SnsUser snsUser, Vaccine v, VaccinationCenter vc){
        this.snsUser = snsUser;
        this.vaccine = v;
        this.vaccinationCenter = vc;
        this.timeAdministred = LocalDateTime.now();
        this.timeDeparted = LocalDateTime.now();
        List<Integer> i = new ArrayList<>();
        i.add(1);
        i.add(2);
        this.dosage = new Dosage(1,i ,i);
    }

    //VALIDATIONS MISSING

    public SnsUser getSnsUser() {return snsUser;}

    public Vaccine getVaccine() {return vaccine;}

    public LocalDateTime getTimeAdministred() {return timeAdministred;}

    public Dosage getDosage() {return dosage;}

    public VaccinationCenter getVaccinationCenter(){return vaccinationCenter;}

    public LocalDateTime getTimeDeparted() {return timeDeparted;}

    public void setTimeDeparted(LocalDateTime timeDeparted){
        validateTimeDeparte(timeDeparted);
        this.timeDeparted = timeDeparted;
    }

    private void validateTimeDeparte(LocalDateTime timeDeparted){
        if (timeDeparted.isBefore(timeAdministred)){
            throw new IllegalArgumentException("Time departed can't be before time administred");
        }
    }

    @Override
    public String toString() {
        return "VaccineAdministration{" +
                "snsUser=" + snsUser +
                ", vaccine=" + vaccine +
                ", timeAdministred=" + timeAdministred +
                ", dosage=" + dosage +
                ", Vaccination center=" + vaccinationCenter +
                '}';
    }
}
