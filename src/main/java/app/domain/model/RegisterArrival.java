package app.domain.model;

import app.mappers.dto.RegisterArrivalDto;
import app.mappers.dto.SnsUserDto;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

public class RegisterArrival {
    
    private final List<Integer> timeArrived;
    private final String SNSNumber;
    private final VaccinationCenter vaccinationCenter;
    private final SnsUser snsUser;
    private final Date date;

    private static final String SNS_NUMBER_REGEX = "^[0-9]{9}$";

    public RegisterArrival(RegisterArrivalDto registerArrivalDto) {
        validateSnsNumber(registerArrivalDto);
        validateTime(registerArrivalDto);
        this.timeArrived = registerArrivalDto.getTimeArrived();
        this.SNSNumber = registerArrivalDto.getSNSNumber();
        this.vaccinationCenter = registerArrivalDto.getVaccinationCenter();
        this.snsUser = registerArrivalDto.getSnsUser();
        this.date = registerArrivalDto.getDate();
    }

    public List<Integer> getTimeArrived() {return timeArrived;}

    public String getSNSNumber() {return SNSNumber;}

    public VaccinationCenter getVaccinationCenter() {return vaccinationCenter;};

    public SnsUser getSnsUser() {return snsUser;}

    public Date getDate() {return date;}

    /**
     * checks if SNS number contains 9 digits
     *
     * @param registerArrivalDto registerArrivalDto DTO with User arrival data
     */
    private void validateSnsNumber(RegisterArrivalDto registerArrivalDto) {
        if (!registerArrivalDto.getSNSNumber().matches(SNS_NUMBER_REGEX)) {
            throw new IllegalArgumentException("SNS number must contain 9 digits");
        }
    }

    /**
     * checks if the time of the arrival was inserted correctly
     *
     * @param registerArrivalDto registerArrivalDto DTO with User arrival data
     */
    private void validateTime(RegisterArrivalDto registerArrivalDto){
        int hour = registerArrivalDto.getTimeArrived().get(0);
        int minute = registerArrivalDto.getTimeArrived().get(1);
        if (hour < 0 || hour > 23 ){
            throw new IllegalArgumentException("Hours were not inserted correctly");
        }
        if (minute < 0 || minute > 59){
            throw new IllegalArgumentException("Minutes were not inserted correctly");
        }
    }

    @Override
    public String toString() {
        return String.format("SNSUser: %s \nArrived at %d:%d \nVaccination Center: %s  \n",getSNSNumber(),getTimeArrived().get(0),getTimeArrived().get(1),getVaccinationCenter());
    }
}
