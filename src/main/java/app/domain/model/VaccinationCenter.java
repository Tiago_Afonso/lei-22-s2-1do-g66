package app.domain.model;

import app.mappers.dto.VaccinationCenterDto;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

public class VaccinationCenter implements Serializable {
    private final String name;
    private final String adress;
    private final String email;
    private final String website;
    private final transient LocalTime openingHour;
    private final transient LocalTime closingHour;
    private final int phoneNumber;
    private final int faxNumber;
    private final int slotDuration;
    private final int vaccinePerSlot;

    public static void validateVaccinationCenter(VaccinationCenterDto vaccinationCenterDto) {
        if (vaccinationCenterDto.getEmail().split("@").length != 2 && vaccinationCenterDto.getEmail().split("\\.").length != 2) {
            throw new IllegalArgumentException("Email is in the wrong format! \n Correct format -> xxxxxxx@xxxx.xx");
        } else if (vaccinationCenterDto.getWebsite().split("\\.").length != 2) {
            throw new IllegalArgumentException("Website is in the wrong format! \n Correct format -> xxxxxxx.xx");
        } else if (vaccinationCenterDto.getPhoneNumber() < 99999999 | vaccinationCenterDto.getPhoneNumber() > 1000000000) {
            throw new IllegalArgumentException("PhoneNumber is in the wrong format! \n Correct format -> xxx xxx xxx (9 digits)");
        }
    }

    public VaccinationCenter(VaccinationCenterDto vaccinationCenterDto) {
        this.name = vaccinationCenterDto.getName();
        this.adress = vaccinationCenterDto.getAdress();
        this.email = vaccinationCenterDto.getEmail();
        this.website = vaccinationCenterDto.getWebsite();
        this.openingHour = vaccinationCenterDto.getOpeningHour();
        this.closingHour = vaccinationCenterDto.getClosingHour();
        this.phoneNumber = vaccinationCenterDto.getPhoneNumber();
        this.faxNumber = vaccinationCenterDto.getFaxNumber();
        this.slotDuration = vaccinationCenterDto.getSlotDuration();
        this.vaccinePerSlot = vaccinationCenterDto.getVaccinePerSlot();
    }

    public VaccinationCenter(String name, String adress,String email, String website, LocalTime openingHour, LocalTime closingHour, int phoneNumber, int faxNumber, int slotDuration, int vaccinePerSlot){
this.name=name;
this.adress=adress;
this.email = email;
this.website=website;
this.openingHour=openingHour;
this.closingHour=closingHour;
this.phoneNumber=phoneNumber;
this.faxNumber=faxNumber;
this.slotDuration=slotDuration;
this.vaccinePerSlot=vaccinePerSlot;
}
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        VaccinationCenter that = (VaccinationCenter) o;
        return phoneNumber == that.phoneNumber ||
                faxNumber == that.faxNumber ||
                slotDuration == that.slotDuration ||
                vaccinePerSlot == that.vaccinePerSlot ||
                Objects.equals(name, that.name) ||
                Objects.equals(adress, that.adress) ||
                Objects.equals(email, that.email) ||
                Objects.equals(website, that.website) ||
                Objects.equals(openingHour, that.openingHour) ||
                Objects.equals(closingHour, that.closingHour);
    }

    public String getName() {
        return name;
    }

    public String getAdress() {
        return adress;
    }

    public String getEmail() {
        return email;
    }

    public String getWebsite() {
        return website;
    }

    public LocalTime getOpeningHour() {
        return openingHour;
    }

    public LocalTime getClosingHour() {
        return closingHour;
    }

    public int getPhoneNumber() {
        return phoneNumber;
    }

    public int getFaxNumber() {
        return faxNumber;
    }

    public int getSlotDuration() {
        return slotDuration;
    }

    public int getVaccinePerSlot() {
        return vaccinePerSlot;
    }
    public List<LocalDateTime> getNextAvailableDates(){
        LocalDateTime today = null;
        /*
        Method is meant to calculate a number of LocalDateTime intances that increase in 1 hour starting in the current time where
        the VaccinationCenter will be open to vaccination
        This List<LocalDateTime> is meant to represent the next number of working times of the vaccination center

         */
        return new ArrayList<>();
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, adress, email, website, openingHour, closingHour, phoneNumber, faxNumber, slotDuration, vaccinePerSlot);
    }

    @Override
    public String toString() {
        return "VaccinationCenter{" +
                "name='" + name + '\'' +
                ", email='" + email + '\'' +
                ", adress='" + adress + '\'' +
                ", website='" + website + '\'' +
                ", openingHour='" + openingHour + '\'' +
                ", closingHour='" + closingHour + '\'' +
                ", phoneNumber=" + phoneNumber +
                ", faxNumber=" + faxNumber +
                ", slotDuration=" + slotDuration +
                ", vaccinePerSlot=" + vaccinePerSlot +
                '}';
    }
}

