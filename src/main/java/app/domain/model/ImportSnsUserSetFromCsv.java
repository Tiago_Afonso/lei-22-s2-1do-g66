package app.domain.model;

import app.controller.App;
import app.domain.store.SnsUserStore;
import app.mappers.dto.SnsUserDto;

import java.io.BufferedReader;
import java.io.FileReader;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;


public class ImportSnsUserSetFromCsv {

    PasswordGenerator passwordGenerator = new PasswordGenerator();
    SnsUserStore snsUserStore = App.getInstance().getCompany().getSnsUserStore();

    /**
     * Load SNS users from CSV file
     * @param fileLocation CSV file path
     */
    public void readCsv(String fileLocation) {
        String line;
        String[] user;
        LocalDate userBirthdate;
        String separator;
        int i;

        try {
            BufferedReader bufferedReader = new BufferedReader(new FileReader(fileLocation));

            //Determine delimiter
            bufferedReader.mark(1);
            line = bufferedReader.readLine();
            if (line.contains(",") && !line.contains(";")) {
                separator = ",";
            } else if (line.contains(";") && !line.contains(",")) {
                separator = ";";
            } else {
                throw new Exception("Invalid separator");
            }

            //Determine if has header
            if (separator.equals(",")) {
                bufferedReader.reset();
                i = 1;
            } else {
                i = 2;
            }
            //read csv
            while ((line = bufferedReader.readLine()) != null){
                try {
                    user = line.split(separator);
                    //System.out.println(user.length);
                    if (user.length < 8) {
                        throw new Exception("Insufficient data");
                    } else if (user.length > 8) {
                        throw new Exception("Excessive data");
                    }
                    if (user[1].equalsIgnoreCase("n/a") || user[1].equals("")) {
                        user[1] = null;
                    }
                    try {
                       userBirthdate = LocalDate.parse(user[2], DateTimeFormatter.ofPattern("d/M/yyyy"));
                    } catch (Exception exception) {
                        throw new Exception("Invalid Date Format");
                    }
                    //name, sex, birthdate, address, phoneNumber, email, snsNumber, ccNumber
                    SnsUserDto snsUserDto = new SnsUserDto(user[0], user[3], user[1], user[4], user[5], passwordGenerator.generatePassword(7, 3, 2), userBirthdate, user[6], user[7]);
                    SnsUser snsUser = snsUserStore.createSnsUser(snsUserDto);
                    snsUserStore.saveSnsUser(snsUser);
                } catch (Exception exception) {
                    System.out.println("Error at line " + i + ": " + exception.getMessage() + ". Line skipped");
                }
                i++;
            }
        } catch (Exception exception) {
            System.out.println("Error: " + exception.getMessage() + ". CSV loading aborted");
        }
    }
}