package app.domain.model;

import java.util.*;

public class PasswordGenerator {

    private static final String uppercaseString = "ABCDEFGJKLMNPRSTUVWXYZ";
    private static final String numberString = "0123456789";
    private static final String allString = "abcdefghijklmnopqrstuvwxyz" + uppercaseString + numberString;

    /**
     * generates a random password
     * @param length password length (must not be smaller than uppercase + number)
     * @param uppercase password minimum uppercase characters
     * @param number password minimum digit characters
     * @return password String
     */
    public String generatePassword(int length, int uppercase, int number) {
        Random random = new Random();
        StringBuilder password = new StringBuilder();
        List<Character> temp = new ArrayList<>();

        if (length < uppercase + number) {
            return "";
        }

        for (int i = 0; i < uppercase; i++) {
            temp.add(uppercaseString.charAt(random.nextInt(uppercaseString.length())));
        }
        for (int i = 0; i < number; i++) {
            temp.add(numberString.charAt(random.nextInt(numberString.length())));
        }
        for (int i = 0; i < length - number - uppercase; i++) {
            temp.add(allString.charAt(random.nextInt(allString.length())));
        }

        Collections.shuffle(temp);
        for (Character s : temp) {
            password.append(s);
        }
        return password.toString();
    }
}
