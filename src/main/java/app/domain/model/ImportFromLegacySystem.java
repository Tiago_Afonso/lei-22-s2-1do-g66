package app.domain.model;

import app.controller.App;
import app.domain.store.InformationLegacySystemStore;
import app.domain.store.SnsUserStore;

import java.io.BufferedReader;
import java.io.FileReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ImportFromLegacySystem {
    private SnsUser snsUser;

    SnsUserStore storeS = App.getInstance().getCompany().getSnsUserStore();
    InformationLegacySystemStore store = App.getInstance().getCompany().getLsStore();

    PasswordGenerator passwordGenerator = new PasswordGenerator();

    public List<InformationLegacySystem> readFromLegacySystem(String path){
        List<InformationLegacySystem> result = new ArrayList<>();
        String line;
        String[] system;
        Date scheduleDateTime;
        Date arrivalDateTime;
        Date nurseAdminDateTime;
        Date leavingDateTime;
        String separator;
        SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy HH:mm");


        try {
            BufferedReader bufferedReader = new BufferedReader(new FileReader(path));

            //Determine delimiter
            bufferedReader.mark(1);
            line = bufferedReader.readLine();
            if (line.contains(",") && !line.contains(";")) {
                separator = ",";
            } else if (line.contains(";") && !line.contains(",")) {
                separator = ";";
            } else {
                throw new Exception("Invalid separator");
            }

            //Determine if has header
            system = line.split(separator);
            if(system.length != 8) {
                bufferedReader.reset();
            } else if (
                    !system[0].equalsIgnoreCase("SNSUSerNumber") &&
                            !system[1].equalsIgnoreCase("VaccineName") &&
                            !system[2].equalsIgnoreCase("Dose") &&
                            !system[3].equalsIgnoreCase("LotNumber") &&
                            !system[4].equalsIgnoreCase("ScheduledDateTime") &&
                            !system[5].equalsIgnoreCase("ArrivalDateTime") &&
                            !system[6].equalsIgnoreCase("NurseAdministrationDateTime") &&
                            !system[7].equalsIgnoreCase("LeavingDateTime")
            ) {
                bufferedReader.reset();
            }

            for (int i = 1; (line = bufferedReader.readLine()) != null; i++) {
                try {
                    system = line.split(separator);
                    //System.out.println(user.length);
                    if (system.length < 8) {
                        throw new Exception("Insufficient data");
                    } else if (system.length > 8) {
                        throw new Exception("Excessive data");
                    }
                    try {
//                        if (!existsSnsUserNum(system[0])) {
//                            createSNS(system[0]);
//                        }
                        scheduleDateTime = formatter.parse(system[4]);
                        arrivalDateTime = formatter.parse(system[5]);
                        nurseAdminDateTime = formatter.parse(system[6]);
                        leavingDateTime = formatter.parse(system[7]);

                        if(!storeS.getSnsUserList().contains(storeS.findUserBySNSNumber(system[0]))){
                            break;
                        } else {
                            InformationLegacySystem ls = store.create(system[0],system[1],system[2],system[3],scheduleDateTime,arrivalDateTime,nurseAdminDateTime,leavingDateTime);
                            //InformationLegacySystem ls = new InformationLegacySystem(system[0],system[1],system[2],system[3],scheduleDateTime,arrivalDateTime,nurseAdminDateTime,leavingDateTime);
                            store.saveInformation(ls);
                            result.add(ls);
                            if(ls == null){
                                System.out.println("Client not registered.");
                            }
                        }

                        //System.out.println(ls);

                    } catch (Exception exception) {
                        throw new Exception("Invalid Date Format");
                    }

                } catch (Exception exception) {
                    System.out.println("Error at line " + i + ": " + exception.getMessage() + ". Line skipped");
                }
            }
        } catch (Exception exception) {
            System.out.println("Error: " + exception.getMessage() + ". CSV loading aborted");
        }

        return result;
    }

//    public boolean existsSnsUserNum (String snsUserNum){
//        snsUser = storeS.findUserByNumber(snsUserNum);
//        return storeS.validateSnsUser(snsUser);
//    }
//
//    public void createSNS (String snsUserNum){
//        LocalDate userBirthdate = LocalDate.parse("13/10/2002", DateTimeFormatter.ofPattern("dd/MM/yyyy"));
//        System.out.println(userBirthdate);
//        SnsUserDto snsUserDto = new SnsUserDto("default", "default", "F", passwordGenerator.generatePassword(9, 0, 9), "email@gmail.com", passwordGenerator.generatePassword(7, 3, 2), userBirthdate, snsUserNum, passwordGenerator.generatePassword(8, 0, 9));
//
//        SnsUser snsUser = storeS.createSnsUser(snsUserDto);
//        storeS.saveSnsUser(snsUser);
//    }
}
