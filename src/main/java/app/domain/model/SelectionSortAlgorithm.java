package app.domain.model;

import app.domain.store.InformationLegacySystemStore;

import java.util.Date;
import java.util.prefs.InvalidPreferencesFormatException;

public class SelectionSortAlgorithm{

    public InformationLegacySystem[] selectionSortArrivalTime(InformationLegacySystem[] arr) {
        double start = System.nanoTime();
        for (int i = 0; i < arr.length - 1; i++){
            int aux = i; //primeira posicao
            for (int j = i + 1; j < arr.length; j++){ //ver se ha algum numero mais pequeno
                if (arr[j].getArrivalDateTime().before(arr[aux].getArrivalDateTime())){ //se houver um mais pequeno
                    aux = j;//aux passa a ser o mais pequeno
                }
            }
            InformationLegacySystem smaller = arr[aux]; //guardar o numero do aux que e o mais baixo num auxiliar smaller
            arr[aux] = arr[i]; //o numero da posicao aux fica com o valor da posicao a ser vista (i)
            arr[i] = smaller; //o da posicao i fica com o valor mais baixo que era o antigo aux
        }
        double end = System.nanoTime();
        double runtime = end - start;
        System.out.println("Runtime: " + runtime/1000000000 + "sec.");
        return arr;
    }

    public InformationLegacySystem[] selectionSortLeavingTime(InformationLegacySystem[] arr){
        double start = System.nanoTime();
        for (int i = 0; i < arr.length - 1; i++){
            int aux = i; //primeira posicao
            for (int j = i + 1; j < arr.length; j++){ //ver se ha algum numero mais pequeno
                if (arr[j].getLeavingDateTime().before(arr[aux].getLeavingDateTime())){ //se houver um mais pequeno
                    aux = j;//aux passa a ser o mais pequeno
                }
            }
            InformationLegacySystem smaller = arr[aux]; //guardar o numero do aux que e o mais baixo num auxiliar smaller
            arr[aux] = arr[i]; //o numero da posicao aux fica com o valor da posicao a ser vista (i)
            arr[i] = smaller; //o da posicao i fica com o valor mais baixo que era o antigo aux
        }
        double end = System.nanoTime();
        double runtime = end - start;
        System.out.println("Runtime: " + runtime/1000000+ "milissec.");
        return arr;
    }
}
