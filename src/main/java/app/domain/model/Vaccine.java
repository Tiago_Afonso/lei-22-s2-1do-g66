package app.domain.model;
import java.io.Serializable;
import java.util.List;

public class Vaccine implements Serializable {
    private String name;
    private String iD;
    private String brand;

    private VaccineType vType;

    private List<AgeGroup> ageGroups;
    public Vaccine(String name,String iD,String brand,VaccineType vType,List ageGroups){
        this.name=name;
        this.iD=iD;
        this.brand=brand;
        this.vType=vType;
        this.ageGroups=ageGroups;
    }
 public String getName(){
        return name;
 }

 public String getVaccineTypeDesc(){
        return vType.getDesignation();
 }

    @Override
    public String toString() {
        return "Vaccine{" +
                "name='" + name + '\'' +
                ", iD='" + iD + '\'' +
                ", brand='" + brand + '\'' +
                ", vType=" + vType +
                ", ageGroups=" + ageGroups +
                '}';
    }
}
