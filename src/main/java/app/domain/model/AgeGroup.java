package app.domain.model;
import app.controller.App;
import app.domain.model.Dosage;

public class AgeGroup {
    private int upperAge;
    private int lowerAge;

    private Dosage dosage;
    public AgeGroup (int upperAge,int lowerAge, Dosage dosage){
        this.upperAge=upperAge;
        this.lowerAge=lowerAge;
        this.dosage=dosage;
    }

    @Override
    public String toString() {
        return "AgeGroup{" +
                "upperAge=" + upperAge +
                ", lowerAge=" + lowerAge +
                ", dosage=" + dosage +
                '}';
    }
}
