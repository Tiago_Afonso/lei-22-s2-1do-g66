package app.domain.model;

import java.io.Serializable;

public class VaccineType implements Serializable {
    private int code;
    private String designation;

    public VaccineType(int code, String designation){
        this.code=code;
        this.designation=designation;
    }
    public int getCode(){
        return code;
    }
    public String getDesignation(){return designation; }

    @Override
    public String toString() {
        return "VaccineType{" +
                "code=" + code +
                ", designation='" + designation + '\'' +
                '}';
    }
}
