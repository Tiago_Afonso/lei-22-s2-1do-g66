package app.domain.model;

import app.mappers.dto.AdverseReactionDto;

import java.io.Serializable;

public class AdverseReaction implements Serializable {
    private final SnsUser snsUser;
    private final String description;

    public AdverseReaction(AdverseReactionDto adverseReactionDto) throws Exception {
        this.snsUser = adverseReactionDto.getSnsUser();
        this.description = adverseReactionDto.getDescription();
        validateDescription();
    }

    public SnsUser getSnsUser() {
        return snsUser;
    }

    public String getDescription() {
        return description;
    }

    public void validateDescription() throws Exception {
        if (this.description.length() > 300) {
            throw new Exception("Adverse reactions description cannot exceed 300 characters");
        }
    }

    @Override
    public String toString() {
        return snsUser.getNameSnsNumber() + "Description: " +  description;
    }
}
