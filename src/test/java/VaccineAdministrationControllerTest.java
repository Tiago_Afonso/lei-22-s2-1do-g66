import app.controller.App;
import app.controller.VaccineAdministrationController;
import app.domain.model.*;
import app.domain.store.WaitingRoomStore;
import app.mappers.dto.SnsUserDto;
import app.mappers.dto.VaccineAdministrationDto;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.junit.jupiter.api.Test;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
class VaccineAdministrationControllerTest implements Serializable {

    private final transient String name = "Name Test";
    private final transient String address = "Rua test";
    private final transient String sex = "F";
    private final transient String email1 = "test1@email.pt";
    private final transient String email2 = "test2@email.pt";
    private final transient String email3 = "test3@email.pt";
    private final transient String password = "PASSWORD123";
    private final transient LocalDate birthDate = LocalDate.now();
    private final transient String snsNumber1 = "123456789";
    private final transient String snsNumber2 = "223456789";
    private final transient String snsNumber3 = "323456789";
    private final transient String ccNumber1 = "12345678";
    private final transient String ccNumber2 = "22345678";
    private final transient String ccNumber3 = "32345678";
    private final transient String phoneNumber1 = "111222333";
    private final transient String phoneNumber2 = "211222333";
    private final transient String phoneNumber3 = "311222333";

    transient SnsUserDto snsUserDto1 = new SnsUserDto(name, address, sex, phoneNumber1, email1, password, birthDate, snsNumber1, ccNumber1);
    transient SnsUserDto snsUserDto2 = new SnsUserDto(name, address, sex, phoneNumber2, email2, password, birthDate, snsNumber2, ccNumber2);
    transient SnsUserDto snsUserDto3 = new SnsUserDto(name, address, sex, phoneNumber3, email3, password, birthDate, snsNumber3, ccNumber3);

    transient SnsUser snsUser1 = new SnsUser(snsUserDto1);
    transient SnsUser snsUser2 = new SnsUser(snsUserDto2);
    transient SnsUser snsUser3 = new SnsUser(snsUserDto3);

    transient VaccineType vt = new VaccineType (1234, "Covid19");
    transient List<Integer> vtList = new ArrayList<>(){{
        add(17);
        add(18);
    }};
    transient Vaccine vaccine = new Vaccine("Spikevax", "123456", "Moderna", vt, vtList);
    transient List<Integer> dosage1 = new ArrayList<>(){{
        add(30);
    }};
    transient List<Integer> timeInterval1 = new ArrayList<>(){{
        add(30);
    }};
    transient Dosage dosage = new Dosage(2,dosage1,timeInterval1);
    transient VaccinationCenter vaccinationCenter = new VaccinationCenter("Centro 1", "Rua do Centro 45", "centro1@mail.com", "centro1.com", LocalTime.of(8, 0), LocalTime.of(20,0), 939393939, 20202, 50, 10);

    transient VaccineAdministrationDto vaccineAdministrationDto1 = new VaccineAdministrationDto(snsUser1, vaccine, LocalDateTime.now(),dosage,vaccinationCenter);
    transient VaccineAdministrationDto vaccineAdministrationDto2 = new VaccineAdministrationDto(snsUser2, vaccine, LocalDateTime.now(),dosage,vaccinationCenter);
    transient VaccineAdministrationDto vaccineAdministrationDto3 = new VaccineAdministrationDto(snsUser3, vaccine, LocalDateTime.now(),dosage,vaccinationCenter);

    transient VaccineAdministration vaccineAdministration1 = new VaccineAdministration(vaccineAdministrationDto1);
    transient VaccineAdministration vaccineAdministration2 = new VaccineAdministration(vaccineAdministrationDto2);
    transient VaccineAdministration vaccineAdministration3 = new VaccineAdministration(vaccineAdministrationDto3);


    transient VaccineAdministrationController vaccineAdministrationController = new VaccineAdministrationController();
    @Test
    void createAdministration() {
        VaccineAdministration result = vaccineAdministrationController.createAdministration(vaccineAdministrationDto1);
        System.out.println(result);
        System.out.println(vaccineAdministration1);
        assertTrue(EqualsBuilder.reflectionEquals(result, vaccineAdministration1));
    }

    @Test
    void saveAdministration() {
        vaccineAdministrationController.createAdministration(vaccineAdministrationDto2);
        assertTrue(vaccineAdministrationController.saveAdministration(true));
    }

    @Test
    void getVaccineAdministrations() {
        List<VaccineAdministration> vaccineAdministrationList = new ArrayList<>();
        vaccineAdministrationList.add(vaccineAdministration1);
        vaccineAdministrationList.add(vaccineAdministration2);
        vaccineAdministrationList.add(vaccineAdministration3);
        vaccineAdministrationController.createAdministration(vaccineAdministrationDto1);
        vaccineAdministrationController.saveAdministration(true);
        vaccineAdministrationController.createAdministration(vaccineAdministrationDto2);
        vaccineAdministrationController.saveAdministration(true);
        vaccineAdministrationController.createAdministration(vaccineAdministrationDto3);
        vaccineAdministrationController.saveAdministration(true);
        assertNotEquals(vaccineAdministrationList, vaccineAdministrationController.getVaccineAdministrations());
    }

    @Test
    void getWaitingRoomStore() {
        WaitingRoomStore expected = App.getInstance().getCompany().getWaitingRoomStore();
        WaitingRoomStore result = vaccineAdministrationController.getWaitingRoomStore();
        assertEquals(expected,result);
    }

    @Test
    void getAvailableVaccines() {
        List<Vaccine> expected = App.getInstance().getCompany().getVaccineStore().getVaccineTypeList();
        List<Vaccine> result = vaccineAdministrationController.getAvailableVaccines();
        assertEquals(expected,result);
    }

    @Test
    void getDosages() {
        List<Dosage> expected = App.getInstance().getCompany().getDosageStore().getDosageList();
        List<Dosage> result = vaccineAdministrationController.getDosages();
        assertEquals(expected,result);
    }



}