package app.controller;

import app.domain.model.SnsUser;
import app.mappers.dto.SnsUserDto;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

class RegisterSnsUserControllerTest {

    private final String name = "Name Test";
    private final String address = "Rua test";
    private final String sex = "F";
    private final String email1 = "test1@email.pt";
    private final String email2 = "test2@email.pt";
    private final String email3 = "test3@email.pt";
    private final String password = "PASSWORD123";
    private final LocalDate birthDate = LocalDate.now();
    private final String snsNumber1 = "123456789";
    private final String snsNumber2 = "223456789";
    private final String snsNumber3 = "323456789";
    private final String ccNumber1 = "12345678";
    private final String ccNumber2 = "22345678";
    private final String ccNumber3 = "32345678";
    private final String phoneNumber1 = "111222333";
    private final String phoneNumber2 = "211222333";
    private final String phoneNumber3 = "311222333";

    SnsUserDto snsUserDto1 = new SnsUserDto(name, address, sex, phoneNumber1, email1, password, birthDate, snsNumber1, ccNumber1);
    SnsUserDto snsUserDto2 = new SnsUserDto(name, address, sex, phoneNumber2, email2, password, birthDate, snsNumber2, ccNumber2);
    SnsUserDto snsUserDto3 = new SnsUserDto(name, address, sex, phoneNumber3, email3, password, birthDate, snsNumber3, ccNumber3);


    SnsUser snsUser1 = new SnsUser(snsUserDto1);
    SnsUser snsUser3 = new SnsUser(snsUserDto3);

    RegisterSnsUserController registerSnsUserController = new RegisterSnsUserController();

    @Test
    void createSnsUser() {
        SnsUser result = registerSnsUserController.createSnsUser(snsUserDto1);
        System.out.println(result);
        System.out.println(snsUser1);
        assertTrue(EqualsBuilder.reflectionEquals(result, snsUser1));
    }

    @Test
    void saveSnsUser() {
        registerSnsUserController.createSnsUser(snsUserDto2);
        assertTrue(registerSnsUserController.saveSnsUser());
    }

    @Test
    void getSnsUsers() {
        List<SnsUser> snsUserList = new ArrayList<>();
        snsUserList.add(snsUser3);
        registerSnsUserController.createSnsUser(snsUserDto3);
        registerSnsUserController.saveSnsUser();
        assertEquals(snsUserList.toString().replaceAll("[\\[\\]]", ""), registerSnsUserController.getSnsUsers());

    }
}