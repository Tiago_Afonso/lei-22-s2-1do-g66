package app.domain.model;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import static org.junit.jupiter.api.Assertions.*;

class ImportUserSetFromCsvTest {
    ImportSnsUserSetFromCsv importSnsUserSetFromCsv = new ImportSnsUserSetFromCsv();

    private final PrintStream standardOut = System.out;
    private final ByteArrayOutputStream outputStreamCaptor = new ByteArrayOutputStream();

    //to test sOut
    @BeforeEach
    public void setUp() {
        System.setOut(new PrintStream(outputStreamCaptor));
    }

    //reset after test
    @AfterEach
    public void tearDown() {
        System.setOut(standardOut);
    }


    @Test
    void readCsvInvalidSeparator() {
        importSnsUserSetFromCsv.readCsv("src/test/app/domain/model/TestFiles/TestInvalidSeparator.csv");
        assertEquals("Error: Invalid separator. CSV loading aborted", outputStreamCaptor.toString().trim());
    }

    @Test
    void readCsvInsufficientData() {
        importSnsUserSetFromCsv.readCsv("src/test/app/domain/model/TestFiles/TestInsufficientData.csv");
        assertEquals("Error at line 1: Insufficient data. Line skipped", outputStreamCaptor.toString().trim());
    }

    @Test
    void readCsvExcessiveData() {
        importSnsUserSetFromCsv.readCsv("src/test/app/domain/model/TestFiles/TestExcessiveData.csv");
        assertEquals("Error at line 1: Excessive data. Line skipped", outputStreamCaptor.toString().trim());
    }

    @Test
    void readCsvInvalidDateFormat() {
        importSnsUserSetFromCsv.readCsv("src/test/app/domain/model/TestFiles/TestInvalidDateFormat.csv");
        assertEquals("Error at line 1: Invalid Date Format. Line skipped", outputStreamCaptor.toString().trim());
    }

}