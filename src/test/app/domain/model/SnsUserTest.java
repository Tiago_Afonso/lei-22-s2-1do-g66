package app.domain.model;

import app.mappers.dto.SnsUserDto;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.*;

class SnsUserTest {
    private final String name = "Name Test";
    private final String address = "Rua test";
    private final String sex = "F";
    private final String email = "test@email.pt";
    private final String password = "PASss12";
    private final LocalDate birthDate = LocalDate.now();
    private final String snsNumber = "123456789";
    private final String ccNumber = "12345678";
    private final String phoneNumber = "111222333";

    SnsUserDto snsUserDto = new SnsUserDto(name, address, sex, phoneNumber, email, password, birthDate, snsNumber, ccNumber);
    SnsUser snsUser = new SnsUser(snsUserDto);

    @Test
    void getSnsNumber() {
        assertEquals(snsNumber, snsUser.getSnsNumber());
    }

    @Test
    void getPhoneNumber() {
        assertEquals(phoneNumber, snsUser.getPhoneNumber());
    }

    @Test
    void getCcNumber() {
        assertEquals(ccNumber, snsUser.getCcNumber());
    }

    @Test
    void validateSnsUser() {
        assertDoesNotThrow(() -> new SnsUser(snsUserDto));
    }

    @Test
    void validateName() {
        SnsUserDto snsUserDto1 = new SnsUserDto("test@", address, sex, phoneNumber, email, password, birthDate, snsNumber, ccNumber);
        Exception exception1 = assertThrows(IllegalArgumentException.class, () -> new SnsUser(snsUserDto1));
        assertEquals("Name cannot contain numbers or special characters", exception1.getMessage());
    }

    @Test
    void validateSex() {
        SnsUserDto snsUserDto1 = new SnsUserDto(name, address, "test", phoneNumber, email, password, birthDate, snsNumber, ccNumber);
        Exception exception1 = assertThrows(IllegalArgumentException.class, () -> new SnsUser(snsUserDto1));
        assertEquals("Sex must be either M or F", exception1.getMessage());

    }

    @Test
    void validatePhoneNumber() {
        SnsUserDto snsUserDto1 = new SnsUserDto(name, address, sex, "98765432", email, password, birthDate, snsNumber, ccNumber);
        Exception exception1 = assertThrows(IllegalArgumentException.class, () -> new SnsUser(snsUserDto1));
        assertEquals("Phone Number must contain 9 digits", exception1.getMessage());

        SnsUserDto snsUserDto2 = new SnsUserDto(name, address, sex, "9876543210", email, password, birthDate, snsNumber, ccNumber);
        Exception exception2 = assertThrows(IllegalArgumentException.class, () -> new SnsUser(snsUserDto2));
        assertEquals("Phone Number must contain 9 digits", exception2.getMessage());
    }

    @Test
    void validateEmail() {
        SnsUserDto snsUserDto2 = new SnsUserDto(name, address, sex, phoneNumber, "test@", password, birthDate, snsNumber, ccNumber);
        Exception exception2 = assertThrows(IllegalArgumentException.class, () -> new SnsUser(snsUserDto2));
        assertEquals("Email format must be name@domain", exception2.getMessage());

        SnsUserDto snsUserDto3 = new SnsUserDto(name, address, sex, phoneNumber, "@email.com", password, birthDate, snsNumber, ccNumber);
        Exception exception3 = assertThrows(IllegalArgumentException.class, () -> new SnsUser(snsUserDto3));
        assertEquals("Email format must be name@domain", exception3.getMessage());
    }

    @Test
    void validatePassword() {
        SnsUserDto snsUserDto1 = new SnsUserDto(name, address, sex, phoneNumber, email, "PAS12", birthDate, snsNumber, ccNumber);
        Exception exception1 = assertThrows(IllegalArgumentException.class, () -> new SnsUser(snsUserDto1));
        assertEquals("Password must have at least 7 characters", exception1.getMessage());

        SnsUserDto snsUserDto2 = new SnsUserDto(name, address, sex, phoneNumber, email, "PAssw12", birthDate, snsNumber, ccNumber);
        Exception exception2 = assertThrows(IllegalArgumentException.class, () -> new SnsUser(snsUserDto2));
        assertEquals("Password must at least contain 3 uppercase letters", exception2.getMessage());

        SnsUserDto snsUserDto3 = new SnsUserDto(name, address, sex, phoneNumber, email, "PASswr1", birthDate, snsNumber, ccNumber);
        Exception exception3 = assertThrows(IllegalArgumentException.class, () -> new SnsUser(snsUserDto3));
        assertEquals("Password must contain at least 2 digits", exception3.getMessage());
    }

    @Test
    void validateBirthdate() {
        SnsUserDto snsUserDto1 = new SnsUserDto(name, address, sex, phoneNumber, email, password, birthDate.plusDays(1), snsNumber, ccNumber);
        Exception exception1 = assertThrows(IllegalArgumentException.class, () -> new SnsUser(snsUserDto1));
        assertEquals("Birthdate must not be higher than current date", exception1.getMessage());
    }

    @Test
    void validateSnsNumber() {
        SnsUserDto snsUserDto1 = new SnsUserDto(name, address, sex, phoneNumber, email, password, birthDate, "12345678", ccNumber);
        Exception exception1 = assertThrows(IllegalArgumentException.class, () -> new SnsUser(snsUserDto1));
        assertEquals("SNS number must contain 9 digits", exception1.getMessage());

        SnsUserDto snsUserDto2 = new SnsUserDto(name, address, sex, phoneNumber, email, password, birthDate, "1234567890", ccNumber);
        Exception exception2 = assertThrows(IllegalArgumentException.class, () -> new SnsUser(snsUserDto2));
        assertEquals("SNS number must contain 9 digits", exception2.getMessage());

    }

    @Test
    void validateCcNumber() {
        SnsUserDto snsUserDto1 = new SnsUserDto(name, address, sex, phoneNumber, email, password, birthDate, snsNumber, "1234567");
        Exception exception1 = assertThrows(IllegalArgumentException.class, () -> new SnsUser(snsUserDto1));
        assertEquals("CC number must contain 8 digits", exception1.getMessage());

        SnsUserDto snsUserDto2 = new SnsUserDto(name, address, sex, phoneNumber, email, password, birthDate, snsNumber, "123456789");
        Exception exception2 = assertThrows(IllegalArgumentException.class, () -> new SnsUser(snsUserDto2));
        assertEquals("CC number must contain 8 digits", exception2.getMessage());

    }

    @Test
    void saveSnsUser() {
        assertTrue(snsUser.saveSnsUser());
        assertFalse(snsUser.saveSnsUser());
    }

    @Test
    void testToString() {
        String expected = "Name: Name Test\n" +
                "Address: Rua test\n" +
                "Sex: F\n" +
                "Phone Number: 111222333\n" +
                "Email: test@email.pt\n" +
                "Birth Date: " + LocalDate.now() + "\n" +
                "SNS Number: 123456789\n" +
                "CC Number: 12345678";
        assertEquals(expected, snsUser.toString());
    }
}