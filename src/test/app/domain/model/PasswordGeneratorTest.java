package app.domain.model;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class PasswordGeneratorTest {
    PasswordGenerator passwordGenerator = new PasswordGenerator();

    @Test
    void generatePassword() {
        String password = passwordGenerator.generatePassword(7, 3, 2);
        String uppercase = password.chars().filter(Character::isUpperCase).collect(StringBuilder::new, StringBuilder::appendCodePoint, StringBuilder::append).toString();
        String number = password.chars().filter(Character::isDigit).collect(StringBuilder::new, StringBuilder::appendCodePoint, StringBuilder::append).toString();
        assertEquals(7, password.length());
        assertTrue(uppercase.length() >= 3);
        assertTrue(number.length() >= 2);
    }

}