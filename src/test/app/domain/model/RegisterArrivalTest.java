package app.domain.model;

import app.controller.RegisterSnsUserController;
import app.controller.RegisterVaccinationCenterController;
import app.domain.model.RegisterArrival;
import app.domain.model.SnsUser;
import app.mappers.dto.RegisterArrivalDto;
import app.mappers.dto.SnsUserDto;
import app.mappers.dto.VaccinationCenterDto;
import org.junit.jupiter.api.Test;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class RegisterArrivalTest {

    @Test
    void getTimeArrived() {
    }

    @Test
    void getSNSNumber() {
    }

    @Test
    void getVaccinationCenter() {
    }

    @Test
    void getSnsUser() {
    }

    @Test
    void getDate() {
    }

    @Test
    void testToString() {
    }

//    @Test
//    void validateSnSNumber(){
//        List<Integer> time = new ArrayList<>();
//        time.add(23); time.add(54);
//        VaccinationCenterDto vaccinationCenterDto1 = new VaccinationCenterDto("Centro 1", "Rua do Centro 45", "centro1@mail.com", "centro1.com", "8", "20", 939393939, 20202, 50, 10);
//        SnsUserDto snsUserDto1 = new SnsUserDto("Francisco", "Rua Do Francisco", "M", "934301197", "francisco@gmail.com", "1234567ABC", LocalDate.now(), "123456789", "12345678");
//        RegisterSnsUserController registerSnsUserController = new RegisterSnsUserController();
//        RegisterVaccinationCenterController registerVaccinationCenterController = new RegisterVaccinationCenterController();
//        Date date = getQuickDate("10-10-2001");
//
//        RegisterArrivalDto registerArrivalDto1 = new RegisterArrivalDto(time,"1231231234",registerVaccinationCenterController.createVaccinationCenter(vaccinationCenterDto1),registerSnsUserController.createSnsUser(snsUserDto1), date);
//        Exception exception1 = assertThrows(IllegalArgumentException.class, () -> new RegisterArrival(registerArrivalDto1));
//        assertEquals("SNS number must contain 9 digits", exception1.getMessage());
//    }

//    @Test
//    void validateTimeHours(){
//        List<Integer> time = new ArrayList<>();
//        time.add(25); time.add(54);
//        VaccinationCenterDto vaccinationCenterDto1 = new VaccinationCenterDto("Centro 1", "Rua do Centro 45", "centro1@mail.com", "centro1.com", "8", "20", 939393939, 20202, 50, 10);
//        SnsUserDto snsUserDto1 = new SnsUserDto("Francisco", "Rua Do Francisco", "M", "934301197", "francisco@gmail.com", "1234567ABC", LocalDate.now(), "123456789", "12345678");
//        RegisterSnsUserController registerSnsUserController = new RegisterSnsUserController();
//        RegisterVaccinationCenterController registerVaccinationCenterController = new RegisterVaccinationCenterController();
//        Date date = getQuickDate("10-10-2001");
//
//        RegisterArrivalDto registerArrivalDto1 = new RegisterArrivalDto(time,"123456789",registerVaccinationCenterController.createVaccinationCenter(vaccinationCenterDto1),registerSnsUserController.createSnsUser(snsUserDto1), date);
//        Exception exception1 = assertThrows(IllegalArgumentException.class, () -> new RegisterArrival(registerArrivalDto1));
//        assertEquals("Hours were not inserted correctly", exception1.getMessage());
//    }

//    @Test
//    void validateTimeMinutes(){
//        List<Integer> time = new ArrayList<>();
//        time.add(12); time.add(78);
//        VaccinationCenterDto vaccinationCenterDto1 = new VaccinationCenterDto("Centro 1", "Rua do Centro 45", "centro1@mail.com", "centro1.com", "8", "20", 939393939, 20202, 50, 10);
//        SnsUserDto snsUserDto1 = new SnsUserDto("Francisco", "Rua Do Francisco", "M", "934301197", "francisco@gmail.com", "1234567ABC", LocalDate.now(), "123456789", "12345678");
//        RegisterSnsUserController registerSnsUserController = new RegisterSnsUserController();
//        RegisterVaccinationCenterController registerVaccinationCenterController = new RegisterVaccinationCenterController();
//        Date date = getQuickDate("10-10-2001");
//
//        RegisterArrivalDto registerArrivalDto1 = new RegisterArrivalDto(time,"123456789",registerVaccinationCenterController.createVaccinationCenter(vaccinationCenterDto1),registerSnsUserController.createSnsUser(snsUserDto1), date);
//        Exception exception1 = assertThrows(IllegalArgumentException.class, () -> new RegisterArrival(registerArrivalDto1));
//        assertEquals("Minutes were not inserted correctly", exception1.getMessage());
//    }

    public Date getQuickDate(String toDate) {
        try {

            SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");

            Date date = df.parse(toDate);

            return date;
        } catch (
                ParseException ex) {
            throw new IllegalArgumentException();
        }

    }
}