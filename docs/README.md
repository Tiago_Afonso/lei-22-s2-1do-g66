# Integrating Project for the 2nd Semester of LEI-ISEP 2021-22

# 1. Team Members

The teams consists of students identified in the following table.

| Student Number	 | Name                     |
|-----------------|--------------------------|
| **1201305**     | Tiago Rafael Urze Afonso |
| **1191008**     | Rodrigo Braga Rodrigues  |
| **1180651**     | Jorge Rodrigues          |
| **1201850**     | Francisco Marques        |
| **1200835**     | Francisca Carmo          |



# 2. Task Distribution ###


Throughout the project's development period, the distribution of _tasks / requirements / features_ by the team members was carried out as described in the following table.

**Keep this table must always up-to-date.**

| Task                        | [Sprint A](SprintA/README.md) | [Sprint B](SprintB/README.md)   | [Sprint C](SprintC/README.md)   | [Sprint D](SprintD/README.md)   |
|-----------------------------|-------------------------------|---------------------------------|---------------------------------|---------------------------------|
| Glossary                    | [all](SprintA/Glossary.md)    | [all](SprintB/Glossary.md)      | [all](SprintC/Glossary.md)      | [all](SprintD/Glossary.md)      |
| Use Case Diagram (UCD)      | [all](SprintA/UCD.svg)        | [all](SprintB/UCD.svg)          | [all](SprintC/UCD.md)           | [all](SprintD/UCD.md)           |
| Supplementary Specification | [all](SprintA/FURPS.md)       | [all](SprintB/FURPS.md)         | [all](SprintC/FURPS.md)         | [all](SprintD/FURPS.md)         |
| Domain Model                | [all](SprintA/DM.md)          | [all](SprintB/DM.md)            | [all](SprintC/DM.md)            | [all](SprintD/DM.md)            |
| US03 (design + code)        |                               | [1201305](SprintB/US3/US3.md)   |                                 |                                 |
| US 010 (Design)             |                               | [1191008](SprintB/US10/US10.md) |                                 |                                 |
| US 009 (Design)             |                               | [1200835](SprintB/US09/US09.md) |                                 |                                 |
| US12(code)                  |                               | [1180651]                       |                                 |                                 |
| US13(documentation + code)  |                               | [1180651]                       |                                 |                                 |
| US14 (design + code)        |                               |                                 | [1201305](SprintC/US14/US14.md) |
| US1 (design + code)         |                               | [1180651]                       |                                 |   
| US2 (design)				  |								  |									| [1191008](SprintC/US2/US2.md)	  ||
| US18 (design)               |                               | [1180651] [1191008]             |                                 |
| US18 (code)                 |                               | [1180651]                       |                                 |
| US13 (refinement)           |                               | [1180651]                       |                                 |
| US12 (refinement)           |                               | [1180651]                       |                                 |
| US5 (design)                |                               | [1200835](SprintC/US5/US5.md)   |                                 |
| US07 (code + design)        |                               |                                 |                                 | [1201305](sprintD/US07/US07.md) |
| US08 (design)				  |								  |									|								  | [1191008](SprintD/US08/US08.md) |
| US15(Design)				  |                               |                                 |                                 | [1191008](SprintD/US15/US15.md) |