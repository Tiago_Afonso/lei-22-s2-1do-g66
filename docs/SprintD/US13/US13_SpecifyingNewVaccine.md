# US 13 - US13	As an administrator I wish to specify a new vaccine and it's administration process

## 1. Requirements Engineering


### 1.1. User Story Description


US13	As an administrator, I wish to specify a new vaccine and it's administration process

### 1.2. Customer Specifications and Clarifications


**From the specifications document:**

>"For instance, for the Covid-19 type, there is (i) the
Pfizer vaccine, (ii) the Moderna vaccine, (iii) the Astra Zeneca vaccine, and so on. The vaccine
administration process comprises (i) one or more age groups (e.g.: 5 to 12 years old, 13 to 18 years
old, greater than 18 years old), and (ii) per age group, the doses to be administered (e.g.: 1, 2, 3), the
vaccine dosage (e.g.: 30 ml), and the time interval regarding the previously administered dose.
Regarding this, it is important to notice that between doses (e.g.: between the 1st and 2nd doses) the
dosage to be administered might vary as well as the time interval elapsing between two consecutive
doses (e.g.: between the 1st and 2nd doses 21 days might be required, while between the 2nd and the
3rd doses 6 months might be required)."


**From the client clarifications:**

>"Each vaccine has the following attributes: Id, Name, Brand, Vaccine Type, Age Group, Dose Number, Vaccine Dosage and Time Since Last Dose.""




### 1.3. Acceptance Criteria


* **AC1:** All data must be given



### 1.4. Found out Dependencies


* Existing list of vaccine types


### 1.5 Input and Output Data


**Input Data:**

* Name
* ID
* Brand
* Age Groups
* Number of doses to be administered (for each age group selected)
* Vaccine dosage (for each age group selected)
* Time interval (for each age group selected-1) (between doses)

**Output Data:**

* List of vaccine types
* Vaccine data
* (In)Success of the operation

### 1.6. System Sequence Diagram (SSD)



![US13_SSD](US13_SSD.svg)



**Other alternatives might exist.**

### 1.7 Other Relevant Remarks

*

## 2. OO Analysis

### 2.1. Relevant Domain Model Excerpt

![US13_MD](US13_MD.svg)

### 2.2. Other Remarks

n/a


## 3. Design - User Story Realization

### 3.1. Rationale
| Interaction ID | Question: Which class is responsible for... | Answer  | Justification (with patterns)  |
|:-------------  |:--------------------- |:------------|:---------------------------- |
| Step 1  		 |	... interacting with the actor? | SpecifyVaccineUI   |  Pure Fabrication: there is no reason to assign this responsibility to any existing class in the Domain Model.           |
| 			  		 |	... coordinating the US? | SpecifyVaccineController | Controller                             |
| Step 2       |  ... asking for data?     | SpecifyVaccineUI               | Class used for user Interaction
| Step 3  		 |	                             |       |         |
| Step 4       |  ... having vaccine type store?  |  Company         |  |
|              |  ... having vaccine types?  |  VaccineTypeStore         | Class asigned to hold vaccine type information |
|              | ... showing vaccine type list? | SpecifyVaccineUI | Class used for user interaction |
| Step 5  		 |	 |   |  |
| Step 6       |  ... asking for data?                      | SpecifyVaccineUI      | Class used for user interaction      |
| Step 7       |  |   |   |
| Step 8       | ... asking for data? | SpecifyVaccineUI | Class used for user interaction |
| Step 9       | ... creating age group object? | AgeGroup  | IE: The object created in step 9 has its own data.|
|              | ... creating dosage object? | Dosage | IE: The object created in step 9 has its own data. |
|              | ... saving dosage object? | DosageStore | Class asigned to save dosage objects |
|              | ... saving age group object? | AgeGroupStore  | Class asigned to save age group objects |
|              | ... associating age group and dosage objects to the vaccine? | Vaccine | Class used to create Vaccine objects |
| Step 10      | ...showing the data and asking for confirmation? | SpecifyVaccineUI | Class used for user interaction |
| Step 11      |  |   |   |
| Step 12      | ...creating new vaccine object | Vaccine   | IE: The object created in step 12 has its own data.  |
|              | ...saving new vaccine information | VaccineStore   | IE: Store pattern   |
| Step 13      | ...showing the data? | SpecifyVaccineUI | Class used for user interaction |
### Systematization ##

**According to the taken rationale, the conceptual classes promoted to software classes are:**

* Company
* AgeGroup
* Dosage
* Vaccine

**Other software classes (i.e. Pure Fabrication) identified:**

* SpecifyVaccineUI
* SpecifyVaccineController
* VaccineTypeStore
* VaccineStore
* AgeGroupStore
* DosageStore



## 3.2. Sequence Diagram (SD)

**Alternative 1**
![US13_SD](US13_SD.svg)

**Other alternatives may exist**

## 3.3. Class Diagram (CD)

**From alternative 1**
![US13_CD](US13_CD.svg)

# 4. Tests


# 5. Construction (Implementation)

*In this section, it is suggested to provide, if necessary, some evidence that the construction/implementation is in accordance with the previously carried out design. Furthermore, it is recommeded to mention/describe the existence of other relevant (e.g. configuration) files and highlight relevant commits.*

*It is also recommended to organize this content by subsections.*

# 6. Integration and Demo

*In this section, it is suggested to describe the efforts made to integrate this functionality with the other features of the system.*


# 7. Observations

*In this section, it is suggested to present a critical perspective on the developed work, pointing, for example, to other alternatives and or future related work.*
