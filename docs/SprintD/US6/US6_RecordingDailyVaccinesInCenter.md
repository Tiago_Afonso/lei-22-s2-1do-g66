# US 6 - DGS wants to record the daily number of people vaccincated in each vaccination center

## 1. Requirements Engineering


### 1.1. User Story Description


As the company, I want to record the daily number of people vaccinated in a given center

### 1.2. Customer Specifications and Clarifications


**From the specifications document:**
"DGS wants to record daily the total number of people vaccinated in each vaccination center"
"The algorithm should run automatically at a time defined in a configuration file and should registerthe date, the name of the vaccination center and the total number of vaccinated users"


**From the client clarifications:**




### 1.3. Acceptance Criteria

* **AC1:** The algorithm should run automatically at a time defined in a configuration file and should register the date, the name of the vaccination center and the total number of vaccinated users


### 1.4. Found out Dependencies
* Loaded list of vaccination centers
* Loaded list of vaccines adminitered
* Configuration file

### 1.5 Input and Output Data


**Input Data:**



**Output Data:**
* Number of vaccinated people in the specific day and center
* Insuccess of the operation (may it happen)

### 1.6. System Sequence Diagram (SSD)



![US6_SSD](US6_SSD.svg)



**Other alternatives might exist.**

### 1.7 Other Relevant Remarks

*

## 2. OO Analysis

### 2.1. Relevant Domain Model Excerpt

![US6_MD](US6_MD.svg)

### 2.2. Other Remarks

n/a


## 3. Design - User Story Realization

### 3.1. Rationale
| Interaction ID | Question: Which class is responsible for... | Answer  | Justification (with patterns)  |
|:-------------  |:--------------------- |:------------|:---------------------------- |
| Step 1	  	 |	... coordinating the US? | RecordDailyVaccinesController  | IE: Controller                             |
| Step 2       |  ...holding store for vaccination centers?     |  Company              | No reason to assign this to any other class  |
|    |  ...having vaccination centers saved? | VaccinationCenterStore | HC+LC pattern |
| Step 3       | ...holding stored vaccines that have been administered? | Company |  No reason to assign this to any other class |
| Step 3      | ...having administered vaccines stored?? | VaccineAdministrationStore |
| Step 4  		 |	...calculating the number of vaccines for each center?                              | RecordDailyVaccinesController       | US coordinator        |
|   | ...having the daily record of vaccines stored? | Company | No reason to assign this to any other class |
|       |  ...saving info into file?  | DailyRecordVaccineStore       | HC+LC pattern  |
| Step 5  		 |	...informing insuccess of operation | DailyRecordVaccineStore  | Error will be saved in the binary file |


### Systematization ##

**According to the taken rationale, the conceptual classes promoted to software classes are:**
Company
**Other software classes (i.e. Pure Fabrication) identified:**
RecordDailyVaccinesController
VaccinationCenterStore
VaccineAdministrationStore
DailyRecordVaccineStore


## 3.2. Sequence Diagram (SD)

**Alternative 1**
![US6_SD](US6_SD.svg)

**Other alternatives may exist**

## 3.3. Class Diagram (CD)

**From alternative 1**
![US6_CD](US6_CD.svg)

# 4. Tests


# 5. Construction (Implementation)

*In this section, it is suggested to provide, if necessary, some evidence that the construction/implementation is in accordance with the previously carried out design. Furthermore, it is recommeded to mention/describe the existence of other relevant (e.g. configuration) files and highlight relevant commits.*

*It is also recommended to organize this content by subsections.*

# 6. Integration and Demo

*In this section, it is suggested to describe the efforts made to integrate this functionality with the other features of the system.*


# 7. Observations

*In this section, it is suggested to present a critical perspective on the developed work, pointing, for example, to other alternatives and or future related work.*
