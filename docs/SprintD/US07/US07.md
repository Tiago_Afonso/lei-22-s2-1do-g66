# US 07 - Register Adverse Reactions

## 1. Requirements Engineering

### 1.1. User Story Description

As a Nurse, I intend to record adverse reactions of a SNS user.

### 1.2. Customer Specifications and Clarifications 

**From the provided document**

>If the nurse identifies any
adverse reactions during that recovery period, the nurse should record the adverse reactions in the
system.

**From the customer clarifications**
>**Question**: "[1] Should the adverse reactions only be recorded on the program, on a text file in a computer folder, or both? [2] And if the SNS User that the nurse intends to record an adverse reaction isn't in the recovery room, should the system notify her about that issue?"
>
>**Answer**:
>1. The adverse reactions should not be recorded in a file.
>2. No.

>Question: "When recording adverse reactions, does the Nurse need to follow a form? If so, what are its attributes?"
>
>Answer: The nurse does not need to fill a form. The nurse only writes a text with at most 300 characters.

>Question: "What are the requirements for a sns user's adverse reactions to be recorded? Is it only necessary to confirm the existence of the chosen sns user or there's something else?"
>
>Answer: If the nurse identifies any adverse reactions during that recovery period, the nurse should record the adverse reactions in the system. The nurse must enter the user's SNS number and must describe the adverse reactions.

>Question: "in this US (07) , the registration of adverse reactions is simply by indicating the sns user that received the shot or we need to register the adverse reactions by sns user arrival time? By this, i meant, that the nurse registers sns user's adverse reaction by order of arrival, automatically, or the system is manual, like she/he needs to indicate the name and etc? "
>
>Answer: The nurse identifies adverse reactions during the recovery period. But the nurse can register the adverse reactions whenever she wants.

>Question: "1 Question: The SNS User will record the reactions or it's only the nurse? 2 Question : The Nurse must select one SNS User and then write the reactions of that SNS User? 3 Question : When the Nurse will record those reactions? "
>
>Answer: If the nurse identifies any adverse reactions during that recovery period, the nurse should record the adverse reactions in the system. The nurse must enter the user's SNS number and must describe the adverse reactions.

### 1.3. Acceptance Criteria

* **AC01:** Adverse reaction description as a maximum of 300 characters

* **AC02:** Only the Nurse role can record adverse reactions

* **AC03:** Adverse reactions can be recorded at any time (even if the recovery period has ended)

### 1.4. Found out Dependencies

US03: responsible for SNS Users.

### 1.5 Input and Output Data

Input Data:
* SNS number

Output Data:
* List of SNS Users SNS number and name
* Operation (in)success

### 1.6. System Sequence Diagram (SSD)

![US07-SSD](US07-SSD.svg)


### 1.7 Other Relevant Remarks

No other relevant remarks

## 2. OO Analysis

### 2.1. Relevant Domain Model Excerpt 
*In this section, it is suggested to present an excerpt of the domain model that is seen as relevant to fulfill this requirement.* 

![US07-MD](US07-MD.svg)

### 2.2. Other Remarks

No other relevant remarks

## 3. Design - User Story Realization 

### 3.1. Rationale

**The rationale grounds on the SSD interactions and the identified input/output data.**

| Interaction ID                                          | Question: Which class is responsible for...   | Answer                    | Justification (with patterns)                                                                                |
|:--------------------------------------------------------|:----------------------------------------------|:--------------------------|:-------------------------------------------------------------------------------------------------------------|
| Step 1: start recording adverse reactions               | ... interacting ith the Nurse?                | AdverseReactionGUI        | Pure Fabrication: there is no reason to assign this responsibility to any existing class in the Domain Model |
|                                                         | ... coordinating the US?                      | AdverseReactionController | Controller: responsible for receiving or handling a system event                                             |
| Step 2: shows list of SNS users                         | ... knowing SNS Users                         | SnsUserStore              | HC+LC: has all SNS users                                                                                     |
|                                                         | ... transfer data from domain to UI           | SnsUserDto/SnsUserMapper  | DTO: using a DTO to obtain all SNS users SNS number and name                                                 |
| Step 3: selects an SNS user and writes adverse reaction | ... transfer data from UI to domain           | AdverseReactionDto        | DTO: using a DTO to process data                                                                             |
|                                                         | ... get SnsUser object of selected SNS number | SnsUserStore              | HC+LC: has all SNS users                                                                                     | 
|                                                         | ... instancing a new adverse reaction         | AdverseReactionStore      | HC+LC: has all adverse reactions                                                                             |
|                                                         | ... validating adverse reaction               | AdverseReaction           | Creator: has its own data                                                                                    |
|                                                         | ... save adverse reaction                     | AdverseReactionStore      | HC+LC: has all adverse reactions                                                                             |
| Step 4: inform operation (in)success                    |                                               |                           |                                                                                                              |
### Systematization ##

According to the taken rationale, the conceptual classes promoted to software classes are: 

* AdverseReaction
* AdverseReactionStore

Other software classes (i.e. Pure Fabrication) identified: 
  
 * AdverseReactionGUI
 * AdverseReactionController

## 3.2. Sequence Diagram (SD)

![US07-SD](US07-SD.svg)
![SD_GET_SNSUSER_LIST](SD_GET_SNSUSER_LIST.svg)

## 3.3. Class Diagram (CD)

![US07-CD](US07-CD.svg)

# 4. Tests

# 5. Construction (Implementation)

# 6. Integration and Demo 

# 7. Observations