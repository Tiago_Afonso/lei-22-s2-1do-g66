# US4 - To record the administration of a vaccine to a SNS User

## 1. Requirements Engineering


### 1.1. User Story Description

As a nurse, I want to record the administration of a vaccine to a SNS user. At the end of the recovery period, the user should receive a SMS message informing the SNS user that he can leave the vaccination center.

### 1.2. Customer Specifications and Clarifications


**From the specifications document:**

> At any time, a nurse responsible for administering the vaccine will use the application to check the
list of SNS users that are present in the vaccination center to take the vaccine and will call one SNS
user to administer him/her the vaccine.

> Usually, the user that has arrived firstly will be the first one
to be vaccinated (like a FIFO queue). However, sometimes, due to operational issues, that might not
happen. The nurse checks the user info and health conditions in the system and in accordance with
the scheduled vaccine type, and the SNS user vaccination history, (s)he gets system instructions
regarding the vaccine to be administered (e.g.: vaccine and respective dosage considering the SNS
user age group).

> After giving the vaccine to the user, each nurse registers the event in the system,
more precisely, registers the vaccine type (e.g.: Covid-19), vaccine name/brand (e.g.: Astra Zeneca,
Moderna, Pfizer), and the lot number used.

> Afterwards, the nurse sends the user to a recovery room,
to stay there for a given recovery period (e.g.: 30 minutes). If there are no problems, after the given
recovery period, the user should leave the vaccination center. The system should be able to notify
(e.g.: SMS or email) the user that his/her recovery period has ended. 

**From the client clarifications:**

* **Question:** Regarding the recovery period, how should we define it? Is it the same for all vaccines or should the nurse specify in each case what the recovery time is?
* **Answer:** The recovery period/time is the same for all vaccines. The recovery period/time should be defined in a configuration file.

* **Question:** As we can read in Project Description, the vaccination flow follows these steps: 1. Nurse calls one user that is waiting in the waiting room to be vaccinated;
                2. Nurse checks the user's health data as well as which vaccine to administer; 3. Nurse administers the vaccine and registers its information in the system.
                The doubt is: do you want US08 to cover steps 2 and 3, or just step 3?

* **Answer:** 1.The nurse selects a SNS user from a list. 2. Checks user's Name, Age and Adverse Reactions registered in the system. 3. Registers information about the administered vaccine.

### 1.3. Acceptance Criteria

* **AC1:**  The nurse should select a vaccine and the administred dose number.

### 1.4. Found out Dependencies

* There is a dependency to US4 as there needs to be users already in the waiting room, US12 and US13 as there needs to be an already existing vaccine and vaccine type and US7 as I need to know a user's adverse reactions to know which vaccine to administrate.

### 1.5 Input and Output Data

**Input Data:**
* **Typed Data:**
  + Vaccine type and vaccine name/brand and lot number used
    
* **Selected Data:**
  + SNS User from the waiting list

**Output Data:**
* SMS notification regarding the user's end of recovery period
* (In)Success of the operation

### 1.6. System Sequence Diagram (SSD)



![US08_SSD](US08_SSD.svg)



**Other alternatives might exist.**

### 1.7 Other Relevant Remarks

n/a

## 2. OO Analysis

### 2.1. Relevant Domain Model Excerpt

![US08_DM](US08_DM.svg)

### 2.2. Other Remarks

n/a


## 3. Design - User Story Realization

### 3.1. Rationale

| Interaction ID | Question: Which class is responsible for... | Answer  | Justification (with patterns)  |
|:-------------  |:--------------------- |:------------|:---------------------------- |
| Step 1      |	... interacting with the actor? | VaccineAdministrationUI   |  Pure Fabrication: there is no reason to assign this responsibility to any existing class in the Domain Model. |
| 			  		 |	... coordinating the US? | VaccineAdministrationController | Controller: responsible for receiving or handling a system event |
| Step 2       |  ... asking for data?     | VaccineAdministrationUI  | Class used for user Interaction |
|   		   |	 |   |  |
| Step 3  	   |  ... having users in the waiting room? |  ListSNSUsersWaitingRoomStore | Class used to hold a list of users in the waiting room |
| Step 3       | ...showing user data and asking for confirmation? | VaccineAdministrationUI  | Class used for user interaction  |
|   		   |	 |   |  |
| Step 4       |  ... having adverse reaction store?  |  Company         | Company knows user adverse reactions  |
| Step 4       |  ... having existing user's adverse reactions? | AdverseReactionStore | Class assigned to hold user's adverse reactions (HCLC) |
| Step 4       |  ... showing user's adverse reactions and asking for confirmation? | VaccineAdministrationUI | Class used for user Interaction |
|   		   |	 |   |  |
| Step 5       |  ... having vaccine schedule store? | Company | Company knows vaccine schedules |
| Step 5       |  ... having existing vaccine schedules and types?  |  VaccineScheduleStore     | Class used to hold vaccine schedule information |
| Step 5       |  ... having dosage store?  |  Company     | Company knows vaccine dosages |
| Step 5       |  ... having existing dosages?  |  DosageStore     | Class used to hold vaccine dosage information |
|              |     |   |  |
| Step 6       | ...showing data and asking for confirmation? | VaccineAdministrationUI  | Class used for user interaction  |
|              |     |   |  |
| Step 7       | ...creating a vaccine administration instance? | VaccineAdministration | Class knows its own data |
| Step 7       | ...saving administration data?  | VaccineAdministrationStore  | Class assigned to hold user vaccine administration data |
| Step 7       | ...initiating the recovery period notifier timer? | VaccineAdministrationController | Controller: responsible for receiving or handling a system event |
| Step 7       | ...showing (in)success of operation? | VaccineAdministrationUI   | Class used for user interaction   |


### Systematization ##

According to the taken rationale, the conceptual classes promoted to software classes are:
* Company
* VaccineAdministration

Other software classes (i.e. Pure Fabrication) identified:
* VaccineAdministrationUI
* VaccineAdministrationController
* ListSNSUsersWaitingRoomStore
* AdverseReactionStore
* VaccineScheduleStore
* DosageStore
* VaccineAdministrationStore

## 3.2. Sequence Diagram (SD)

![US08_SD](US08_SD.svg)


**Other alternatives may exist**

## 3.3. Class Diagram (CD)

![US08_CD](US08_CD.svg)

# 4. Tests

# 5. Construction (Implementation)

## Class RegisterArrivalController

# 6. Integration and Demo

* A new option on the Nurse menu options was added.
