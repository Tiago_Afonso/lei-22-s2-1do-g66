# Supplementary Specification (FURPS+)

## Functionality

### 1. Localization
The application must support the Portuguese and English languages.
### 2. Email
The system should be able to send SMS and email to the user.
### 3. Reporting
Support to generate reports of the vaccination process.
### 4. Security
* All those who wish to use the application must be authenticated with a password holding seven alphanumeric characters, including three capital letters and two digits.
* Only the nurses are allowed to access all user’s health data.
### 5. System management
Each center coordinator manages a center.
### 6. Workflow
The software must be able to identify current processes and workflows that might be sources of inefficiency, delay, duplication of effort, or wasted time in the practice.

## Usability
* None were identified

## Reliability
* None were identified

## Performance
* None were identified

## Supportability

### 1. Adaptability
* Application can be used for other outbreaks (and vaccine types)

### 2. Compatability
* TODO

### 3. Configurability
* Admin configures core information (e.g.: type of vaccines, vaccines, vaccination centers, employees)

### 4. Scalability
* Can be commercialized by other companies/organization
* Can support other vaccines besides COVID-19

## +

### Design Constraints
* Analysis, requirements engineering and development should be in accordance with OO practices;

### Implementation Constraints

* Application should be developed in java using Intellij IDE/NetBeans
* Implement tests for all methods, except for methods that implement Input/Output operation using JUnit 5
* Coverage report using JaCoCo plugin
* Graphical Interface developed in JavaFX 11

### Interface Constraints

* Issuance of EU COVID Digital Certificate to be implemented by DGS IT department

### Physical Constraints

* None were identified