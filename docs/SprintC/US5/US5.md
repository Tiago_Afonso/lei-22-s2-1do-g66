# US 5 - Consult the users in the waiting room of a vacination center
## 1. Requirements Engineering
### 1.1. User Story Description

As a nurse, I intend to consult the users in the waiting room of a vacination center.

### 1.2. Customer Specifications and Clarifications

**From the specifications document:**

> The receptionist should send the SNS user to a waiting room where (s)he should wait for his/her time.

> A nurse responsible for administering the vaccine will use the application to check the list of SNS users that are present in the vaccination center

**From the client clarifications:**

>* Question: "Is it supposed to remove a SNS user of the wait list when he leaves the waitting room to get the vaccine? If yes, how do we know when  the sns user leaves the waitting room?"
>
> 
>* Answer: "US5 is only to list users that are in the waiting room of a vaccination center. In Sprint D we will introduce new user stories."

>* Question: "What information about the Users (name, SNS number, etc) should the system display when listing them?"
>
> 
>* Answer: "Name, Sex, Birth Date, SNS User Number and Phone Number."

>* Question: "Regarding US05, the listing is supposed to be for the day itself or for a specific day."
>
>
>* Answer: "The list should show the users in the waiting room of a vaccination center."

>* Question: "Regarding the US05. In the PI description it is said that, by now, the nurses and the receptionists will work at any center. Will this information remain the same on this Sprint, or will they work at a specific center?"
>
>
>* Answer: "Nurses and receptionists can work in any vaccination center."

>* Question: "Regarding US 05, what does consulting constitute in this context? Does it refer only to seeing who is present and deciding who gets the vaccine or is checking the user info to administer the vaccine, registering the process, and sending it to the recovery room also part of this US?"
>
>
>* Answer: "The goal is to check the list of users that are waiting and ready to take the vaccine."

>* Question: "We need to know if the nurse have to chose the vaccination center before executing the list or if that information comes from employee file?"
>
>
>* Answer: "When the nurse starts to use the application, firstly, the nurse should select the vaccination center where she his working. The nurse wants to check the list of SNS users that are waiting in the vaccination center where she his working."

>* Question: "I would like to know which are the attributes of the waiting room."
>
>
>* Answer: "The waiting room will not be registered or defined in the system. The waiting room of each vaccination center has the capacity to receive all users who take the vaccine on given slot."


### 1.3. Acceptance Criteria

SNS Users’ list should be presented by order of arrival.

### 1.4. Found out Dependencies

* Dependency to US9 where the vaccination centers are created.
* Dependency to US3 where the SNS Users are registered.
* Dependency to US10 where the employee (nurse in this case) is registered.

### 1.5 Input and Output Data

**Input Data:**

* Selected data:
    * VaccinationCenter 

**Output Data:**

* List of vaccination centers
* The list of sns users that are in the waiting room of the selected vaccination center

### 1.6. System Sequence Diagram (SSD)

![US5-SSD](SSD/US5-SSD.svg)

### 1.7 Other Relevant Remarks

n/a

## 2. OO Analysis
### 2.1. Relevant Domain Model Excerpt

![US5-DM](DM/US5-DM.png)

### 2.2. Other Remarks

n/a

## 3. Design - User Story Realization
### 3.1. Rationale

| Interaction ID                                                         | Question: Which class is responsible for... | Answer                            | Justification (with patterns)                                                                                 |
|:-----------------------------------------------------------------------|:--------------------------------------------|:----------------------------------|:--------------------------------------------------------------------------------------------------------------|
| Step 1: Choose to see the list of SNS users in the waiting room        | 	... interacting with the nurse?            | ListSNSUsersWaitingRoomUI         | Pure Fabrication: there is no reason to assign this responsibility to any existing class in the Domain Model. |
| 			  		                                                                | 	... coordinating the US?                   | ListSNSUsersWaitingRoomController | Controller                                                                                                    |
| 	Step 2: Shows a list of vaccination centers and asks to select one    | 	... having vaccination center store?       | Company                           | IE: Company knows VaccinationCenterStore                                                                      |
| 			  		                                                                | 	... having vaccination centers?            | VaccinationCenterStore            | IE: VaccinationCenterStore has the information of VaccinationCenters                                          |
| 			  		                                                                | 	... showing vaccine type list?             | ListSNSUsersWaitingRoomUI         | IE: is responsible for user interactions.                                                                     |
| Step 3: Selects a vaccination center 		                                | 		n/a					                                  |                                   |                                                                                                               |
| Step 4: Shows the list of SNS users in the waiting room                      | ...having waiting room store?               | Company                           | IE: Company knows WaitingRoomStore                                                                            |
| 		                                                                     | 	...having waiting rooms?                   | WaitingRoomStore                  | IE: WaitingRoomStore has the information of WaitingRoom                                                       |
|                                                                        | ...showing sns users in the waiting room?   | ListSNSUsersWaitingRoomUI         | IE: is responsible for user interactions.                                                |

### Systematization ##

According to the taken rationale, the conceptual classes promoted to software classes are:

* VaccinationCenterStore
* WaitingRoomStore
* Company

Other software classes (i.e. Pure Fabrication) identified:

* ListSNSUsersWaitingRoomUI
* ListSNSUsersWaitingRoomController
* RegisterVaccinationCenterController

## 3.2. Sequence Diagram (SD)

![US5-SD](SD/US5-SD.svg)

## 3.3. Class Diagram (CD)

![US5-CD](CD/US5-CD.svg)

# 4. Tests

**Test 1:** Check the get method

    private final String name = "Name Test";
    private final String address = "Rua test";
    private final String sex = "F";
    private final String email1 = "test1@email.pt";
    private final String email2 = "test2@email.pt";
    private final String email3 = "test3@email.pt";
    private final String password = "PASSWORD123";
    private final LocalDate birthDate = LocalDate.now();
    private final String snsNumber1 = "123456789";
    private final String snsNumber2 = "223456789";
    private final String snsNumber3 = "323456789";
    private final String ccNumber1 = "12345678";
    private final String ccNumber2 = "22345678";
    private final String ccNumber3 = "32345678";
    private final String phoneNumber1 = "111222333";
    private final String phoneNumber2 = "211222333";
    private final String phoneNumber3 = "311222333";

    SnsUserDto snsUserDto1 = new SnsUserDto(name, address, sex, phoneNumber1, email1, password, birthDate, snsNumber1, ccNumber1);
    SnsUserDto snsUserDto2 = new SnsUserDto(name, address, sex, phoneNumber2, email2, password, birthDate, snsNumber2, ccNumber2);
    
    SnsUser snsUser1 = new SnsUser(snsUserDto1);
    SnsUser snsUser2 = new SnsUser(snsUserDto2);
    

    ListSNSUsersWaitingRoomStore store = new ListSNSUsersWaitingRoomStore();
    
    store.addSNSUserWR(snsUserDto1);
    store.addSNSUserWR(snsUserDto2);
    
    @Test
    public void getSNSUsersWaitingRoomTest(){
        boolean expected = store.getListSNSUsersWaitingRoom();
        List<SNSUserDto> list = new ArrayList<>();
        list.add(snsUserDto1);
        list.add(snsUserDto2);
        boolean result = list;
        Assert.assertEquals(expected, result);
    }

}

# 5. Construction (Implementation)

## Class ListSNSUsersWaitingRoomController

        public List<SNSUsersWaitingRoom> getListSNSUsersWaitingRoom() {
        return this.listSNSUsersWaitingRoomStore.getListSNSUsersWaitingRoom();
    }


## Class WaitingRoomStore


		public ListSNSUsersWaitingRoomStore(SNSUsersWaitingRoom listsnsUsersWaitingRoom) {
        SNSUsersWaitingRoom.add(listsnsUsersWaitingRoom);
    }

    public List<SNSUsersWaitingRoom> getListSNSUsersWaitingRoom() {
        return SNSUsersWaitingRoom;
    }

# 6. Integration and Demo

* A new option on the Nurse menu options was added.




