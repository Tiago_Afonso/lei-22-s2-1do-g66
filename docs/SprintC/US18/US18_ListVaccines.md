# US 18 - US18 As a center coordinator, I want to get a list of all vaccines.

## 1. Requirements Engineering


### 1.1. User Story Description


As a center coordinator, I want to get a list of all vaccines.

### 1.2. Customer Specifications and Clarifications


**From the specifications document:**



**From the client clarifications:**




### 1.3. Acceptance Criteria


* **AC1:** All data must be given
* **AC2:** The vaccines should be grouped by their type and then
alphabetically listed by its name.


### 1.4. Found out Dependencies


* Existing list of vaccines in the system


### 1.5 Input and Output Data


**Input Data:**

*
**Output Data:**

* List of vaccines
* (In)Success of the operation

### 1.6. System Sequence Diagram (SSD)



![US18_SSD](US18_SSD.svg)



**Other alternatives might exist.**

### 1.7 Other Relevant Remarks

*

## 2. OO Analysis

### 2.1. Relevant Domain Model Excerpt

![US18_MD](US18_MD.svg)

### 2.2. Other Remarks

n/a


## 3. Design - User Story Realization

### 3.1. Rationale
| Interaction ID | Question: Which class is responsible for... | Answer  | Justification (with patterns)  |
|:-------------  |:--------------------- |:------------|:---------------------------- |
| Step 1  		 |	... interacting with the actor? | ListVaccinesUI   |  Pure Fabrication: there is no reason to assign this responsibility to any existing class in the Domain Model.           |
| 			  		 |	... coordinating the US? | ListVaccinesController | Controller                             |
| Step 2       |  ...having vaccine store?     | Company               | Company knows vaccines |
| Step 2      | ...knowing all the saved vaccines? | VaccineStore |
 HCLC pattern |
| Step 3     | ...showing vaccine list? | ListVaccinesUI | Class used for user interaction |
### Systematization ##

**According to the taken rationale, the conceptual classes promoted to software classes are:**


**Other software classes (i.e. Pure Fabrication) identified:**




## 3.2. Sequence Diagram (SD)

**Alternative 1**
![US18_SD](US18_SD.svg)

**Other alternatives may exist**

## 3.3. Class Diagram (CD)

**From alternative 1**
![US18_CD](US18_CD.svg)

# 4. Tests


# 5. Construction (Implementation)

*In this section, it is suggested to provide, if necessary, some evidence that the construction/implementation is in accordance with the previously carried out design. Furthermore, it is recommeded to mention/describe the existence of other relevant (e.g. configuration) files and highlight relevant commits.*

*It is also recommended to organize this content by subsections.*

# 6. Integration and Demo

*In this section, it is suggested to describe the efforts made to integrate this functionality with the other features of the system.*


# 7. Observations

*In this section, it is suggested to present a critical perspective on the developed work, pointing, for example, to other alternatives and or future related work.*
