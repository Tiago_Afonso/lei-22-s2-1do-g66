# US 2 - As a receptionist at one vaccination center, I want to schedule a vaccination.
## 1. Requirements Engineering
### 1.1. User Story Description

As a receptionist at one vaccination center, I want to schedule a vaccination.

### 1.2. Customer Specifications and Clarifications

**From the specifications document:**

>	Each vaccine schedule has a date, and needs to be associated with a vaccine type, an SNSUser and a vaccination center.

> A Receptionist is responsible for properly registering vaccine schedules for a SNSUser.

**From the client clarifications:**

>Question: Could you explain the acceptance criteria for this User Story?
Answer: "The algorithm has to check which vaccine the SNS user took before and check if the conditions (age and time since the last vaccine) are met. If the conditions are met the vaccination event should be scheduled and registered in the system. When scheduling the first dose there is no need to check these conditions."

>Question: Should we assume that the vaccination center has to be the same one that the receptionist works at?

### 1.3. Acceptance Criteria

* **AC1:** Vaccine schedules need to be related to a SNSuser, vaccine type and vaccination center.

* **AC2** The algorithm should check if the SNS User is within the age and time since the last vaccine.

* **AC3** The SNSUser must not have another scheduled vaccination at the time of the registration.

### 1.4. Found out Dependencies

* Loaded list of vaccine types.
* Loaded list of SNSUsers.
* Loaded list of vaccination centers.

### 1.5 Input and Output Data

**Input Data:**

* Typed data:
    * SNSUser's number
    * Vaccine type's code
    * Vaccination center's name

**Output Data:**

* All the information of the vaccine scheduled.
* Success or failure of the operation

### 1.6. System Sequence Diagram (SSD)

![US2_SSD](US2_SSD.svg)

### 1.7 Other Relevant Remarks

n/a

## 2. OO Analysis
### 2.1. Relevant Domain Model Excerpt

![US2_MD](US2_MD.svg)

### 2.2. Other Remarks

n/a

## 3. Design - User Story Realization
### 3.1. Rationale

| Interaction ID  | Question: Which class is responsible for...   | Answer | Justification (with patterns) |
|:-----------------------------------------------------------------------|:----------------------------------------------|:------------------------------------|:--------------------------------------------------------------------------------------------------------------|
|Step 1: Choose to register the vaccine schedule| ...interacting with the Receptionist?| ScheduleVaccinationUI | Pure Fabrication: there is no reason to assign this responsibility to any existing class in the Domain Model. |
|| ...coordinating the US?| ScheduleVaccinnationController| Controller |
|| ...instantiating the new vaccine schedule?| VaccineScheduleStore| Creator (Rule 1): in the DM, the Receptionist manages the vaccine schedules.  |
|Step 2: Requests SNSUser number| n/a|||
|Step 3: Inputs SNSUser number | ...saving the number submitted? | ScheduleVaccinnationUI| I.E knows the receptionist's inputs|
|Step 4: Validates the SNSUser | ...checking if user exists? | UserStore | I.E stores all known users|
|| ...checking if the user has another vaccine already scheduled?| VaccineScheduleStore| I.E knows all vaccine schedules|
|Step 5: Shows Vaccine type list| ...storing all known vaccine type instances?| VaccineTypeStore| I.E stores all known vaccine types|
|Step 6: Selects Vaccine type| ...saving the chosen vaccine type?| ScheduleVaccinationUI| I.E knows the receptionist's inputs|
|Step 7: Shows vaccination center list| ...storing all known vaccination center instances?| VaccinationCenterStore| I.E stores all known vaccination centers|
|Step 8: Selects vaccination center |...saving the chosen vaccination center?| ScheduleVaccinnationUI| I.E knows the receptionist's inputs|
|Step 9: Shows available dates for scheduling|...calculating available dates for vaccination?| VaccineScheduleStore| I.E knows user's previous vaccine type's identifying attributes|
||...providing the previous vaccine's specifications?|VaccineType|I.E knows its own attributes|
||...providing the user's previous vaccine type?| VaccineTypeStore|I.E stores all known vaccine types|
|Step 10: Selects date| ...saving the chosen date?| ScheduleVaccinnationUI| I.E knows the receptionist's inputs|
|Step 11: Validates resulting vaccine schedule?| ...validating the data localy?| VaccineSchedule| I.E knows its own parameters|
|| ...validating the data globaly?| VaccineScheduleStore| I.E knows already existing vaccine schedules|
|| ...checking if a vaccine exists for the specified vaccine type and age group?| VaccineStore| I.E stores all known vaccine instances|
|Step 12: Asks confirmation| ...requesting user confimation?|ScheduleVaccinationUI||
|Step 13: Confirms input| n/a|||
|Step 14: Informs of operation's success| ...informing the receptionist of the operation's success?| ScheduleVaccinationUI||

### Systematization ##

According to the taken rationale, the conceptual classes promoted to software classes are:

 * UserStore
 * SNSUser
 * VaccineSchedule
 * VaccineType

Other software classes (i.e. Pure Fabrication) identified:

 * ScheduleVaccinationUI  
 * ScheduleVaccinnationController
 * VaccineScheduleStore
 * VaccinationCenterStore
 * VaccineTypeStore

## 3.2. Sequence Diagram (SD)
## 3.2.1 SD
![US2_SD](US2_SD.svg)

## 3.3. Class Diagram (CD)

![US2_CD](US2_CD.svg)

# 4. Tests

**Test 1:** Check that vaccine types can't be created with null attributes

*It is also recommended to organize this content by subsections.*

# 5. Construction (Implementation)



# 6. Integration and Demo

* A new option on the Receptionist's menu options was added.

* Some demo purposes some users are bootstrapped while system starts.
