# US4 - To register an user's arrival

## 1. Requirements Engineering


### 1.1. User Story Description

As a receptionist at a vaccination center, I want to register the arrival of a SNS user to take the vaccine.

### 1.2. Customer Specifications and Clarifications


**From the specifications document:**

> The receptionist asks the SNS user for his/her SNS user number and
confirms that he/she has the vaccine scheduled for the that day and time. If the information is
correct, the receptionist acknowledges the system that the user is ready to take the vaccine.

**From the client clarifications:**

* **Question:** The receptionist must have the possibility to choose which center she wants to register the SNS user's arrival every time she uses this feature, or should we make the nurse pick a center after they log in?
* **Answer:** To start using the application, the receptionist should first select the vaccination center where she is working.

### 1.3. Acceptance Criteria

* **AC1:** No duplicate entries should be possible for the same SNS user on the same day or vaccine period. 

### 1.4. Found out Dependencies

* There is a dependency to US1 and US2 as to register a SNS user's arrival there needs to be a vaccination scheduled.

### 1.5 Input and Output Data

**Input Data:**
* **Typed Data:**
  + SNS user number
  + Time of arrival
    
* **Selected Data:**
  + Vaccination center

**Output Data:**
* Day and time of the schedule
* (In)Success of the operation

### 1.6. System Sequence Diagram (SSD)



![US4_SSD](US4_SSD.svg)



**Other alternatives might exist.**

### 1.7 Other Relevant Remarks

n/a

## 2. OO Analysis

### 2.1. Relevant Domain Model Excerpt

![US4_DM](US4_DM.svg)

### 2.2. Other Remarks

n/a


## 3. Design - User Story Realization

### 3.1. Rationale

| Interaction ID | Question: Which class is responsible for... | Answer  | Justification (with patterns)  |
|:-------------  |:--------------------- |:------------|:---------------------------- |
| Step 1  		 |	... interacting with the actor? | RegisterArrivalUI   |  Pure Fabrication: there is no reason to assign this responsibility to any existing class in the Domain Model.           |
| 			  		 |	... coordinating the US? | RegisterArrivalController | Controller                             |
| Step 2       |  ... asking for data?     | RegisterArrivalUI               | Class used for user Interaction
|   		 |	                             |       |         |
| Step 3       |  ... having vaccination center store?  |  Company         | Company knows vaccination centers  |
|              |  ... having existing vaccination centers?  |  VaccinationCenterStore         | Class assigned to hold vaccination centers. |
|              | ... showing vaccination center list? | RegisterArrivalUI | Class used for user interaction |
|   		 |	 |   |  |
| Step 4       |  ...having user arrivals store?      | Company      | Company knows user arrivals     |
|    | ... having existing user arrivals? | RegisterArrivalStore | Class assigned to hold user arrivals |
|    | ...having vaccine schedules stored? | Company | Company knows scheduled times |
|    | ...having available times? | VaccineScheduleStore | Class assigned to hold vaccine scheduling times. |
|      |  |   |   |
| Step 5       | ...showing data and asking for confirmation? | RegisterArrivalUI  | Class used for user interaction  |
|        |  |   | |
| Step 6      | ...creating a user arrival instance? | RegisterArrival | Class knows its own data |
|       | ...saving arrival data?  | RegisterArrivalStore  | Class assigned to hold user arrival data |
| Step 7      | ...showing (in)success of operation? | RegisterArrivalUI   | Class used for user interaction   |


### Systematization ##

According to the taken rationale, the conceptual classes promoted to software classes are:

* RegisterArrival
* Company

Other software classes (i.e. Pure Fabrication) identified:

* RegisterArrivalUI
* RegisterArrivalController
* VaccinationCenterStore
* RegisterArrivalStore
* VaccineScheduleStore

## 3.2. Sequence Diagram (SD)

![US4_SD](US4_SD.svg)


**Other alternatives may exist**

## 3.3. Class Diagram (CD)

![US4_CD](US4_CD.svg)

# 4. Tests

**Test 1:** Checking the SNS number validation

```java

@Test
    void validateSnSNumber(){
        List<Integer> time = new ArrayList<>();
        time.add(23); time.add(54);
        VaccinationCenterDto vaccinationCenterDto1 = new VaccinationCenterDto("Centro 1", "Rua do Centro 45", "centro1@mail.com", "centro1.com", "8", "20", 939393939, 20202, 50, 10);
        SnsUserDto snsUserDto1 = new SnsUserDto("Francisco", "Rua Do Francisco", "M", "934301197", "francisco@gmail.com", "1234567ABC", LocalDate.now(), "123456789", "12345678");
        RegisterSnsUserController registerSnsUserController = new RegisterSnsUserController();
        RegisterVaccinationCenterController registerVaccinationCenterController = new RegisterVaccinationCenterController();
        Date date = getQuickDate("10-10-2001");

        RegisterArrivalDto registerArrivalDto1 = new RegisterArrivalDto(time,"1231231234",registerVaccinationCenterController.createVaccinationCenter(vaccinationCenterDto1),registerSnsUserController.createSnsUser(snsUserDto1), date);
        Exception exception1 = assertThrows(IllegalArgumentException.class, () -> new RegisterArrival(registerArrivalDto1));
        assertEquals("SNS number must contain 9 digits", exception1.getMessage());
    }
```

**Test 2:** Checking if hour value assigned is valid

```java
@Test
    void validateTimeHours(){
        List<Integer> time = new ArrayList<>();
        time.add(25); time.add(54);
        VaccinationCenterDto vaccinationCenterDto1 = new VaccinationCenterDto("Centro 1", "Rua do Centro 45", "centro1@mail.com", "centro1.com", "8", "20", 939393939, 20202, 50, 10);
        SnsUserDto snsUserDto1 = new SnsUserDto("Francisco", "Rua Do Francisco", "M", "934301197", "francisco@gmail.com", "1234567ABC", LocalDate.now(), "123456789", "12345678");
        RegisterSnsUserController registerSnsUserController = new RegisterSnsUserController();
        RegisterVaccinationCenterController registerVaccinationCenterController = new RegisterVaccinationCenterController();
        Date date = getQuickDate("10-10-2001");

        RegisterArrivalDto registerArrivalDto1 = new RegisterArrivalDto(time,"123456789",registerVaccinationCenterController.createVaccinationCenter(vaccinationCenterDto1),registerSnsUserController.createSnsUser(snsUserDto1), date);
        Exception exception1 = assertThrows(IllegalArgumentException.class, () -> new RegisterArrival(registerArrivalDto1));
        assertEquals("Hours were not inserted correctly", exception1.getMessage());
    }
```

**Test 3:** Checking if minutes value assigned is valid

```java
@Test
    void validateTimeMinutes(){
        List<Integer> time = new ArrayList<>();
        time.add(12); time.add(78);
        VaccinationCenterDto vaccinationCenterDto1 = new VaccinationCenterDto("Centro 1", "Rua do Centro 45", "centro1@mail.com", "centro1.com", "8", "20", 939393939, 20202, 50, 10);
        SnsUserDto snsUserDto1 = new SnsUserDto("Francisco", "Rua Do Francisco", "M", "934301197", "francisco@gmail.com", "1234567ABC", LocalDate.now(), "123456789", "12345678");
        RegisterSnsUserController registerSnsUserController = new RegisterSnsUserController();
        RegisterVaccinationCenterController registerVaccinationCenterController = new RegisterVaccinationCenterController();
        Date date = getQuickDate("10-10-2001");

        RegisterArrivalDto registerArrivalDto1 = new RegisterArrivalDto(time,"123456789",registerVaccinationCenterController.createVaccinationCenter(vaccinationCenterDto1),registerSnsUserController.createSnsUser(snsUserDto1), date);
        Exception exception1 = assertThrows(IllegalArgumentException.class, () -> new RegisterArrival(registerArrivalDto1));
        assertEquals("Minutes were not inserted correctly", exception1.getMessage())
    }
```

# 5. Construction (Implementation)

## Class RegisterArrivalController

```java
public RegisterArrival createArrival(RegisterArrivalDto registerArrivalDto){
        registerArrival = registerArrivalStore.createArrivalTime(registerArrivalDto);
        registerArrivalStore.validateArrival(registerArrival);
        return registerArrival;
    }
```

```java
 public boolean saveArrival() {return this.registerArrivalStore.saveArrival(registerArrival);}
```

```java
public String getArrivals() {return this.registerArrivalStore.toString();}
```

```java
public List<RegisterArrival> getArrivalsData() {return this.registerArrivalStore.getListUserArrivalTimes();}
```

```java
public SnsUser getUserFromSNSNumber(String snsNumber) {
        for (SnsUser snsUsers : snsUserStore.getSnsUserList()) {
            if (snsUsers.getSnsNumber().equals(snsNumber)) {
                return snsUsers;
            }
        }
        throw new IllegalArgumentException("There's no SNS user registered with respective SNS number");
    }
```

# 6. Integration and Demo

* A new option on the Receptionist menu options was added.
