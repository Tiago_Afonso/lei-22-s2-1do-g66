# US 1 -	As an SNSUser I wish to schedule my vaccination

## 1. Requirements Engineering


### 1.1. User Story Description


As an SNSUser, I wish to schedule my vaccination

### 1.2. Customer Specifications and Clarifications


**From the specifications document:**
To take a vaccine, the SNS user should use the application to schedule his/her vaccination. The user
should introduce his/her SNS user number, select the vaccination center, the date, and the time (s)he
wants to be vaccinated as well as the type of vaccine to be administered (by default, the system
suggests the one related to the ongoing outbreak). Then, the application should check the
vaccination center capacity for that day/time and, if possible, confirm that the vaccination is
scheduled and inform the user that (s)he should be at the selected vaccination center at the
scheduled day and time. The SNS user may also authorize the DGS to send a SMS message with
information about the scheduled appointment. If the user authorizes the sending of the SMS, the
application should send an SMS message when the vaccination event is scheduled and registered in
the system.

**From the client clarifications:**
The acceptance criteria for US1 and US2 are: a. A SNS user cannot schedule the same vaccine more than once.



### 1.3. Acceptance Criteria


* **AC1:** All data must be given/selected
* **AC2:** A SNS user cannot schedule the same vaccine more than
once.


### 1.4. Found out Dependencies
* Loaded list of vaccination centers
* Loaded list of vaccine types

### 1.5 Input and Output Data


**Input Data:**

* SNS number
* Vaccination center (selection)
* Vaccine type (selection)
* Date and time (selection)


**Output Data:**

* List of vaccination centers
* List of vaccine types
* List of available times for scheduling
* Data introduced/selected by user (confirmation of data)
* (In)Success of the operation

### 1.6. System Sequence Diagram (SSD)



![US1_SSD](US1_SSD.svg)



**Other alternatives might exist.**

### 1.7 Other Relevant Remarks

*

## 2. OO Analysis

### 2.1. Relevant Domain Model Excerpt

![US1_MD](US1_MD.svg)

### 2.2. Other Remarks

n/a


## 3. Design - User Story Realization

### 3.1. Rationale
| Interaction ID | Question: Which class is responsible for... | Answer  | Justification (with patterns)  |
|:-------------  |:--------------------- |:------------|:---------------------------- |
| Step 1  		 |	... interacting with the actor? | ScheduleVaccineUI   |  Pure Fabrication: there is no reason to assign this responsibility to any existing class in the Domain Model.           |
| 			  		 |	... coordinating the US? | ScheduleVaccineController  | Controller                             |
| Step 2       |  ... asking for data?     | ScheduleVaccineUI               | Class used for user Interaction |
| Step 3       | ...having sns users stored? | Company | Company knows sns users |
| Step 3      | ...using the introduced sns user number to get the sns user? ! SNSUserDTO/SNSUserMapper |
| Step 3  		 |	...validating sns user? number?                             | SNSUserStore       | Class assigned to hold sns users        |
| Step 4       |  ... having vaccination center store?  |  Company         | Company knows vaccination centers  |
|              |  ... having existing vaccination centers?  |  VaccinationCenterStore         | Class assigned to hold vaccination centers. |
|              | ... showing vaccination center list? | ScheduleVaccineUI | Class used for user interaction |
| Step 5  		 |	 |   |  |
| Step 6       |  ...having vaccine type store?                      | Company      | Company knows vaccine types     |
| Step 6    | ... having existing  vaccine types? | VaccineTypeStore | Class assigned to hold vaccine types. |
| Step 6    | ...having vaccine schedules stored? | Company | Company knows scheduled times |
| Step 6    | ...having available times? | VaccineScheduleStore | Class assigned to hold vaccine scheduling times. |
| Step 7       |  |   |   |
| Step 8       | ...showing data and asking for confirmation? | ScheduleVaccineUI  | Class used for user interaction  |
| Step 9       |  |   | |
| Step 10      | ...creating vaccine schedule instance? | VaccineSchedule | Class knows its own data |
| Step 10      | ...saving schedule data?  | VaccineScheduleStore  | Class assigned to hold vaccine schedules. |
| Step 11      | ...showing (in)success of operation? | ScheduleVaccineUI   | Class used for user interaction   |

### Systematization ##

**According to the taken rationale, the conceptual classes promoted to software classes are:**
VaccineSchedule
Company
VaccineType
VaccinationCenter
HealthCareCenter
SNSUser

**Other software classes (i.e. Pure Fabrication) identified:**
VaccineTypeStore
VaccineScheduleStore
VaccinationCenterStore
HealthCareCenterStore
ScheduleVaccineUI
ScheduleVaccineController
SNSUserStore
SNSUserMapper
SNSUserDTO


## 3.2. Sequence Diagram (SD)

**Alternative 1**
![US1_SD](US1_SD.svg)

**Other alternatives may exist**

## 3.3. Class Diagram (CD)

**From alternative 1**
![US1_CD](US1_CD.svg)

# 4. Tests


# 5. Construction (Implementation)

*In this section, it is suggested to provide, if necessary, some evidence that the construction/implementation is in accordance with the previously carried out design. Furthermore, it is recommeded to mention/describe the existence of other relevant (e.g. configuration) files and highlight relevant commits.*

*It is also recommended to organize this content by subsections.*

# 6. Integration and Demo

*In this section, it is suggested to describe the efforts made to integrate this functionality with the other features of the system.*


# 7. Observations

*In this section, it is suggested to present a critical perspective on the developed work, pointing, for example, to other alternatives and or future related work.*
