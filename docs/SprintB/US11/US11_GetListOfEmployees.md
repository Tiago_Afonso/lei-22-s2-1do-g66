# US 11 - US11 As an administrator, I want to get a list of Employees with a given function/ role

## 1. Requirements Engineering


### 1.1. User Story Description


US11 As an administrator, I want to get a list of Employees with a given function/ role

### 1.2. Customer Specifications and Clarifications


**From the specifications document:**

> Employees are distinguished from regular client because of the presence of a role in their user data

**From the client clarifications:**

* **Question:** What attributes of each employee should be shown on the list?
* **Answer:** The application should present all Employee attributes.

### 1.3. Acceptance Criteria

* **AC1:** Each employee must have a role 

### 1.4. Found out Dependencies

No dependencies were found

### 1.5 Input and Output Data

**Input Data:**
* **Typed Data:** (none)
* **Selected Data:** (none)

**Output Data:**
* List of employees and their respective functions/roles
* (In)Success of the operation

### 1.6. System Sequence Diagram (SSD)



![US11_SSD](US11_SSD.svg)



**Other alternatives might exist.**

### 1.7 Other Relevant Remarks

n/a

## 2. OO Analysis

### 2.1. Relevant Domain Model Excerpt

![US11_MD](US11_DM.svg)

### 2.2. Other Remarks

n/a


## 3. Design - User Story Realization

### 3.1. Rationale

| Interaction ID                                                         | Question: Which class is responsible for...   | Answer                              | Justification (with patterns)                                                                                 |
|:-----------------------------------------------------------------------|:----------------------------------------------|:------------------------------------|:--------------------------------------------------------------------------------------------------------------|
| Step 1: Request list of employees and their functions/roles            | 	... interacting with the administrator?      | RequestEmployeeListUI   | Pure Fabrication: there is no reason to assign this responsibility to any existing class in the Domain Model. |
| 			  		                                            | 	... coordinating the US?                     | RequestEmployeeListController | Controller                                                                                                    |
| 			  		                                                                | 	... get the user list?  | Company      | Company                                        |
| 			  		                                                                | 	... hold the user list data?  | UserStore      | Store                                        |
| Step 2: Show list of employees and respective functions/roles and inform operation success | 	...showing the list of employees?  |  RequestEmployeeListUI  |                                         |
|                                                                                            | 	...informing operation success?  |  RequestEmployeeListUI  |                                         |


### Systematization ##

According to the taken rationale, the conceptual classes promoted to software classes are:

* UserStore
* Company

Other software classes (i.e. Pure Fabrication) identified:

* RequestEmployeeListUI
* RequestEmployeeListController

## 3.2. Sequence Diagram (SD)

![US11_SD](US11_SD.svg)


**Other alternatives may exist**

## 3.3. Class Diagram (CD)

![US11_CD](US11_CD.svg)

# 4. Tests

n/a

# 5. Construction (Implementation)


# 6. Integration and Demo

* A new option on the Administrator menu options was added.
* Some users with employee roles were bootstrapped for testing purposes.
