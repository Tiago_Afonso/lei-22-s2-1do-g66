# US 3 - Register an SNS user

## 1. Requirements Engineering

### 1.1. User Story Description

As a receptionist, I want to register a SNS user.

### 1.2. Customer Specifications and Clarifications 

>* Question: "There we can read "US03 - As a receptionist, I want to register a SNS User."
>Accordingly to our project description, the person allowed to register a SNS User it's the DGS Administrator".
>* Answer: There is no error. We will have two User Stories for registering SNS users. One of these USs is US03 and the other US will be introduced later, in Sprint C.

>* Question: "Regarding US3: "As a receptionist, I want to register an SNS User.", What are the necessary components in order to register an SNS User?"
>* Answer: The attributes that should be used to describe a SNS user are: Name, Address, Sex, Phone Number, E-mail, Birth Date, SNS User Number and Citizen Card Number.
>The Sex attribute is optional. All other fields are required.
>The E-mail, Phone Number, Citizen Card Number and SNS User Number should be unique for each SNS user.

>* Question: "In terms of the attributes that you have pointed out I would like to know the limit and rules of those attributes (EX: Phonenumber, birthdate,etc)."
>* Answer: During Sprint B I will not introduce attribute rules/formats other than the ones that I already introduced (in this forum or in the project description). Please study the concepts and define appropriate formats for the attributes.

**TODO**

### 1.3. Acceptance Criteria

* AC1: The SNS user must become a system user. The "auth" component available on the repository must be reused (without modifications).
* AC2: SNS User sex must be optional. All other attributes must be obligatory.
* AC3: Password must contain at least 3 uppercase letters, 2 digits and at length 7.

### 1.4. Found out Dependencies

No dependencies where founded.

### 1.5 Input and Output Data

Input:
* name
* address
* sex (optional)
* phone number
* email
* password
* birthdate
* sns number
* cc number

Output:
* inputted data
* success message

### 1.6. System Sequence Diagram (SSD)


![US3-SSD](US3-SSD.svg)


### 1.7 Other Relevant Remarks

No other relevant remarks.

## 2. OO Analysis

### 2.1. Relevant Domain Model Excerpt

![US3-MD](US3-MD.svg)

### 2.2. Other Remarks

No other relevant remarks.

## 3. Design - User Story Realization 

### 3.1. Rationale

**The rationale grounds on the SSD interactions and the identified input/output data.**

| Interaction ID                                     | Question: Which class is responsible for...                            | Answer       | Justification (with patterns)              |
|:---------------------------------------------------|:-----------------------------------------------------------------------|:-------------|:-------------------------------------------|
| Step 1: Starts Add New SNS user                    | ... instancing a new SNS user?                                         | client       | Creator: R1/2                              |
| Step 2: requested data                             | 	                                                                      |              |                                            |
| Step 3: types requested data                       | ... saving the input data                                              | clientDto    | DTO: decouple UI from domain layer         |
| Step 4: shows the data and requests a confirmation | ... validating the data locally (e.g.mandatory vs non-mandatory data)? | client       | IE: knows its own data                     |
| Step 4: shows the data and requests a confirmation | ... validating the data globally (e.g. duplicated)?                    | SnsUserStore | IE: knows every Client object and its data |
| Step 5: confirms the data                          | ... saving the created Client                                          | SnsUserStore | IE: records all Client objects             |
| Step 6: informs operation success                  | ... informing operation success?                                       | UI           | IE: responsible for user interaction       |              


### Systematization ##

According to the taken rationale, the conceptual classes promoted to software classes are: 

 * SnsUser
 * Company

Other software classes (i.e. Pure Fabrication) identified: 
 * RegisterSnsUserUI  
 * RegisterSnsUserController
 * SnsUserStore
 * SnsUserDto

## 3.2. Sequence Diagram (SD)

![US3-SD](US3-SD.svg)

## 3.3. Class Diagram (CD)

![US3-CD](US3-CD.svg)

# 4. Tests

**Test 1:** Test getters

    @Test
    void getSnsNumber() {
        assertEquals(snsNumber, snsUser.getSnsNumber());
    }

**Test 2:** Validate if attributes are in accordance with the acceptance criteria

    @Test
    void validateEmail() {
        assertDoesNotThrow(() -> new SnsUser(snsUserDto));

        SnsUserDto snsUserDto2 = new SnsUserDto(name, address, sex, phoneNumber, "test@", password, birthDate, snsNumber, ccNumber);
        Exception exception2 = assertThrows(IllegalArgumentException.class, () -> new SnsUser(snsUserDto2));
        assertEquals("Email format must be name@domain", exception2.getMessage());

        SnsUserDto snsUserDto3 = new SnsUserDto(name, address, sex, phoneNumber, "@email.com", password, birthDate, snsNumber, ccNumber);
        Exception exception3 = assertThrows(IllegalArgumentException.class, () -> new SnsUser(snsUserDto3));
        assertEquals("Email format must be name@domain", exception3.getMessage());
    }

**Test 3:** Validate if it is not possible to create two users with the same email

    @Test
    void saveSnsUser() {
    assertTrue(snsUser.saveSnsUser());
    assertFalse(snsUser.saveSnsUser());
    }

**Test 4:** Validate if generated password is in accordance with the acceptance criteria

    @Test
    void generatePassword() {
        String password = passwordGenerator.generatePassword(7, 3, 2);
        String uppercase = password.chars().filter(Character::isUpperCase).collect(StringBuilder::new, StringBuilder::appendCodePoint, StringBuilder::append).toString();
        String number = password.chars().filter(Character::isDigit).collect(StringBuilder::new, StringBuilder::appendCodePoint, StringBuilder::append).toString();
        assertEquals(7, password.length());
        assertTrue(uppercase.length() >= 3);
        assertTrue(number.length() >= 2);
    }

# 5. Construction (Implementation)

# 6. Integration and Demo

# 7. Observations





