# OO Analysis #

The construction process of the domain model is based on the client specifications, especially the nouns (for _concepts_) and verbs (for _relations_) used.

## Rationale to identify domain conceptual classes ##
To identify domain conceptual classes, start by making a list of candidate conceptual classes inspired by the list of categories suggested in the book "Applying UML and Patterns: An Introduction to Object-Oriented Analysis and Design and Iterative Development".


### _Conceptual Class Category List_ ###

**Business Transactions**

* Vaccination Schedule

---

**Transaction Line Items**

* Vaccine Administration

---

**Product/Service related to a Transaction or Transaction Line Item**

* Vaccine
* (EU COVID) Vaccination Certificate
* SMS message
* email message

---


**Transaction Records**

* Vaccination Administration

---  


**Roles of People or Organizations**

* SNS User
* Receptionist
* Nurse
* Center Coordinator
* System Administrator
---


**Places**

* Community Mass Vaccination Center
* Health Care Center
---

**Noteworthy Events**

* Outbreak of a new infectious disease
* Registree of a new type of vaccine
* Issuing vaccination certificate
* Configuring and managing core information
* Vaccination scheduling
* Default selection of Vaccination center
* Informing user about vaccine time and place
* Authorizing SMS message
* Sending of the SMS message with scheduled vaccination information
* Registree of arrival of the user
* Validation of the user and acknowledgment of the system that the user is ready
* Sending user to waiting room
* Checking who is in the center waiting for the vaccine
* Calling user for administration of vaccine
* Checking user's medical information
* Instruction of which vaccine to administer
* Registree of vaccine administration
* Sending user to the waiting room
* Notifying user that the recovery period has ended
* User leaving vaccination center
* Recording of adverse reactions
* Issuance of the EU COVID Digital Certificate
* Monitoring of the the vaccination process
* Registree of centers, SNS users, center coordinators, receptionists, and nurses enrolled in
the vaccination process

---


**Physical Objects**

* Vaccine container
* Syringe

---


**Descriptions of Things**

* Vaccine type
* Kinds of centers
* Vaccine brand
* Medical history of the user

---


**Catalogs**

*  None were identified

---


**Containers**

*  None were identified

---


**Elements of Containers**

*  None were identified

---


**Organizations**

*  SNS
*  DGS
*  ARS
*  AGES

---

**Other External/Collaborating Systems**

* EmailSender
* SMSSender

---


**Records of finance, work, contracts, legal matters**

* Report

---


**Financial Instruments**

*  None were identified

---


**Documents mentioned/used to perform some work/**

* Vaccination Schedule
* Vaccination Instruction
* SNS User Health Data
* Adverse Reaction
* Performance Data
---



###**Rationale to identify associations between conceptual classes**###

An association is a relationship between instances of objects that indicates a relevant connection and that is worth of remembering, or it is derivable from the List of Common Associations:

+ Company manages/owns HealthCareCenter.
+ Company manages/owns VaccinationCenter.
+ Company is responsible for owning and providing Vaccine.
+ Company is responsible for promoting VaccineType.
+ SystemAdministrator is physically and logically contained within Company.
+ SystemAdministrator is responsible for registering User.
+ SystemAdministrator is responsible for creating VaccinationCenter.
+ SystemAdministrator is responsible for creating HealthCareCenter.
+ VaccinationCenter is the employer of Receptionist.
+ VaccinationCenter is the employer of CenterCoordinator.
+ VaccinationCenter is the employer of Nurse.
+ VaccinationCenter is responsible for administering one VaccineType.
+ HealthCareCenter is the employer of Receptionist.
+ HealthCareCenter is the employer of Nurse.
+ HealthCareCenter is responsible for administering many VaccineType.
+ VaccineType is applied to AgeGroup.
+ VaccineType is recorded in VaccineSchedule.
+ Nurse is responsible for registering VaccinationEvent.
+ VaccinationEvent logically fulfills VaccineSchedule.
+ VaccinationEvent is applied to SNSUser.
+ VaccineSchedule is created in relation to SNSUser.
+ SNSUser is logically a part of AgeGroup.
+ VaccinationAdministrationProcess is created in relation to AgeGroup.
+ Vaccine is physically and logically a part of VaccinationAdministrationProcess.
+ Vaccine is logically a part of VaccineType.
+ VaccinationAdministrationProcess is logically a part of VaccinationEvent.
+ Receptionist is responsible for registering the arrivalDate in VaccinationEvent.



| Concept (A) 		             |  Association   	                |  Concept (B) |
|----------	   		             |:-------------:		                |------:       |
| Company  	                   | manages    		 	                | HealthCareCenter  |
| Company  	                   | applies    		 	                | Vaccine  |
| Comapny  	                   | promotes administration    		 	| VaccineType  |
| Company  	                   | manages    		 	                | VaccinationCenter  |
| Company  	                   | knows    		 	                  | SystemAdministrator  |
| HealthCareCenter  	         | can administer    		 	          | VaccineType  |
| HealthCareCenter  	         | employs    		 	                | Nurse  |
| HealthCareCenter  	         | employs    		 	                | Recptionist  |
| VaccineAdministrationProcess | of                               | Vaccine             |
| VaccineAdministrationProcess | according to                     | AgeGroup             |
| VaccineAdministrationProcess | of                               |    VaccinationEvent          |
| VaccinationCenter  	         | employs    		 	| Receptionist  |
| VaccinationCenter  	         | employs    		 	| Nurse  |
| SystemAdministrator          | registers        | User |
| SystemAdministrator          | creates          | HealthCareCenter |
| VaccineSchedule              | created for      | SNSUser |
| VaccineSchedule              | of taking        | VaccineType |
| CenterCoordinator            | coordinates      | VaccinationCenter |
| CenterCoordinator            | stores           | Information  |
| VaccinationEvent             | fulfilling       | VacinationSchedule |
| VaccinationEvent             | administered on  | SNSUser   |
| Vaccine                      | is of            | VaccineType |
| VaccineType                  | has              | AgeGroup    |
| Receptionist                 | schedules        | VaccineSchedule |
| Nurse                        | administers      | VaccinationEvent |
| AgeGroup                     | of               | SNSUser |
| Receptionist                 | registers        | Vaccination|



## Domain Model

**Do NOT forget to identify concepts atributes too.**

**Insert below the Domain Model Diagram in a SVG format**

![DM.svg](DM.svg)
