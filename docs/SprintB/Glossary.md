# Glossary

**Terms, Expressions and Acronyms (TEA) must be organized alphabetically.**

| **_TEA_** (EN)  | **_TEA_** (PT) | **_Description_** (EN)                                           |                                       
|:------------------------|:-----------------|:--------------------------------------------|
| **NHS** | **SNS** | National Health Care organization
| **RHC** | **ARS** | Regional Health Care organization|
| **HCG** | **AGES** | Healthcare Centres Group|
| **DGS** | **DGS** | Directorate-General of Health of Portugal |
| **pandemic** | **pandemia** | Infections disease that has spread across multiple continents|
| **COVID19**|**COVID19**| Infections disease (current pandemic) |
| **immunization** | **immunizacao** | process which an individuals immune system fortifies against an infectious disease |
| **vaccine** | **vacina** | process which an individuals immune system fortifies against an infectious disease |
| **vaccination certificate** | **certeficado de vacinacao** | Digital document proving that a person is fully vaccinated has a full vaccination against a specific disease |
| **EU COVID Digital Certificate**|**Certificado Digital COVID**| Digital document proving that a person is fully vaccinated against COVID-19|
| **adverse reactions** | **reacoes adversas** |  An unwanted effect caused by the administration of a vaccine |
| **health care centers** | **centros de saude**| Health centers are community-based and patient-directed organizations that deliver comprehensive, culturally competent, high-quality primary health care services to the nation’s most vulnerable individuals and families|
| **vacination center** | **centro de vacinacao** |organization where the vaccines will be administering|
| **nurse** | **enfermeira**| a person who is trained to vaccinate people and give care to people who are sick |
| **receptionist** | **receptionist** | a person who greets and deals with clients and visitors |
| **SNS user** | **usuario SNS**| should use the application to schedule his/her vaccination|
| **center coordinators** | **cordenador do centro** | has the responsibility to manage the Covid- 19 vaccination process|
| **administrator** | **administrador** | responsible for properly configuring and managing the core information|

